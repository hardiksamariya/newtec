var sendNoti = function(ids, messageDetail,roomInfo) {
  
    var data = {
      app_id: "155d040f-de3a-4769-ab47-76647cc11dcb",
      content_available: true,
      data: { "message": messageDetail, "type": "alert", "roomInfo": roomInfo },
      include_player_ids: ids
      };
  
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Authorization": "Basic M2U3MTRjZDEtYTc1ZC00NDk3LWIyOTItNjc4YjFmNDNhMzRi"
    };
    
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers
    };
    
    
    var https = require('https');
    var req = https.request(options, function(res) {  
      res.on('data', function(data) {
        console.log("Response:");
         
        console.log(JSON.parse(data));
      });
    });
    
    req.on('error', function(e) {
      console.log("ERROR: >>>>>>>>>>>>>>>>> ");
      console.log(e);
    });
    
    req.write(JSON.stringify(data));
    req.end();
  };
  
module.exports = sendNoti;
