
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

//add schema model 
var user = require('../model/users');
var room = require('../model/room');
var message = require('../model/message');
var constants = require('../model/constants');
var authenticate = require('../route/authenticate');

var numUsers = 0;
var roomOnline = 0;
var currentRoomID = '';

module.exports = function (io) {

    // established socket connection
    io.on('connection', function (socket) {

        var user_id = 0;
        var room_idsss = [];
        var addedUser = false;

        // change status of login and log out and update socket id
        //params : userId
        //params : notification_token

        socket.on('error', function () {
        })
        // socket.on('create', (data) => {
        //     io.nsps['/'].sockets[data[0]].join(data[1]);
        //   });

        socket.on('login', function (params) {

            // console.log("login is called"+params.user_id);

            socket.user_id = params.user_id;

            user_id = params.user_id;

            //for user device check 

            if (user_id) {

                if (params.is_registered == "true") {

                    user.findOne({ _id: ObjectId(user_id) }, function (err, notifFound) {
                        if (err) {
                            ////console.log("Login ==> error occurred in device check>>>>>>> ", err);
                        }
                        else {
                            // console.log("Previous User Found !!",notifFound.socket_id);
                            if (notifFound.socket_id !== 'undefined' && notifFound.socket_id !== '') {
                                ////console.log("Login ==> logout send to previous user", notifFound.socket_id);
                                io.to(notifFound.socket_id).emit("logout", { messageDataName: true });
                            }
                            else{
                            //to update the current user info in database
                            updateUserDataOnLogin(params);
                        }
                    }
                    });

                } else {

                    user.findOne({ _id: ObjectId(user_id) }, function (err, notifFound) {
                        if (err) {
                            //////console.log("error occurred in device check>>>>>>> ", err);
                        }
                        else {
                            if (notifFound) {
                                // decrypt = authenticate.data.decryptText(notifFound.access_token);
                                // if (!notifFound.access_token) {
                                    // console.log("printing database access token>>>", notifFound.socket_id, "printing params access token>>", params.socket_id);
                                    console.log("Login ==> logout send to previous user false",notifFound);
                                //     socket.emit("logout", { messageDataName: true });
                                // }
                                // else {
                                //     //update the user info as we have matched the acces token and its valid
                                //     updateUserDataOnLogin(params);
                                // }
                                // console.log("params=================="+params)
                                updateUserDataOnLogin(params);
                            }


                        }
                    });
                }
            }
        });

       
        function updateUserDataOnLogin(params) {

            let user_id = params.user_id;
            ////console.log("user_id in update user data login ", params , " and user id is ", user_id);
            var deviceType = "Android"
            if (typeof params.device_type !== 'undefined' && params.device_type) {
                deviceType = params.device_type
            }
            // last_login: Date.now().toString(), removed 
            user.update({ _id: ObjectId(user_id) }, {
                $set: {
                    device_type: deviceType, online_status: true, is_login: true,
                    socket_id: socket.id, notification_token: params.notification_token, last_login: Date.now()
                }
            }, function (err, data2) {
                if (err) {
                    ////////console.log(err, "update user socketid");
                } else {
                    // console.log(data2);
                    console.log("updated socket id is ",socket.id,'>>>>>>>>>>',data2);
                    user.findOne({ _id: ObjectId(user_id) }, function (err, data1) {
                        if (err) {
                            ////////console.log(err, "get user info");
                        } else {
                            console.log(">>>>>>>>>>>>>data>>>>>>>>>>>>>>>>>>> ", params);
                            console.log(">>>>>>>>>>>>>data1>>>>>>>>>>>>>>>>>>> ", data1);

                            room.aggregate([{ $unwind: "$users" }, {
                                $match: {
                                    $and: [{ "users.user_id": ObjectId(user_id) },
                                    { "users.status": "joined" }]
                                }
                            }, { $project: { _id: 1 } }], function (err, rooms) {
                                if (err) {
                                    ////////console.log(err, 'to add to rooms');
                                } else {
                                    rooms.forEach(rms => {
                                        socket.join(rms._id);
                                        room_idsss.push(rms._id);
                                    });
                                }
                            });
                            socket.emit("userInfo", { userData: data1 });
                        }
                    });
                }
            });
        }

        // change status of login and log out and update socket id
        //params : userId
        socket.on('getRooms', function (params) {
            var data = params;

            user_id = data["user_id"];
            ////////console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", constants.PUBLIC_GROUP_ID + "   !!!!!!!    " + user_id);
            room.aggregate([{ $match: { $or: [{ _id: ObjectId(constants.PUBLIC_GROUP_ID) }, { $and: [{ "users.user_id": ObjectId(user_id) }, { "users.status": "joined" }] }] } },
            { $unwind: "$users" }, { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
            { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
            {
                $group: {
                    '_id': {
                        "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
                        "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "is_deleted": "$is_deleted", "history_enable": "$history_enable", "openToAll": "$open_to_all", "updated_at": "$updated_at"
                    },
                    'users': { '$push': '$users' }
                }
            }], function (err, roomInfo) {
                if (err) {
                    ////////console.log(err, "get room info");
                } else {
                    console.log("@@@@@@@", roomInfo);
                    socket.emit("roomsData", { roomData: roomInfo });
                }
            });
        });




        // //NEW GET ROOM sOCKET
        // socket.on('getRooms', function (params) {
        //     var data = params;
        //     // ////////console.log(data);
        //     user_id = data["user_id"];
        //     ////////console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", constants.PUBLIC_GROUP_ID + "   !!!!!!!    " + user_id);
        //     room.aggregate([{ $match: { $and: [{ "users.user_id": ObjectId(user_id) }, { "users.status": "joined" }]}},
        //     { $unwind: "$users" }, { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
        //     { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } }
     
        //     ], function (err, roomInfo) {
        //         if (err) {
        //             ////////console.log(err, "get room info");
        //         } else {
        //             //////////console.log("@@@@@@@", roomInfo);
        //             socket.emit("roomsData", { roomData: roomInfo });
        //         }
        //     });
        // });

        //get messages of particular user
        //params : userId
        //params : last_fetch_time
        socket.on('getMessages', function (params) {
            ////////console.log("get messages ", params);
            var data = params;
            user_id = data["user_id"];
            
            last_fetch_time = data["last_fetch_time"];
            ////console.log("time from param : ", last_fetch_time);

            if (user_id) {
                user.findOne({ _id: ObjectId(user_id) }, function (err, data) {
                    if (err) {
                        ////////console.log("error is here ", err);
                    }
                    else {
                        if (data) {
                            room.aggregate([{ $unwind: "$users" }, { $match: { $and: [{ "users.user_id": ObjectId(user_id) }, { "users.status": "joined" }] } },
                            { $project: { _id: 1 } }], function (err, rooms) {
                                if (err) {
                                    ////////console.log(err);
                                } else {
                                    var room_ids = [];

                                    rooms.forEach(rm => {
                                        room_ids.push(ObjectId(rm._id))
                                    });

                                    console.log("last_fetch_time >>> ", last_fetch_time);
                                    //adding public group data
                                    message.aggregate([{ $match: {$and: [{ room_id: { $in: room_ids }, created_at: { $gt: last_fetch_time } }]} }, { $sort: { created_at: -1 } },
                                    { $group: { _id: "$room_id", messages: { $push: "$$ROOT" } } }], function (err, messages) {
                                        if (err) {
                                            ////////console.log(err);
                                        } else {
                                            ////console.log("messages are ", messages)
                                            socket.emit("oldMessages", { messageData: messages });
                                        }
                                    });

                                    //for check withdraw message
                                    // message.aggregate([{ $match: {$and: [{ room_id: { $in: room_ids }, created_at: { $gt: last_fetch_time }, is_withdraw: true }]} }, { $sort: { created_at: -1 } },
                                    //     { $group: { _id: "$room_id", messages: { $push: "$$ROOT" } } }], function (err, withdrawMessages) {
                                    //         if (err) {
                                    //             ////////console.log(err);
                                    //         } else {
                                    //             ////console.log("withdraw messages are ", withdrawMessages)
                                    //             for(let i=0;i< withdrawMessages.length;i++){
                                    //                 socket.emit("withdrawMessage",{ message_id: withdrawMessages[i]._id});
                                    //             }
                                    //         }
                                    // });

                                    ////////console.log(">>>>>>> ", data);
                                    categories = [];
                                    data.categories.forEach(category => {
                                        categories.push(ObjectId(category))
                                    });

                                    //time test
                                    var d = new Date();
                                    d.setDate(d.getDate() - 2);
                                    var ddd = new Date(d.toUTCString());
                                    var dd = new Date(ddd.getFullYear(), ddd.getMonth(), ddd.getDate());
                                    ////////console.log("new date >>", dd.getTime());
                                    currentDate = dd.getTime() + "";

                                    ////////console.log("main time >> ", currentDate);

                                    ////////console.log("categories data ", categories);
                                    message.aggregate([
                                        {
                                            $match: {
                                                $or: [
                                                    { room_id: ObjectId("5b27477e4b20cb8fab0d6b3a"), updated_at: { $gt: last_fetch_time }, categories: { $exists: false } },
                                                    { categories: { $in: categories }, room_id: ObjectId("5b27477e4b20cb8fab0d6b3a"), updated_at: { $gt: last_fetch_time } }
                                                ]
                                            }
                                        },
                                        { $sort: { created_at: -1 } },
                                        { $group: { _id: "$room_id", messages: { $push: "$$ROOT" } } }], function (err, messages) {
                                            if (err) {
                                                ////////console.log(err);
                                            } else {
                                                socket.emit("oldPublicMessages", { messageData: messages });
                                            }
                                        });
                                }
                            });
                        }
                        else {
                            ////////console.log("user not found");
                        }

                    }
                })

            }
        });

        socket.on('getWithdrawMessages', function (params) {
            var data = params;
            user_id = data["user_id"];
            last_fetch_time = data["last_fetch_time"];
            ////console.log("time from param : ", last_fetch_time);

            if (user_id) {
                user.findOne({ _id: ObjectId(user_id) }, function (err, data) {
                    if (err) {
                        ////////console.log("error is here ", err);
                    }
                    else {
                        if (data) {
                            room.aggregate([{ $unwind: "$users" }, { $match: { $and: [{ "users.user_id": ObjectId(user_id) }, { "users.status": "joined" }] } },
                            { $project: { _id: 1 } }], function (err, rooms) {
                                if (err) {
                                    ////////console.log(err);
                                } else {
                                    var room_ids = [];

                                    rooms.forEach(rm => {
                                        room_ids.push(ObjectId(rm._id))
                                    });

                                    //adding public group data
                                    // message.aggregate([{ $match: {$and: [{ room_id: { $in: room_ids }, created_at: { $gt: last_fetch_time } }]} }, { $sort: { created_at: -1 } },
                                    // { $group: { _id: "$room_id", messages: { $push: "$$ROOT" } } }], function (err, messages) {
                                    //     if (err) {
                                    //         ////////console.log(err);
                                    //     } else {
                                    //         ////console.log("messages are ", messages)
                                    //         socket.emit("oldMessages", { messageData: messages });
                                    //     }
                                    // });

                                    //for check withdraw message
                                    message.aggregate([{ $match: {$and: [{ room_id: { $in: room_ids }, updated_at: { $gt: last_fetch_time }, is_withdraw: true }]} }, { $sort: { created_at: -1 } },
                                        { $group: { _id: "$room_id", messages: { $push: "$$ROOT" } } }], function (err, withdrawMessages) {
                                            if (err) {
                                                ////////console.log(err);
                                            } else {
                                                ////console.log("withdraw messages are ", withdrawMessages)
                                                for(let i=0;i< withdrawMessages.length;i++){
                                                    ////console.log("withdrawMessage id >>>>>>>>>",withdrawMessages[i].messages[0]._id);
                                                    socket.emit("withdrawMessage",{ message_id: withdrawMessages[i].messages[0]._id});
                                                }
                                            }
                                    });
                                }
                            })
                        }
                    }
                })
            }
        });

        socket.on('readMessage', function (params) {
            var message_id = params.message_id;
            var user_id = params.user_id;

            message.update({ _id: ObjectId(message_id), read_id: { $ne: user_id } }, { $push: { read_id: user_id } }, function (err, messageInfo) {
                if (err) {
                    res.json(err);
                } else {
                    ////////console.log(user_id);
                    ////////console.log(message_id);
                    ////////console.log("Data Update");
                }
            });
            ////////console.log("read Id Update");
        });

        // params : 1. message_ids, 2. user_id
        socket.on('deleteMessage', function (params) {
            var message_ids = params.message_ids.toString();
            var user_id = params.user_id;
            var message_id = [];

            message_ids.split(",").forEach(message_idd => {
                message_id: message_idd
                message_id.push(message_idd);
            });
            // res.json(message_id.length);
            message_id.forEach(m_id => {
                message.update({ _id: ObjectId(m_id) }, { $push: { deleted_id: user_id } }, function (err, messageInfo) {
                    if (err) {
                        ////////console.log("Error Occured ", err);
                    } else {
                        ////////console.log("Delete ID Update");
                    }
                });
            });
            ////////console.log("No Data Updated");
        })



        // params : 1. user_id, 2. room_id
        socket.on('typing', function (params) {
            user.findOne({ _id: ObjectId(params.user_id) }, function (err, data) {
                if (err) {
                    ////console.log("Error Occured ", err);
                } else {
                    if (params.message == undefined || params.message == "")
                        msg = data.first_name + ' ' + data.last_name + " is typing...";
                    else
                        msg = params.message;
                    con = socket.broadcast.to(params.room_id).emit('showTyping',
                        { "msg": msg, "room_id": params.room_id, "default_message": params.message, "user_id": params.user_id ,"phone": data.phone});
                    ////console.log(">>>>>>>>>>>> on typing >>>>>>>>>>>>>>>")
                }
            });
            // ////////console.log(para);
        });

        socket.on('error', function (err) {
            ////////console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ", err);
        });

        socket.on('stopTyping', function (params) {
            ////console.log(">>>>>>>>>>>>> on stop typing >>>>>>>>>>>>>>>>>>> ");
            aaa = socket.broadcast.to(params.room_id).emit('hideTyping', { "room_id": params.room_id, "user_id": params.user_id });

        });



        socket.on('logout', function (params) {
            console.log("socket id logout called and updated ");
            user.update({ _id: params.user_id }, { $set: { online_status: false, socket_id: "", notification_token: "" } }, function (err, data) {
                if (err) {

                } else {

                }
            });
        });

        socket.on('disconnect', function () {
            console.log("user get dissconnected");
            room_idsss.forEach(id => {
                socket.leave(id);
            });

            room_idsss = [];

            ////console.log("disconnect called and socket cleared ");
            if (user_id) {
                user.findOne({ _id: user_id }, function (err, userData) {
                    if (err) {
                        ////////console.log("error in disconneect ", err);
                    }
                    else {
                        if (userData) {

                            if (userData.socket_id !== '' && userData.socket_id !== 'undefined') {
                                if (userData.socket_id == socket.id) {
                                    ////console.log("in disconnect true part");
                                    user.update({ _id: user_id }, { $set: { online_status: false, socket_id: "" } }, function (err, data) {
                                        if (err) {

                                        } else {
                                            socket.broadcast.emit('hideTyping', { "user_id": user_id });
                                        }
                                    });
                                }
                                else {
                                    ////console.log("nothing happens in disconnect ");
                                }

                            }

                        }


                    }
                })
            }
        });

        socket.on('updateOnlineStatus', function (params) {

            ////console.log('update online status called ', params);

            user_id = params.user_id;
            is_online = params.is_online;
            access_token = params.access_token;
            notification_token = params.notification_token;
            is_registered = params.is_registered;
            // console.log("is_registered=================",is_registered);
            is_registered=true;
            is_online=true;
            console.log("is_registered=================",is_registered);

            if (user_id) {
                user.findOne({ _id: (ObjectId(user_id)) }, function (err, Info) {
                    if (err) {
                        ////////console.log("Error in updateOnlineStatus",);
                    } else {
                        // ////////console.log("updateOnlineStatus>>>>>>>>>>>>>>>>>>>>Info",Info);
                        if (is_registered == "true") {
                            ////console.log('is Registred true');

                            if (Info.socket_id !== 'undefined' && Info.socket_id !== '') {
                                console.log('Emit ot --> ', Info.socket_id);
                                io.to(Info.socket_id).emit("logout", { userData: Info.access_token });
                            }
                            //update the current user info in database
                            user.update({ _id: ObjectId(user_id) }, {
                                $set: {
                                    online_status: is_online,
                                    notification_token: notification_token
                                }
                            }, function (err, done) {
                                if (err) {

                                }
                                else {
                                    ////console.log("online status updated new socket id ");
                                }
                            })
                        }
                        else {
                            if (Info) {
                                // decrypt = authenticate.data.decryptText(Info.access_token);
                                ////console.log("decrypt access token in updateOnlineStatus>>>>>>>>>>>>>>>>>>>>>>>>>>",decrypt);
                                if (access_token) {
                                    let str;
                                    if (is_online) {
                                        str = { online_status: is_online, socket_id: socket.id };
                                    }
                                    else {
                                        str = { online_status: is_online, socket_id: "" };
                                    }
                                    user.update({ _id: ObjectId(user_id) }, { $set: str }, function (err, done) {
                                        if (err) {

                                        }
                                        else {

                                        }

                                    })
                                } else {

                                    socket.emit("logout", { userData: Info.access_token });
                                }
                            }


                        }
                    }
                })
            }
        });
    });

}