var express = require('express');
var app = express();

var route = require('./route/route');
var chat = require('./SocketEvents/chat');

var http = require('http').Server(app);

var path = require('path');
var multer = require('multer');

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

////schedule message //////////
var ScheduleJob = require('./model/scheduleJobs')
var Message = require('./model/message')
var schedule = require('node-schedule');
var commonMethods = require('./route/commonMethods');
////
var io = require('socket.io')(http);

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

app.use(express.static('public'));

var catalogue = require('./route/catalogue');
app.use('/catalogue', catalogue);

app.use(function (req, res, next) {

    // Website you wish to allow to connect


    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8081');
    //res.setHeader('Access-Control-Allow-Origin', 'http://52.66.4.80:5000');


    // res.setHeader('Access-Control-Allow-Origin', 'http://13.232.159.175:8081');
    // res.setHeader('Access-Control-Allow-Origin', 'http://52.66.4.80:5000');
    // res.setHeader('Access-Control-Allow-Origin', 'http://52.66.4.80');

    // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8081');

    res.setHeader('Access-Control-Allow-Origin', '*');                          //  For all url
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:9000');


    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// function for routes
route(app, io);

// function for socket events;
chat(io);

//establish connection 
http.listen(3060, function () {
    console.log('listening on *:3060');

    schedule.scheduleJob('* 0 0 * * *', function () {
        currentDate = Date.now();
        //console.log("current date in public messages ", currentDate);
        currentDate = parseInt(currentDate) - (86400000 * 2);
        currentDate = currentDate + "";

        Message.deleteMany({ room_id: ObjectId("5b27477e4b20cb8fab0d6b3a"), updated_at: { $lt: currentDate } }, function (err, messagesDeleted) {
            if (err) {
                //console.log("error while deleting messages before two days in public group in index.js on date :- ", Date.now());
            }
            else {
                //console.log({ error: false, message: "Check message deleted on date:- " + Date.now(), data: messagesDeleted });
            }
        });
    });

    //rescheduling messages if some messages left
    ScheduleJob.deleteOne({ room_ids_to_sent: { $size: 0 } }, function (err, scheduledJob) {
        if (err) {
            //console.log("error while fetching schedule job ", err);
        }
        else {
            //console.log({ error: false, message: "message deleted" });
        }
    })

    ScheduleJob.find({}, function (err, scheduleMessageData) {
        if (err) {
            //console.log("in index.js error occurred while scheduling job ", err);
        }
        else {
            if (scheduleMessageData.length > 0) {
                scheduleMessageData.forEach(message => {
                    date = new Date(parseInt(message.scheduled_at));
                    checkDate = Date.now();
                    if (date <= checkDate) {
                        //console.log("let seee in true", date);
                        for (let i = 0; i < message.room_ids_to_sent.length; i++) {
                            //console.log("log here we gooo ");
                            commonMethods.data.sendTest(null, message, message.room_ids_to_sent[i], message._id, io, function (result) {
                                //console.log("schedule message ", result);
                            });
                        }
                    }
                    else {
                        //console.log("let see in false", date)
                        var j = schedule.scheduleJob(date, function () {
                            for (let i = 0; i < message.room_ids_to_sent.length; i++) {
                                //console.log("log here we gooo ");
                                commonMethods.data.sendTest(null, message, message.room_ids_to_sent[i], message._id, io, function (result) {
                                    //console.log("schedule message ", result);
                                });
                            }
                        });
                    }
                })
            }
        }
    });


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // require('heapdump');

    // var leakyData = [];
    // var nonLeakyData = [];

    // class SimpleClass {
    //     constructor(text) {
    //         this.text = text;
    //     }
    // }

    // function cleanUpData(dataStore, randomObject) {
    //     var objectIndex = dataStore.indexOf(randomObject);
    //     dataStore.splice(objectIndex, 1);
    // }

    // function getAndStoreRandomData() {
    //     var randomData = 1234567890;
    //     var randomObject = new SimpleClass(randomData);

    //     leakyData.push(randomObject);
    //     nonLeakyData.push(randomObject);

    //     // cleanUpData(leakyData, randomObject); //<-- Forgot to clean up
    //     cleanUpData(nonLeakyData, randomObject);
    // }

    // function generateHeapDumpAndStats() {
    //     //1. Force garbage collection every time this function is called
    //     try {
    //         global.gc();
    //     } catch (e) {
    //         //console.log("You must run program with 'node --expose-gc index.js' or 'npm start'");
    //         process.exit();
    //     }

    //     //2. Output Heap stats
    //     var heapUsed = process.memoryUsage().heapUsed;
    //     //console.log("Program is using " + heapUsed + " bytes of Heap.")

    //     //3. Get Heap dump
    //     process.kill(process.pid, 'SIGUSR2');
    // }

    // //Kick off the program
    // setInterval(getAndStoreRandomData, 5); //Add random data every 5 milliseconds
    // setInterval(generateHeapDumpAndStats, 2000); //Do garbage collection and heap dump every 2 seconds
});
