let multer = require('multer');
let multerS3 = require('multer-s3');
var aws = require('aws-sdk');
let express = require("express");
let router = express.Router();
let Catalogue = require('../model/catalogue');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

////////////////////////////////Multer S3 Config/////////////////////////////////////////////////////////

var AWS_ACCESS_KEY = 'AKIAJVEJEHPNQH2XEBQQ'; // This may change later
var AWS_SECRET_KEY = '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'; // this may also change later
var AWS_REGION = 'ap-south-1'; // may change
var S3_BUCKET = 'tecmaths'; // may change

aws.config.update({ accessKeyId: AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY, region: AWS_REGION });
s3 = new aws.S3();

var s3key = "Catalogues/" + 'image__' + Date.now() + '_';
var s3keyURL = "Catalogues/" + 'image__' + Date.now() + '_';
var taggingValue = "auto-delete=false";

/////////// Config for S3 Bucket with naming convention
var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'tecmaths',
        acl: 'public-read',
        tagging: taggingValue,
        contentType: multerS3.AUTO_CONTENT_TYPE,

        metadata: function (req, file, cb) {
            //////////////console.log("testing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ", file.fieldname);
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req, file, cb) {
            s3keyURL = s3key + file.originalname;
            //////////////console.log('fileUrl>>>>>>>>>>>>>>>>>', s3keyURL);
            cb(null, s3keyURL);
        }
    })
});
///////////////////////////////////////////////////////////////////////////////////////////////

router.post("/", function(req, res) {
    //console.log("get cata")
    let room_id = req.body.room_id;

    Catalogue.find({room_id : room_id}, function(err, data){
        if(err){
            //console.log("error occurred ", err);
            res.json({error : true, message : "Error occurred", data : err})
        }
        else{
            if(data.length > 0){
                res.json({error : false, message: "Success", "data" : data});
            }
            else{
                res.json({error : true, message : "No data found"});
            }            
        }
    });
});

router.post("/create", function(req, res) {
    let room_id = ObjectId(req.body.room_id);
    let text = req.body.text;
    let caption = req.body.caption;
    let type = req.body.type;
    let user_id = [req.body.user_id];
    let image = req.body.image;
    
    let data = new Catalogue({
        text : text,
        caption : caption,
        type : type,
        image:image,
        room_id : room_id,
        read_by : user_id,
        created_at : Date.now(),
        updated_at : Date.now()
    })

    data.save((err, savedData)=>{
        if(err){
            //console.log("error on line 83");
            res.json({error : true, message : "Error occurrres", data : err});
        }
        else{
            res.json({error : false, message : "Success", data : savedData});
        }
    })    
});

router.post("/markRead", function(req, res) {
    //console.log("Mark Read>>>>>>>>>>>",req.body.user_id);
  let user_id = req.body.user_id;
  let catalogue_id = req.body.catalogue_id;

  catalogue_id = catalogue_id.split(",");
  catalogue_ids = [];
  catalogue_id.forEach(element => {
    catalogue_ids.push(ObjectId(element));
  });

  var lent =0;
  for(let i=0;i< catalogue_ids.length;i++){

    Catalogue.findOne({_id : catalogue_ids[i], read_by : ObjectId(user_id)}, function(err, data){
        if(err){
            lent++;
            //console.log("mark read ", err);
            res.json({error : true, message : "Error occurred", data : err})
        }
        else{
          if(!data){
              lent++;
              //update user
              Catalogue.updateOne({_id : catalogue_ids[i]} , {$push : {read_by : ObjectId(user_id)}}, function(err, data){
                  if(err){
                      //console.log("error ", err);
                      res.json({error : true, message : "Error while update", data : err});
                  }
                  else{
                      if(lent == catalogue_ids.length){
                        res.json({error : false, message : "Marked read", data : data});
                      }
                      
                  }
              });
          }else{
              lent++
              if(lent == catalogue_ids.length){
                res.json({error : false, message : "Already read", data : data})
              }
          }
        }
    })
  }
  
});

router.post("/getUnreadCount", function(req, res) {
    //console.log("getUnreadCount>>>>>>>>>>>>>>>>>",req.body.user_id);
    user_id = req.body.user_id;
    room_id = req.body.room_id;

    // Catalogue.find({room_id : room_id},{_id: 0, read_by: 1}, function(err,catData){
    //     if(err){
    //         //console.log("Error>>>>>>>>>>>>>>>",err);
    //     }if(catData){
    //         //console.log("catData is here>>>>>>>>>>>>",catData);
            Catalogue.find({room_id : room_id,read_by : {$nin : [ObjectId(user_id)]}}, (err,data)=>{
                if(err){
                    //console.log("error in get unread count ", err);
                    res.json({error : true, message : "Error", data : err})
                }
                else{
                    //console.log("un read count is >>>>>>>>>",data.length);
                    res.json({error : false, message : "Success", count : data.length});
                }
            });
    //     }else{
    //         res.json({error : true, message : "Catalogue not found"})
    //     }
    // })
    
});

router.post("/update", function(req, res) {
    data = req.body;
    id = data['_id'];
    Catalogue.update({_id : ObjectId(id) }, {$set : data}, (err, done)=>{
        if(err){
            //console.log("error in update ", err);
            res.json({error : true, message : "Error", data : err});
        }
        else{
            Catalogue.find({_id : ObjectId(id)}, (err, data)=>{
                if(err){
                    res.json({error : true, message : "Error", data : err});
                }
                else{
                    res.json({error : false, message : "Success", data : data});
                }
            })
            
        }
    });
});

router.post("/delete", function(req, res) {
    cat_id = req.body.cat_id;
    Catalogue.deleteOne({_id : ObjectId(cat_id)}, function(err, done){
        if(err){
            res.json({error : true, message : "Error occurred"});
        }
        else{
            res.json({error : false, message : "Success"});
        }
    });
});

module.exports = router;
