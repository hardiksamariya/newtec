var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var user = require('../model/users');
var room = require('../model/room');
var TestMessage = require('../model/message');
var constants = require('../model/constants');
var sendNotification = require('../sendNotification');
var sendNotificationForIOS = require('../sendNotificationForIOS');
var authentication = require('./authenticate');
var ScheduleJob = require('../model/scheduleJobs')

var methods = {

    updateOrInsertIntoRoom: function(userId, groupId,display_name, next) {
      
        room.updateOne({_id: ObjectId(groupId), "users.user_id": ObjectId(userId)}, {$set: {"users.$.join_at": Date.now().toString(), "users.$.status": "joined", 
        "users.$.is_admin": false, "users.$.user_id": ObjectId(userId), "users.$.display_name": display_name, "updated_at":Date.now().toString() }}, 
        function(err, roomUpdateDone) {
            if(roomUpdateDone.nModified == 1 && roomUpdateDone.n == 1 && roomUpdateDone.ok == 1) {
                if(err) {
                    next({"error": true, "message": "Database Error!"});
                } else {
                    next(true);
                }
            } else {
                room.updateOne({_id: ObjectId(groupId)}, {$push: {users: {join_at: Date.now().toString(), status: "joined", 
                is_admin: false, user_id: ObjectId(userId), display_name: display_name, "updated_at":Date.now().toString()}}}, function(err, roomUpdateDone) {
                   
                    if(err) {
                        next({"error": true, "message": "Database Error!!"});
                    } else {
                        next(true);
                    }
                });
            }
        });
    },
    getRoom : function(groupId, done){

        //////console.log("group id in common method is here ",groupId);
       
        room.aggregate([{ $match:  { _id: ObjectId(groupId) } } ,
        { $unwind: "$users" }, { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
        { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
        {
            $group: {
                '_id': {
                    "_id": "$_id","location":"$location","category_id":"$category_id","keyword":"$keyword", "working_hour": "$working_hour", "day_of_working": "$day_of_working","cover_photo":"$cover_photo", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
                    "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "is_deleted": "$is_deleted", "history_enable": "$history_enable", "openToAll": "$open_to_all", "updated_at": "$updated_at"
                },
                'users': { '$push': '$users' }
            }
        }], function (err, roomInfo) {
            if (err) {
                ////console.log(err, "get room info");
                // next({'error':true});
            } else {
               // console.log("@@@@@@@@@@@@@@@@@ rooom info here is ", roomInfo);
                if(roomInfo){
                    done(roomInfo);
                }
              
            }
        });

    },

    joinRoom: function(userId, groupId, addedBy, display_name,io, from_link,next) {
        ////console.log("hello from inside of joinRoom");
        // res.json({"message":"hello"})
        // next({"error": addedBy , "message": "Database Error!!!"});
        let _this = this;
        this.updateOrInsertIntoRoom(userId, groupId, display_name,function(result) {
            
            if(result == true) {
                // next({"Meag":addedBy});
                user.updateOne({_id: ObjectId(userId)}, {$push: {rooms: ObjectId(groupId)}}, function(err, userUpdateDone) {
                    if(err) {
                        next({"error": true, "message": "Database Error!!!"});
                    } else {
                        // next({"Me":userUpdateDone});
                        if(groupId != constants.PUBLIC_GROUP_ID) {
                            // next(addedBy);
                            if(addedBy == undefined) {
                                // next({addedBy});
                                next(true);
                            } else {
                                // next(addedBy);
                                ////console.log("getRoom called");
                                _this.getRoom(groupId, (mInfo) => {
                                    // next({"a":"in joinRoom about getRoom","b":});
                                    
                                    if(mInfo){
                                        io.to(groupId).emit("updateRoom", { roomId: mInfo });
                                    }
                                });
                               
                                // next({error: true, message : "Error occurred addedByUserInfo", data : addedBy});
                                user.findOne({_id: ObjectId(addedBy)}, function(err, addedByUser) {
                                    // next(addedByUser);
                                    // next({error: true, message : "Error occurred addedByUserInfo", data : userId});
                                    if(err) {
                                        next({error: true, message : "Error occurred addedByUserInfo", data : err});
                                    } else {
                                        if(addedByUser) {
                                            user.findOne({_id : ObjectId(userId) }, function(error, addeduser) {
                                                if(error){
                                                    next({error: true, message : "Error occurred addeduserInfo", data : error});
                                                }
                                                else{

                                                    if(addeduser) {
                                                        
                                                        room.aggregate([{ $match: { _id: ObjectId(groupId)} }, { $unwind: "$users" },
                                                        { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                                                        { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                                                        { $group: { '_id': { "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by",
                                                        "created_at": "$created_at", "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", 
                                                        "history_enable": "$history_enable" ,"updated_at":"$updated_at"}, 'users': { '$push': '$users' } } }], 
                                                            function(err, dataaa) {
                                                            if(err) {
                                                                next({error: true, message : "Error occurred joinGroup roomInfo", data : err});
                                                            }
                                                            else {
                                    
                                                                if(dataaa) {
                                                                    var responseData = dataaa[0];
                                                                    if(from_link == "true"){
                                                                        var contentValue =display_name+" joined from this group's invited link";
                                                                    }else{
                                                                        var contentValue = addedByUser.first_name + ' ' + addedByUser.last_name + " added " + display_name;
                                                                    }

                                                                    //console.log("contentValue is here>>>>>>",contentValue);
                                                                    
                                                                    var currentTime = Date.now().toString();
                                                                    var messageData = new TestMessage({
                                                                        sender_id: ObjectId(addedBy),
                                                                        room_id: ObjectId(groupId),
                                                                        content_type: "alert",
                                                                        retain_count: responseData.users.length,
                                                                        content: contentValue,
                                                                        created_at: currentTime,
                                                                        updated_at: currentTime
                                                                    });
                                                                    messageData.save(function (err, messageInfo) {
                                                                        if (err) {
                                                                            next({error: true, message : "Error occurred joinGroup messageInfo", data : err});
                                                                        } else {
                                                                            TestMessage.aggregate([{ $match: { _id: ObjectId(messageInfo._id) } },
                                                                                { $lookup: { from: "users", localField: "sender_id", foreignField: "_id", as: "sender_id" } }],                                                                         
                                                                                function(err, messageInformation) {
                                                                                if(err) {
                                                                                    next({error: true, message : "Error occurred joinGroup messageInformation", data : err});
                                                                                } else {
                                                                                    if (Array.isArray(messageInformation)) {
                                                                                        var messageDetail = messageInformation[0];
                                                                                        responseData.users.forEach(userss => {
                                                                                            try {
                                                                                                if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                                                                                     //sendNotification([userss.user_id[0].notification_token], messageDetail);                                                                                                    
                                                                                                }
                                                                                            } catch(e){}
                                                                                        });
                                                                                        
                                                                                        try {
                                                                                            if (addeduser.socket_id != '')
                                                                                                io.nsps['/'].sockets[addeduser.socket_id].join(groupId);
                                                                                        } catch(e){}
                                                                                        //this is for group where added user
                                                                                        // if(from_link == "true"){
                                                                                            // room.findOne({_id: ObjectId(groupId)}, function(err, data1){
                                                                                            //     if(err){

                                                                                            //     }else{
                                                                                            //         if(data1){
                                                                                            //             user.findOne({_id: ObjectId(data1.created_by)}, function(err, data2){
                                                                                            //                 if(err){

                                                                                            //                 }else{
                                                                                            //                     if(data2){
                                                                                                                    // io.to(groupId).emit("onMessageReceived", messageDetail);
                                                                                            //                     }
                                                                                            //                 }
                                                                                            //             })
                                                                                            //         }
                                                                                            //     }
                                                                                            // })

                                                                                            
                                                                                        // }else{
                                                                                            io.to(groupId).emit("onMessageReceived", messageDetail);
                                                                                       
                                                                                        // }
                                                                                        
                                                                                        
                                                                                        // this is for user who has been added
                                                                                        room.findOne({_id: ObjectId(groupId)},function(err,done){
                                                                                            if(err){
                                                                                                
                                                                                            }else{
                                                                                                // console.log("added you");
                                                                                                if(from_link == "true"){
                                                                                                     message =display_name+" joined from this group's invited link"; 
                                                                                                }else{
                                                                                                    message=addedByUser.first_name+" added you";
                                                                                                }
                                                                                               
                                                                                                io.to(addeduser.socket_id).emit("userAdded", {room_id: groupId,room_name: done.name, message: message, user_id:userId  });

                                                                                               
                                                                                            }
                                                                                                  
                                                                                            
                                                                                        
                                                                                        })
                                                                                        
                                                                                       
                                                                                        next(true);
                                                                                    }
                                                                                }
                                                                            })
                                                                        }
                                                                    });
                                                                }else{
                                                                    next({error: true, message : "Data not found"});
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        } else {
                            next(true);
                        }
                    }
                });
            } else {
                next(result);
            }
        }); 
    },

    leaveRoom: function(userId, groupId, removedBy, io,display_name, next) {
        let _this = this;
        room.updateOne({_id: ObjectId(groupId), "users.user_id": ObjectId(userId)}, {$set: {"users.$.status": "left", "users.$.is_admin": false,"updated_at":Date.now().toString(), }}, 
        function(err, roomUpdateDone) {
            if(err) {
                next({"error": true, "message": "Database Error!"});
            } else {
                authentication.data.checkRoomForAdmin(groupId, function(result) {
                    user.update({_id: ObjectId(userId)}, {$pull: {rooms: ObjectId(groupId)}}, function(err, userUpdateDone) {
                        if(err) {
                            next({"error": true, "message": "Database Error!!"});
                        } else {
                            if(groupId != constants.PUBLIC_GROUP_ID) {                            
                                user.findOne({_id: ObjectId(userId)}, function(err, userInfo) {
                                    if(err) {
                                        next({"error": true, "message": "Database Error!!!!"});
                                    } else {

                                        if(userInfo){
                                            if(userInfo.socket_id !=='' && userInfo.socket_id !== 'undefined'){

                                                _this.getRoom(groupId, (mInfo) => {
                                                    if(mInfo){
                                                        io.to(userInfo.socket_id).emit("updateRoom", { roomId: mInfo });
                                                    }
                                                });
                                                        
                                            }             
                                        }
        
                                       
        


                                        room.aggregate([{ $match: { _id: ObjectId(groupId) } }, { $unwind: "$users" }, 
                                        { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } }, 
                                        { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } }, 
                                        { $group: { '_id': { "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
                                        "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "history_enable": "$history_enable", "updated_at":"$updated_at" }, 'users': { '$push': '$users' } } }], 
                                        function(err, data) {
                                            if(err) {
                                                next({"error": true, "message": "Database Error!!!!"});
                                            } else {
                                                var responseData = data[0];
                                              
                                                 if(responseData != undefined) {
                                                    authentication.data.fetchUserInfo(removedBy, function(removedbyUser) {
                                                       
                                                        if(removedbyUser == false) {
                                                            var currentTime = Date.now().toString();
                                                            var messageData = new TestMessage({
                                                                sender_id: "",
                                                                room_id: ObjectId(groupId),
                                                                content_type: "alert",
                                                                retain_count: responseData.users.length,
                                                                content: display_name + " left",
                                                                created_at: currentTime,
                                                                updated_at: currentTime
                                                            });      var currentTime = Date.now().toString();
                                                        } else {
                                                            var currentTime = Date.now().toString();
                                                           

                                                            var messageData = new TestMessage({
                                                                sender_id: ObjectId(removedBy),
                                                                room_id: ObjectId(groupId),
                                                                content_type: "alert",
                                                                retain_count: responseData.users.length,
                                                                content: removedbyUser.first_name + ' ' + removedbyUser.last_name + " removed " + display_name,
                                                                created_at: currentTime,
                                                                updated_at: currentTime
                                                            });
                                                        }
                            
                                                        messageData.save(function (err, messageInfo) {
                                                            if (err) {
                                                                next({"error": true, "message": "Database Error! save message.", data : err});
                                                            } else {
                                                                TestMessage.aggregate([{ $match: { _id: ObjectId(messageInfo._id) } }, 
                                                                { $lookup: { from: "users", localField: "sender_id", foreignField: "user_id", as: "sender_id" } }],
                                                                function(err, messageInformation) {
                                                                    if(err) {
                                                                        next({"error": true, "message": "Database Error! fetch message info."});
                                                                    } else {
                                                                        if (Array.isArray(messageInformation)) {
                                                                            var messageDetail = messageInformation[0];
                                                                            responseData.users.forEach(userss => {
                                                                                try {
                                                                                    if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                                                                        // sendNotification([userss.user_id[0].notification_token], messageDetail);
                                                                                    }
                                                                                } catch(e){}
                                                                            });
                                                                        
                                                                            //this is for all member in group

                                                                            io.to(groupId).emit("onMessageReceived", messageDetail);

                                                                            //this is for the removed user

                                                                            room.findOne({_id: ObjectId(groupId)},function(err,done){
                                                                                if(err){
                                                                                    
                                                                                }else{
                                                                                    if(removedbyUser == false){
                                                                                        message= "You left";
                                                                                    }else{
                                                                                        message = removedbyUser.first_name + ' ' + removedbyUser.last_name + " removed you";
                                                                                    }
                                                                                    
                                                                                   console.log("socketId of user added is >>>>>>>>",userInfo.socket_id);
                                                                                    io.to(userInfo.socket_id).emit("userRemoved", {room_id: groupId,room_name: done.name, message: message ,user_id: userId});

                                                                                   
                                                                                }
                                                                                      
                                                                                
                                                                            
                                                                            })
                            
                                                                            next(true);
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });
                                                }else {
                                                    next(true);
                                                }
                                            }
                                        });     
                                    }
                                });
                            } else {
                                next(true);
                            }
                        }
                    });
                });
            }
        });
    },

    updateMessageForLike: function(messageId, userId, type, next) {
        if(type == 'Like') {
            TestMessage.updateOne({_id: ObjectId(messageId)}, {$push: {like_id: userId}}, function(err, done) {
                if(err) {
                    next({error: true, message: "Database Error!!updating message for like", data: err});
                } else {
                    TestMessage.updateOne({_id: ObjectId(messageId)}, {$set: {updated_at: Date.now().toString()}}, function(err, updated) {
                        if(err) {
                            next({error: true, message: "Database Error!!updating message for time", data: err});
                        } else {
                            next(true);
                        }
                    });
                }
            });
        } else {
            TestMessage.updateOne({_id: ObjectId(messageId)}, {$pull: {like_id: userId}}, function(err, done) {
                if(err) {
                    next({error: true, message: "Database Error!!updating message for unlike", data: err});
                } else {
                    TestMessage.updateOne({_id: ObjectId(messageId)}, {$set: {updated_at: Date.now().toString()}}, function(err, updated) {
                        if(err) {
                            next({error: true, message: "Database Error!!updating message for time", data: err});
                        } else {
                            next(true);
                        }
                    });
                }
            });
        }
    },

    // called from acceptRequest and denyRequest
    clearInvitations: function(userId, inviteGroupId, next) {
        room.updateOne({_id: ObjectId(inviteGroupId)}, {$pull: {invitations: {receiver_id: ObjectId(userId)}}}, function(err, roomUpdateDone) {
            if(err) {
                next({error: true, message: "Database Error!! update Room"});
            } else {
                user.updateOne({_id: ObjectId(userId)}, {$pull: {invitations: {room_id: ObjectId(inviteGroupId)}}}, function(err, userUpdateDone) {
                    if(err) {
                        next({error: true, message: "Database Error!! update User"});
                    } else {
                        next(true);
                    }
                });
            }
        });
    },

    sendTest: function(req, messageData, room_id, scheduleMessageId, io, mRes) {  
        
        
        let _this = this;
        var sender_id;
        var message_content;
        var file_url;
        var file_size;
        var content_type; 
        var thumbnail_data;
        var parent_msg;
        if(req){
            sender_id = req.body.id;
            message_content = req.body.message_content;
            file_url = req.body.file_url;
            file_size = req.body.file_size;
            content_type = req.body.content_type;
            thumbnail_data = req.body.thumbnail_data;
            thumbnail_data = thumbnail_data.replaceAll(" ", '+');
            parent_msg = req.body.parent_msg;
        }
        else{
            sender_id = messageData.sender_id;
            message_content = messageData.content;
            file_url = messageData.file_url;
            file_size = messageData.file_size;
            content_type = messageData.content_type;
            thumbnail_data = messageData.thumbnail_data;
            thumbnail_data = thumbnail_data.replaceAll(" ", '+');
            parent_msg = messageData.parent_msg;
        }
        
        var parent_msg_id;
        if (parent_msg == "" && parent_msg == undefined)
            parenfalset_msg_id = "";
        else
            parent_msg_id = parent_msg;

        
        if(scheduleMessageId){
          
            ScheduleJob.update({_id : ObjectId(scheduleMessageId)}, {$pull : {room_ids_to_sent : room_id} }, function(err, data){
                if(err){
                   
                }
                else{
                    
                    ScheduleJob.deleteOne({_id : ObjectId(scheduleMessageId) ,room_ids_to_sent : {$size : 0}}, function(err,scheduledJob){
                        if(err){
                         
                        }
                        else{
                           
                        }
                    })

                }
            })
        }

       
        if(room_id == "5b27477e4b20cb8fab0d6b3a"){

            user.find({},function(err, allUsers){
                if(err){
                   
                }
                else{
                   
                    var read = [];
                    read.push(sender_id);
                    var currentTime = Date.now().toString();

                    var messageData = new TestMessage({
                        sender_id: ObjectId(sender_id),
                        room_id: ObjectId(room_id),
                        content_type: content_type,
                        file_url: file_url,
                        file_size: file_size,
                        thumbnail_data: thumbnail_data,
                        retain_count: 0,
                        content: message_content,
                        read_id: read,
                        parent_msg: parent_msg_id,
                        created_at: currentTime,
                        updated_at: currentTime
                    });
                    messageData.save(function(err, messageInfo) {
                        if (err) {
                           
                            mRes(false);
                        } else {

                            allUsers.forEach(user => {
                              
                                if(user.socket_id != "" && user.socket_id != undefined && io.nsps['/'].sockets[user.socket_id] != undefined){
                                    io.nsps['/'].sockets[user.socket_id].join(room_id);
                                }
                                else{
                                    if(user.device_type == "Android"){
                                        sendNotification([user.notification_token], messageInfo);
                                    }
                                    else{
                                        sendTextMessage =""
                                        if(messageInfo.content_type == "text"){
                                            sendTextMessage = messageInfo.content;
                                        }
                                        else if(messageInfo.content_type == "image"){
                                            sendTextMessage = "Photo";
                                        }
                                        sendNotificationForIOS([user.notification_token], messageInfo, sendTextMessage, "message", "News Feed");
                                    }
                                }
                            });     

                            io.to(room_id).emit("onMessageReceived", messageInfo);
                            _this.updateReadIdDeliveredTo(room_id, messageInfo._id, io);
                           
                            mRes(true);
                        }
                    });
                }
            });
        }
        else{
            room.aggregate([{ $unwind: "$users" }, { $match: { $and: [{ _id: ObjectId(room_id) }, { "users.status": "joined" }] } },
                { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                {
                    $group: {
                        '_id': {
                            "_id": "$_id",
                            "name": "$name",
                            "icon_url": "$icon_url",
                            "created_by": "$created_by",
                            "created_at": "$created_at",
                            "is_group": "$is_group",
                            "invitation_blocked": "$invitation_blocked",
                            "history_enable": "$history_enable"
                        },
                        'users': { '$push': '$users' }
                    }
                }
            ], function(err, responseData) {
                if (err) {
                   
                    mRes(false);
                } else {
                    responseData = responseData[0];
                    var read = [];
                    read.push(sender_id);
                    var currentTime = Date.now().toString();

                    var messageData = new TestMessage({
                        sender_id: ObjectId(sender_id),
                        room_id: ObjectId(room_id),
                        content_type: content_type,
                        file_url: file_url,
                        file_size: file_size,
                        thumbnail_data: thumbnail_data,
                        retain_count: (responseData) ? responseData.users.length : 1,
                        content: message_content,
                        read_id: read,
                        parent_msg: parent_msg_id,
                        created_at: currentTime,
                        updated_at: currentTime
                    });
                    messageData.save(function(err, messageInfo) {
                        if (err) {
                            mRes(false);
                        } else {
                            if(responseData){
                                responseData.users.forEach(userss => {
                                  
                                    if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                        if (userss.user_id[0].device_type == "Android")
                                            sendNotification([userss.user_id[0].notification_token], messageInfo);
                                        else {
                                            messageInfo.thumbnail_data = "";
                                        
                                            sendTextMessage =""
                                            if(messageInfo.content_type == "text"){
                                                sendTextMessage = messageInfo.content;
                                            }
                                            else if(messageInfo.content_type == "image"){
                                                sendTextMessage = "Photo";
                                            }
                                            sendNotificationForIOS([userss.user_id[0].notification_token], messageInfo, sendTextMessage, "message", "News Feed");
                                        }
                                    }
                                });
                            }

                           
                            io.to(room_id).emit("onMessageReceived", messageInfo);
                            _this.updateReadIdDeliveredTo(room_id, messageInfo._id, io);
                          
                            mRes(true);
                        }
                    });
                }
            });
        }
    },
    updateReadIdDeliveredTo: function(room_id, messageId, io){
      
        room.findOne({_id : ObjectId(room_id)}, function(err, roomDatForUpdate){
            if(err){
                
            }
            else{
                
                let userIds = [];
                for(i=0; i<roomDatForUpdate.users.length; i++){
                    if(roomDatForUpdate.users[i].status == "left")
                    userIds[i] = roomDatForUpdate.users[i].user_id;
                }
                //set read id and deliverd id
                if(userIds.length > 0){
                    $data = {$push : {read_id : userIds, delivered_to : userIds} } 
                }
                else{
                    $data = ""
                }
                TestMessage.update({_id : ObjectId(messageId)}, $data , function(err, updateMessageData){

                    room.findOne({_id : ObjectId(room_id)}, {users : 1, _id : 0}, function(err, rooms){
                        if(err){
                         
                        }
                        else{
                            let i =0;
                            let arr = [];
                            rooms.users.forEach(element => {
                              
                                if(element.status == "joined"){
                                    TestMessage.find({room_id : ObjectId(room_id), content_type: {$ne : "alert"}, 'read_info.user_id' : {$ne : ObjectId(element.user_id)}, sender_id: {$ne : ObjectId(element.user_id) }, created_at: {$gt : element.join_at} }, function(err, allMessages){
                                        if(err){
                                           
                                        }
                                        else{
                                            if(allMessages){
                                        
                                                arr[i] = {user_id : element.user_id, messages : allMessages};
                                                i++;        
                                               
                                                user.findOne({_id : element.user_id},{device_type : 1, socket_id : 1, notification_token : 1,_id : 0}, function(err, data){
                                                    if(err){
                                                       
                                                    }
                                                    else{
                                                        if(data.socket_id != ""){
                                                           
                                                            io.to(data.socket_id).emit("pendingMessage", {messageData : allMessages});
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                                else{
                                    i++;
                                    
                                }
                            });
                        }
                    });
                });   
            }
        })
    },
    //data will be sent to particular users
    sendTestForCategory : function(req, messageData, category_id, scheduleMessageId, io, mRes){
        room_id = "5b27477e4b20cb8fab0d6b3a"
        let _this = this;
        var sender_id;
        var message_content;
        var file_url;
        var file_size;
        var content_type; 
        var thumbnail_data;
        var parent_msg;
        if(req){
            sender_id = "5b273eec4b20cb8fab0d698f";
            message_content = req.body.message_content;
            file_url = req.body.file_url;
            file_size = req.body.file_size;
            content_type = req.body.content_type;
            thumbnail_data = req.body.thumbnail_data;
            thumbnail_data = thumbnail_data.replaceAll(" ", '+');
            parent_msg = req.body.parent_msg;
        }
        else{
            sender_id = messageData.sender_id;
            message_content = messageData.content;
            file_url = messageData.file_url;
            file_size = messageData.file_size;
            content_type = messageData.content_type;
            thumbnail_data = messageData.thumbnail_data;
            thumbnail_data = thumbnail_data.replaceAll(" ", '+');
            parent_msg = messageData.parent_msg;
        }
        
        var parent_msg_id;
        if (parent_msg == "" && parent_msg == undefined)
            parenfalset_msg_id = "";
        else
            parent_msg_id = parent_msg;

       

        if(scheduleMessageId){
            console.log("scheduleMessage Called");
        
            ScheduleJob.findOne({_id : ObjectId(scheduleMessageId) }, function(err, scheduledMessageData){
                if(err){
                    console.log("err on line 687 ", err);
                }
                else{
                     let category_id1 = scheduledMessageData.categories[0];
                    //  console.log("categories in common method scheduleJob", category_id1 );
                    user.find({categories : ObjectId(category_id1)}, function(err, allUsers){
                        if(err){
                          
                        }
                        else
                        {
                            // console.log("all users in a category",allUsers);
            
                            var read = [];
                            read.push(sender_id);
                            var currentTime = Date.now().toString();
            
                            var messageData = new TestMessage({
                                sender_id: ObjectId(sender_id),
                                room_id: ObjectId(room_id),
                                content_type: content_type,
                                file_url: file_url,
                                file_size: file_size,
                                thumbnail_data: thumbnail_data,
                                retain_count: 0,
                                content: message_content,
                                read_id: read,
                                parent_msg: parent_msg_id,
                                categories:category_id1,
                                created_at: currentTime,
                                updated_at: currentTime
                            });
                            messageData.save(function(err, messageInfo) {
                                if (err) {
                                    mRes(false);
                                } else {
                                   
                                    allUsers.forEach(user => {
                                     
                                        if(user.socket_id != "" && user.socket_id != "undefined" ){
                                            console.log("categories in common method scheduleJob", category_id1 );
                                            io.to(user.socket_id).emit("onMessageReceived", messageInfo)
                                        }
                                        else{
                                            if(user.device_type == "Android"){
                                                sendNotification([user.notification_token], messageInfo);
                                            }
                                            else{
                                                sendTextMessage =""
                                                if(messageInfo.content_type == "text"){
                                                    sendTextMessage = messageInfo.content;
                                                }
                                                else if(messageInfo.content_type == "image"){
                                                    sendTextMessage = "Photo";
                                                }
                                                sendNotificationForIOS([user.notification_token], messageInfo, sendTextMessage, "message", "News Feed");
                                            }
                                        }
                                    });     
            
                                   
                                    // io.to(room_id).emit("onMessageReceived", messageInfo);
                                   
                                    mRes({data : messageInfo});
                                }
                            });
                        }
                    })
                }
            })
            
            ScheduleJob.update({_id : ObjectId(scheduleMessageId)}, {$pull : {room_ids_to_sent : room_id} }, function(err, data){
                if(err){
                   
                }
                else{
                    
                    ScheduleJob.deleteOne({_id : ObjectId(scheduleMessageId) ,room_ids_to_sent : {$size : 0}}, function(err,scheduledJob){
                        if(err){
                           
                        }
                        else{
                           
                        }
                    })
                }
            })
        }
        else{
            user.find({categories : ObjectId(category_id)}, function(err, allUsers){
                if(err){
                  
                }
                else{
                  
                    // console.log("all users in a category",allUsers);
    
                    var read = [];
                    read.push(sender_id);
                    var currentTime = Date.now().toString();
    
                    var messageData = new TestMessage({
                        sender_id: ObjectId(sender_id),
                        room_id: ObjectId(room_id),
                        content_type: content_type,
                        file_url: file_url,
                        file_size: file_size,
                        thumbnail_data: thumbnail_data,
                        retain_count: 0,
                        content: message_content,
                        read_id: read,
                        parent_msg: parent_msg_id,
                        categories:category_id,
                        created_at: currentTime,
                        updated_at: currentTime
                    });
                    messageData.save(function(err, messageInfo) {
                        if (err) {
                            
                            mRes(false);
                        } else {
                           
                            allUsers.forEach(user => {
                                if(user.socket_id != "" && user.socket_id != "undefined" ){
                                  
                                    
                                    io.to(user.socket_id).emit("onMessageReceived", messageInfo)
                                    // io.sockets[user.socket_id].join(room_id)
                                }
                                else{
                                    if(user.device_type == "Android"){
                                        // console.log("this is to be sent android notif >>>>>>>>>>> ")
                                        sendNotification([user.notification_token], messageInfo);
                                    }
                                    else{
                                        sendTextMessage =""
                                        if(messageInfo.content_type == "text"){
                                            sendTextMessage = messageInfo.content;
                                        }
                                        else if(messageInfo.content_type == "image"){
                                            sendTextMessage = "Photo";
                                        }

                                        // console.log("this is to be sent ios >>>>>>>>>>> ")
                                        sendNotificationForIOS([user.notification_token], messageInfo, sendTextMessage, "message", "News Feed");
                                    }
                                }
                            });     
    
                           
                            io.to(room_id).emit("onMessageReceived", messageInfo);
                           
                            mRes({data : messageInfo});
                        }
                    });
                }
            })
        }    
    }
}

exports.data = methods;
