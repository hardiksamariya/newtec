var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var constants = require('../model/constants');
var user = require('../model/users');
var room = require('../model/room');
var sendAlertNotification = require('../sendAlertNotification');
var cryptLib = require('./cryptLib');
var _crypt = new cryptLib();


var methods = {

    authenticateKey: function(req, res) {
       
        if(req.body.api_key != constants.API_KEY)
            res.json({"error": true, "message": "wrong api key"})
        else
            return true
    },

    authenticateToken: function(req, next) {
        // next({"error": true, "message": "wrong api key"});
        if(req.body.api_key != constants.API_KEY) {
            next({"error": true, "message": "wrong api key"});
           
        }
        else {
            var userId = req.body.id;
            ////console.log("old decrypt access token is >>>>>>",req.body.access_token);
            // var a = this.decryptText(req.body.access_token);
            // ////console.log("old decrypt access token is111 >>>>>>",a);
            //CwJagNjVpRmws8Dg4YGdFVLK4Y8ggB5XouNAATmowUVETvtF3r,,vzL2Fx34wRc6KI7DwLWs7Oc59b9EhL3I5EzRGfOi2MtfFj3a8B
            // var encryptedAccessToken = this.encryptText(req.body.access_token);
            var encryptedAccessToken = req.body.access_token;
            // encryptedAccessToken = encryptedAccessToken.split(' ').join('+')
            // next({"error": true, "message": "invalid access token","accesToken":encryptedAccessToken});
            // var encryptedAccessToken = req.body.access_token;
            ////console.log("old encrypt access token is >>>>>>",encryptedAccessToken);
          
            
            user.findOne({_id: ObjectId(userId)}, function(err, resUser) {
                if(err) {
                    next({ "error": true, "message": "Database Error! finding user for accessToken" });
                } else {
                    // next({ "error": encryptedAccessToken, "message": resUser });
                    if(resUser){
                     
                        if(encryptedAccessToken != resUser.access_token) {
                            next({"error": true, "message": "invalid access token","encryptedAccessToken":encryptedAccessToken,"resUser.access_token":resUser.access_token});
                        } else {
                            next(true);
                        }
                    }
                    else{
                       
                        next(false);
                    }
                    
                }
            });
        }
    },

    generateAccessToken: function() {
        var length = 50;
        var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
        // return this.encryptText(result);
    },

    encryptText: function(plainText) {
        var key = _crypt.getHashSha256(constants.SECRET_KEY, 32);
        return _crypt.encrypt(plainText, key, constants.IV)
    },

    decryptText: function(cypherText) {
        var key = _crypt.getHashSha256(constants.SECRET_KEY, 32);
        return _crypt.decrypt(cypherText, key, constants.IV)
    },

    fetchUserInfo: function(userId, next) {
        user.findOne({_id: userId}, function(err, userInfo) {
            if(err) {
                next(false);
            } else {
                if(userInfo) {
                    next(userInfo);
                } else {
                    next(false);
                }
            }
        });
    },

    checkRoomForAdmin: function(groupId, next) {
        if(groupId == constants.PUBLIC_GROUP_ID) {
            next(true);
        } else {
            room.findOne({_id: ObjectId(groupId)}, function(err, groupInfo) {
                if(err) {
                    next(true);
                } else {
                    var deleteStatus = true;
                    groupInfo.users.some(user => {
                        if(user.status == 'joined') {
                            deleteStatus = false;                            
                        }
                    });
                    if(deleteStatus) {
                        room.deleteOne({_id: ObjectId(groupId)}, function(err, deleteDone) {
                            next(true);
                        });
                    } else {
                        var adminStatus = false;
                        groupInfo.users.some(user => {
                            if(user.status == 'joined' && user.is_admin == true) {
                                adminStatus = true;                                
                            }
                        });
                        if(adminStatus == true) {
                            next(true);
                        } else {
                            var userID = '';
                            groupInfo.users.some(user => {
                                if(user.status == 'joined') {
                                    userID = user.user_id;                                    
                                }
                            });
                            
                            room.updateOne({_id: ObjectId(groupId), "users.user_id": userID}, {$set: {"users.$.is_admin": true}}, function(err, groupUpdateDone) {
                                if(err) {
                                    next(true);
                                } else {
                                    user.findOne({_id: userID}, function(err, userInfo) {
                                        var notificationMessage = 'You are now E-Monitor';
                                        sendAlertNotification([userInfo.notification_token], notificationMessage);
                                        next(true);
                                    });
                                }
                            });
                        }
                    }
                }
            });
        }
    }

}

exports.data = methods;