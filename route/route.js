var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

var schedule = require('node-schedule');
//add schema model 
var user = require('../model/users');
var gallery = require('../model/gallery');
var room = require('../model/room');
var Catalogue = require('../model/catalogue');
var message = require('../model/message');
var otp = require('../model/otp');
var report = require('../model/report');
var ScheduleJob = require('../model/scheduleJobs') //for message scheduling
var authentication = require('./authenticate');
var constants = require('../model/constants');
var Category = require('../model/category');
var commonMethods = require('./commonMethods');
var categoryKeywords = require('../model/categoryKeywords');
var review = require('../model/review');

var sendNotification = require('../sendNotification');
var sendAlertNotification = require('../sendAlertNotification');
var sendOtherNotification = require('../sendOtherNotification');
var sendNotificationForIOS = require('../sendNotificationForIOS');
// var sendNotif = require('../sendNotif');

var sendNotificationForIOSVOIP = require('../sendNotificationForIOSVOIP');
var sendNoti = require('../sendNoti');

//cron job and sending response to php
var http = require('http');

var schedule = require('node-schedule');
var request = require('request');
var bodyParser = require('body-parser');

// var bodyParser = require('body-parser');
// app.use(bodyParser.urlencoded({ extended: false }));

var multer = require('multer');
var multerS3 = require('multer-s3');


var aws = require('aws-sdk');
var AWS_ACCESS_KEY = 'AKIAJVEJEHPNQH2XEBQQ'; // This may change later
var AWS_SECRET_KEY = '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'; // this may also change later
var AWS_REGION = 'ap-south-1'; // may change
var S3_BUCKET = 'tecmaths'; // may change
// var uploadImage = require("./config/uploadImage");
aws.config.update({ accessKeyId: AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY, region: AWS_REGION });
s3 = new aws.S3();
var cryptLib = require('./cryptLib');
var _crypt = new cryptLib();

//////////////////////////////////////////

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


module.exports = function (app, io) {
    app.use(bodyParser.urlencoded({ extended: false }));

    app.get('/', function (req, res) {
        res.json({ error: false, message: "server is running" });
    });

    app.post('/userInfo', function (req, res) {

        user_id = req.body.user_id;

        //just to handle crash if user_id not found we will not proceed
        if (user_id) {

            var deviceType = "Android"
            if (typeof req.body.device_type !== 'undefined' && req.body.device_type) {
                deviceType = req.body.device_type
            }
            //removed last_login
            user.update({ _id: ObjectId(user_id) }, {
                $set: {
                    device_type: deviceType, online_status: true, is_login: true,
                    notification_token: req.body.notification_token
                }
            }, function (err, data2) {
                if (err) {
                    //////////////console.log(err, "update user socketid");
                } else {
                    // ////////////console.log(data);
                    user.findOne({ _id: ObjectId(user_id) }, function (err, data1) {
                        if (err) {
                            ////////////console.log(err, "get user info");
                        } else {

                            room.aggregate([{ $unwind: "$users" }, {
                                $match: {
                                    $and: [{ "users.user_id": ObjectId(user_id) },
                                    { "users.status": "joined" }]
                                }
                            }, { $project: { _id: 1 } }], function (err, rooms) {
                                if (err) {
                                    ////////////console.log(err, 'to add to rooms');
                                } else {
                                    rooms.forEach(rms => {
                                        socket.join(rms._id);
                                        room_idsss.push(rms._id);
                                    });
                                }
                            });
                            res.json({ userData: data1 });
                        }
                    });
                }
            });
        }
    })

    // generate and send otp to number
    app.post('/getOtp', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            let phoneNo = req.body.phone;
            if (phoneNo) {
                // var newOtp = 123456;
                var newOtp = Math.floor(100000 + Math.random() * 900000);
                var otpData = new otp({
                    phone: phoneNo,
                    otp: newOtp,
                    created_at: Date.now().toString(),
                    expire_at: (Date.now() + 7200000).toString()
                })

                user.findOne({ phone: phoneNo }, function (err, usr) {
					console.log(err);console.log(usr);
                    if (err) {
                        res.json({ "error": true, "message": "Database Error!" });
                    } else {
                        if (usr && usr.is_blocked) {
                            res.json({ "error": true, "message": "Account BLOCKED!!! Please contact admin." });
                        } else {
                            //Delete past OTPs
                            otp.deleteMany({ phone: phoneNo }, function (err, deleteDone) {
                                if (err) {
                                    res.json({ "error": true, "message": "Database Error!" });
                                } else {
                                    // save otp details in mongo DB
                                    otpData.save(function (err, data) {
                                        if (err) {
                                            res.json({ "error": true, "message": "Database Error!" });
                                        } else {
                                            request.get({
                                                url: 'https://2factor.in/API/V1/dd8f9b32-9c8b-11e7-94da-0200cd936042/SMS/+91' + phoneNo + '/' + newOtp
                                            }, function (error, response, body) {
                                                if (error) {
                                                    res.json({ "error": true, "message": "2Factor Error!" });
                                                } else {
                                                    var twofResponse = JSON.parse(body);
                                                    // //////////////console.log(twofResponse.Status);
                                                    if (twofResponse.Status == "Success") {
                                                        res.json({ "error": false, "message": "New OTP created successfully", two_factor_response: body });
                                                    } else {
                                                        res.json({ "error": true, "message": "2Factor Error!", two_factor_response: body });
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                });

            } else {
                res.json({ "error": true, "messagfinalDatae": "Phone number is not in right format" });
            }
        }
    });

    // verify otp and return user details if old user and delete otp
    app.post('/verifyOtp', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            let phoneNo = req.body.phone;
            let otpText = req.body.otp;
            var deviceType = "";
            if (req.body.device_type == undefined)
                deviceType = "Android";
            else
                deviceType = req.body.device_type;

            if (phoneNo && otpText) {
                otp.findOne({ phone: phoneNo }, function (err, resOtp) {
                    if (err) {
                        res.json({ "error": true, "message": "Database Error!" });
                    } else {
                        if (resOtp) {
                            if (resOtp.otp == otpText) {
                                if (parseInt(resOtp.expire_at) > Date.now()) {
                                    user.findOne({ phone: phoneNo }, function (err, usr) {
                                        if (err) {
                                            res.json({ "error": true, "message": "Database Error! finding user details" });
                                        } else {
                                            //var accessToken = authentication.data.generateAccessToken();
                                            var encryptedAccessToken = authentication.data.generateAccessToken();
                                            if (usr) {
                                                user.update({ phone: phoneNo }, { $set: { access_token: encryptedAccessToken, device_type: deviceType } }, function (err, done) {
                                                    if (err) {
                                                        res.json({ "error": true, "message": "Database Error! updating access token" });
                                                    } else {
                                                        otp.remove({ phone: phoneNo }, function (error, delDone) {
                                                            if (error) {
                                                                res.json({ "error": true, "message": "Database Error! deleting otp" });
                                                            } else {
                                                                res.json({ "error": false, "message": "OTP verified Successfully", "oldUser": true, "accessToken": encryptedAccessToken, "userDetails": usr });
                                                            }
                                                        });
                                                    }
                                                });
                                            } else {
                                                otp.update({ phone: phoneNo }, { $set: { access_token: encryptedAccessToken } }, function (err, done) {
                                                    if (err) {
                                                        res.json({ "error": true, "message": "Database Error! updating access token" });
                                                    } else {
                                                        if (done) {
                                                            res.json({ "error": false, "message": "OTP verified Successfully", "oldUser": false, "accessToken": encryptedAccessToken });
                                                        }
                                                    }
                                                });

                                            }
                                        }
                                    });
                                } else {
                                    res.json({ "error": true, "message": "Sorry, this OTP has been expired" });
                                }
                            } else {
                                res.json({ "error": true, "message": "Incorrect OTP, Please try again" });
                            }
                        } else {
                            res.json({ "error": true, "message": "No otp generated for this phone number" });
                        }
                    }
                });
            } else {
                if (!phoneNo) {
                    res.json({ "error": true, "message": "Provide phone number parameter" });
                } else {
                    res.json({ "error": true, "message": "Provide otp parameter" });
                }
            }
        }
    })

    app.post('/register', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var accessToken = req.body.access_token;
            // var encryptedAccessToken = authentication.data.encryptText(accessToken);
            var encryptedAccessToken = accessToken;
            // //////////////console.log("AccessToken >>>>>>", accessToken)
            // //////////////console.log("AccessToken >>>>>><<<<<<<<", encryptedAccessToken)
            var phoneNo = req.body.phone;
            otp.findOne({ phone: phoneNo }, function (err, resOtp) {
                if (err) {
                    res.json({ "error": true, "message": "Database Error! finding access token" });
                } else {
                    if (resOtp) {
                        if (encryptedAccessToken != resOtp.access_token) {
                            res.json({ "error": true, "message": "access token mismatch!!" });
                        } else {
                            let deviceId = req.body.device_id;
                            let first_name = req.body.first_name;
                            let last_name = req.body.last_name;
                            let profile_image_url = req.body.profile_image_url;
                            let notificationToken = req.body.notification_token;

                            var usr = new user({
                                first_name: first_name,
                                last_name: last_name,
                                phone: phoneNo,
                                profile_image_url: profile_image_url,
                                device_id: deviceId,
                                access_token: encryptedAccessToken,
                                notification_token: notificationToken,
                                rooms: [ObjectId(constants.PUBLIC_GROUP_ID)],
                                // categories: [ObjectId("5bd98b7ffe6e9c4d0d298367"), ObjectId("5bd98b88fe6e9c4d0d298368"), ObjectId("5bd98b94fe6e9c4d0d298369")],
                                created_at: Date.now().toString(),
                                last_login: Date.now().toString()
                            });

                            // save user details
                            usr.save(function (err, data) {
                                if (err) {
                                    res.json({ "error": true, "message": "Database Error! save user details", "err": err });
                                } else {
                                    otp.remove({ phone: phoneNo }, function (error, delDone) {
                                        if (error) {
                                            res.json({ "error": true, "message": "Database Error! deleting otp" });
                                        } else {
                                            user.aggregate({ $match: { _id: data._id } }, { $unwind: "$rooms" }, { $lookup: { from: "rooms", localField: "rooms", foreignField: "_id", as: "rooms" } },
                                                {
                                                    $group: {
                                                        '_id': {
                                                            "_id": "$_id", "first_name": "$first_name", "last_name": "$last_name", "profile_image_url": "$profile_image_url",
                                                            "phone": "$phone", "last_login": "$last_login", "is_blocked": "$is_blocked", "created_at": "$created_at", "online_status": "$online_status", "socket_id": "$socket_id",
                                                            "notification_token": "$notification_token", "blocked": "$blocked", "blocks": "$blocks", "about": "$about", "access_token": "$access_token",
                                                            "muted_room": "$muted_room", "blocked_room": "$blocked_room", "is_login": "$is_login", "invitations": "$invitations", "device_id": "$device_id"
                                                        },
                                                        'rooms': { '$push': '$rooms' }
                                                    }
                                                },
                                                function (err, finalData) {
                                                    if (err) {
                                                        res.json({ "error": true, "message": "Database Error! fetch user details" });
                                                    } else {
                                                        res.json({ "error": false, "message": "user registered successfully", "data": finalData });
                                                    }
                                                });
                                        }
                                    });
                                }
                            });
                        }
                    } else {
                        res.json({ "error": true, "message": "No otp generated for this phone number" });
                    }
                }
            });
        }
    });

    app.post('/addgallery',function(req, res){
        // res.json({"jdfhkds":"adada"});
        var userId = req.body.id;
        var gallery_images = req.body.gallery_images;
        room.findOne({ _id: ObjectId(userId) }, function (err, userData) {
            if (err) {
                res.json({ "error": true, "message": "Database Error! finding group in gallery" });
            } else {
                if (userData) {
                    
                        var gall = new gallery({
                            user_id: userId,
                            created_at:Date.now().toString(),
                            gallery_images: gallery_images
                        });
                        // res.json({"user":usr})
                        // save user details
                        gall.save(function (err, data) {
                            if (err) {
                                res.json({ "error": true, "message": "Database Error! save user details", "err": err });
                            } else {
                                res.json({"Message":"images are uploaded in gallery","data":data});
                            }
                        });
                    
                } else {
                    res.json({ "error": true, "message": "no group found" });
                }
            }
        });
    });

    app.post('/getGallery',function(req, res){
        // res.json({"jdfhkds":"adada"});
        var userId = req.body.id;
        room.findOne({ _id: ObjectId(userId) }, function (err, userData) {
            if (err) {
                res.json({ "error": true, "message": "Database Error! finding user in gallery" });
            } else {
                if (userData) {
                        gallery.find({user_id:userId},function (err, data) {
                            if (err) {
                                res.json({ "error": true, "message": "Database Error! save user details", "err": err });
                            } else {
                                res.json({"data":{"images":data}});
                            }
                        });
                    
                } else {
                    res.json({ "error": true, "message": "no group found" });
                }
            }
        });
    });

    app.post('/fetchUserInfo', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var id = req.body.id;
                user.aggregate({ $match: { _id: ObjectId(id) } }, { $unwind: "$rooms" }, { $lookup: { from: "rooms", localField: "rooms", foreignField: "_id", as: "rooms" } },
                    {
                        $group: {
                            '_id': {
                                "_id": "$_id", "first_name": "$first_name", "last_name": "$last_name", "profile_image_url": "$profile_image_url",
                                "phone": "$phone", "last_login": "$last_login", "is_blocked": "$is_blocked", "created_at": "$created_at", "online_status": "$online_status", "socket_id": "$socket_id",
                                "notification_token": "$notification_token", "blocked": "$blocked", "blocks": "$blocks", "about": "$about", "access_token": "$access_token",
                                "muted_room": "$muted_room", "blocked_room": "$blocked_room", "is_login": "$is_login", "invitations": "$invitations", "device_id": "$device_id"
                            },
                            'rooms': { '$push': '$rooms' }
                        }
                    },
                    function (err, finalData) {
                        if (err) {
                            res.json({ "error": true, "message": "Database Error! fetch user details" });
                        } else {
                            res.json({ "error": false, "message": "user details", "data": finalData });
                        }
                    })
            } else {
                res.json(result);
            }
        })
    });

    app.post('/demo', function (req, res) {
        var at=req.body.access_token;
        var api_key=req.body.api_key;
        var id=req.body.id;
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                res.json({ "error": false, "Api_key": api_key, "access_token":at,"id":id });
            } else {
                res.json(result);
            }
        })
    });

    
    app.post('/fetchUserDetails', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var id = req.body.id;
                var userId = req.body.user_id;
                user.findOne({ _id: ObjectId(userId) }, function (err, finalData) {
                    if (err) {
                        res.json({ "error": true, "message": "Database Error! fetch user details" });
                    } else {
                        res.json({ "error": false, "message": "user details", "data": finalData });
                    }
                })
            } else {
                res.json(result);
            }
        })
    });

    app.post('/fetchGroupInfo', function (req, res) {

        // authentication.data.authenticateToken(req, function (result) {
        //     if (result == true) {
        var groupId = req.body.group_id;
        // //console.log(">>>GI", req.body.group_id);
        // //console.log(">>>>>>>>>>>groupId", groupId);
        if (groupId) {
            try {
                room.aggregate({ $match: { _id: ObjectId(groupId) } }, { $unwind: "$users" },
                    { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                    { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                    {
                        $group: {
                            '_id': {
                                "_id": "$_id","location":"$location","keyword":"$keyword","category_id":"$category_id", "name": "$name","working_hour":"$working_hour","day_of_working":"$day_of_working","cover_photo":"$cover_photo", "group_address": "$group_address", "icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
                                "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "is_deleted": "$is_deleted", "history_enable": "$history_enable", "invitations": "$invitations", "openToAll": "$open_to_all", "updated_at": "$updated_at"
                            },
                            'users': { '$push': '$users' }
                        }
                    },
                    function (err, data) {
                        if (err) {
                            res.json({ "error": true, "message": "Database Error! fetch room details" });
                        } else {
                            res.json({ "error": false, "message": "room details", "data": data });
                        }
                    });
            } catch (err) {

            }
        } else {
            res.json(result);
        }
        // }else{
        //     //console.log("fetchGroupInfo else part");
        // }

        // })
    });

    app.post('/fetchInvitations', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var userId = req.body.id;
                user.aggregate([{ $match: { _id: ObjectId(userId) } }, { $unwind: "$invitations" },
                { $lookup: { from: "users", localField: "invitations.sender_id", foreignField: "_id", as: "invitations.sender_id" } },
                { $lookup: { from: "rooms", localField: "invitations.room_id", foreignField: "_id", as: "invitations.room_id" } },
                { $group: { '_id': { "_id": "$_id" }, 'invitations': { '$push': '$invitations' } } }, { $project: { _id: 0, invitations: 1 } }],
                    function (err, data) {
                        if (err) {
                            res.json({ "error": true, "message": "Database Error! fetch invitations" });
                        } else {
                            res.json({ "error": false, "message": "invitation list", "data": data });
                        }
                    });
            } else {
                res.json(result);
            }
        })
    });

    app.post('/fetchUsersFromContacts', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var groupId = req.body.group_id;
                var contactsList = req.body.contacts_list;
                //////////////console.log(">>>", contactsList);
                user.find({ 'phone': { $in: JSON.parse(contactsList) }, 'rooms': { $nin: [ObjectId(groupId)] } }, function (err, data) {
                    if (err) {
                        res.json({ "error": true, "message": "Database Error!!" });
                    } else {
                        room.aggregate([{ $match: { _id: ObjectId(groupId) } }, { $unwind: "$invitations" },
                        { $lookup: { from: "users", localField: "invitations.sender_id", foreignField: "_id", as: "invitations.sender_id" } },
                        { $lookup: { from: "users", localField: "invitations.receiver_id", foreignField: "_id", as: "invitations.receiver_id" } },
                        { $group: { '_id': { "_id": "$_id" }, 'invitations': { '$push': '$invitations' } } }, { $project: { _id: 0, invitations: 1 } }],
                            function (err, finalData) {
                                if (err) {
                                    res.json({ "error": true, "message": "Database Error!!!" });
                                } else {
                                    res.json({ "error": false, "message": "users list", "data": data, "requests": finalData });
                                }
                            });
                    }
                });
            } else {
                res.json(result);
            }
        })
    });

    app.post('/leaveGroup', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var removedBy = req.body.removed_by;
                var userId = req.body.id;
                var groupId = req.body.group_id;
                var display_name = req.body.display_name;

                commonMethods.data.leaveRoom(userId, groupId, removedBy, io, display_name, function (result2) {
                    if (result2 == true) {
                        res.json({ error: false, message: "User leaved group Successfully!!" });
                    } else {
                        res.json(result2);
                    }
                })
            } else {
                res.json(result1);
            }
        })
    });

     app.post('/joinGroup', function (req, res) {
     	// res.json({"message":"heloooooo"})
        authentication.data.authenticateToken(req, function (result1) {
            // res.json({"message":"heloooooo"})
            if (result1 == true) {
                var addedBy = req.body.added_by;
                var userId = req.body.id;
                var groupId = req.body.group_id;
                var display_name = req.body.display_name;
                // var where = {
                //     $and : [
                //             //  { 
                //                 // $or : [
                //                     {_id: ObjectId(groupId), "users.user_id": ObjectId(userId)}
                //                 // ]
                //             ,{ 
                //                 "users.$.status":"left"
                //               }
                //             ]
                //    }
                // res.json({"message":"heloooooo"})
                var where = {_id: ObjectId(groupId), "users.user_id": ObjectId(userId),"users.$.status":"left"}
                
                room.find(where, function (errs, groupData) {
                    if (errs) {
                        res.json({ "error": true, "message": "Database Error !!"});
                    }else { 
                        if (groupData.length > 0 ) {
                            res.json({ "error": true, "message": "Already member"});
                        }else{
                            // res.json({"message":"sdasdas"})
                             commonMethods.data.joinRoom(userId, groupId, addedBy,display_name,io, '', function (result2) {
                                // res.json({"message":result2})
                                if (result2) {
                                    res.json({ "error": false, "message": "Join Group Successfully!!" });
                                } else {
                                    res.json(result2);
                                }
                            });
                        }
                    }
                });
            } else {
                res.json({"authproble":result1});
            }

        })
    });

    app.post('/createGroup', function (req, res) {
        // res.json({"dfsdf":"sdffs"})
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var userId = req.body.id ? req.body.id : '';
                var previousGroupId = req.body.group_id ? req.body.group_id : '';
                var groupName = req.body.group_name ? req.body.group_name : '';
                var groupIconUrl = req.body.group_image_url ? req.body.group_image_url : '';
                var display_name = req.body.display_name ? req.body.display_name : '';
                var address = req.body.group_address ? req.body.group_address : '';
			    var category_id = req.body.category_id ? req.body.category_id : '';
                var keyword = req.body.keyword ? req.body.keyword : '';
                var working_hour = req.body.working_hour ? req.body.working_hour : '';
                var day_of_working = req.body.day_of_working ? req.body.day_of_working : '';
                var cover_photo = req.body.cover_photo ? req.body.cover_photo : '';
                var one_to_one_group = req.body.one_to_one_group;
				var latitude = req.body.latitude ? parseFloat(req.body.latitude) : '';
				var longitude = req.body.longitude ? parseFloat(req.body.longitude) : '';
				// res.json({"one to one":one_to_one_group})
                if (groupIconUrl == undefined)
                    groupIconUrl = '';
				if(userId != '' && groupName != '' && display_name !='' && address != '' && keyword != '' && latitude != '' && longitude != '') {
					if(!isNaN(latitude) && !isNaN(longitude)) {
						if(typeof latitude == 'number' && typeof longitude == 'number') {
							var roomdata = new room({
								name: groupName,
								icon_url: groupIconUrl,
								created_by: ObjectId(userId),
								users: [{ user_id: ObjectId(userId), is_admin: true, status: "joined", join_at: Date.now().toString(), "display_name": display_name }],
								created_at: Date.now().toString(),
								updated_at: Date.now().toString(),
								group_address: address,
								category_id:category_id,
                                keyword: keyword,
                                working_hour:working_hour,
                                day_of_working:day_of_working,
                                one_to_one_group:one_to_one_group,
                                cover_photo:cover_photo,
								location: {
									type : "Point",
									coordinates : [latitude, longitude]
								}
							});
							roomdata.save(function (err, data) {
								if (err) {
									res.json({ error: true, message: "Database Error!! creating group...", "data": err });
								} else {
									if (data) {
										// room.createIndex( { "location" : "2dsphere" } );
										room.aggregate([{ $match: { _id: ObjectId(data._id) } }, { $unwind: "$users" },
										{ $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
										{ $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
										{
											$group: {
												'_id': {
													"_id": "$_id", "name": "$name", "group_address": "$group_address", "location": "$location","category_id":"$category_id","keyword":"$keyword","working_hour":"$working_hour","day_of_working":"$day_of_working","cover_photo":"$cover_photo","icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
													"is_group": "$is_group", "one_to_one_group":"$one_to_one_group","updated_at": "$updated_at", "invitation_blocked": "$invitation_blocked", "history_enable": "$history_enable"
												}, 'users': { '$push': '$users' }
											}
										}],
										function (err, roomInfo) {
											if (err) {
												res.json({ error: true, message: "Database Error!! fetch group info...", "data": err });
											} else {
												if (Array.isArray(roomInfo)) {
                                                    // res.json({"gfdgd":roomInfo})
                                                    var responseData = roomInfo[0];
													var currentTime = Date.now().toString();
													var messageData = new message({
														sender_id: userId,
														room_id: ObjectId(responseData._id._id),
														content_type: "alert",
														retain_count: responseData.users.length,
														content: responseData._id.created_by[0].first_name + ' ' + responseData._id.created_by[0].last_name + " created group " + responseData._id.name,
														created_at: currentTime,
														updated_at: currentTime
													});

													messageData.save(function (err, messageInfo) {
														if (err) {
															res.json({ error: true, message: "Database Error!! saving message info...", "data": err });
														} else {
															message.aggregate([{ $match: { _id: ObjectId(messageInfo._id) } },
															{ $lookup: { from: "users", localField: "sender_id", foreignField: "_id", as: "sender_id" } }],
															function (err, messageInformation) {
																if (err) {
																	res.json({ error: true, message: "Database Error!! get message info...", "data": err });
																} else {
																	if (Array.isArray(messageInformation)) {
																		var messageDetail = messageInformation[0];
																		responseData.users.forEach(user_data => {
																			//////////console.log("user socket Id",user_data.user_id[0].socket_id);
																			if (user_data.user_id[0].socket_id != "undefined" && user_data.user_id[0].socket_id != '') {
                                                                                // res.json({"message":user_data.user_id[0].socket_id,"jfhdskjf":responseData._id._id})
                                                                                io.nsps['/'].sockets[user_data.user_id[0].socket_id].join(responseData._id._id);
                                                                                io.to(user_data.user_id[0].socket_id).emit("onMessageReceived", messageInfo);
                                                                              
																			} else if (user_data.user_id[0].notification_token != '') {
																				sendNotification([user_data.user_id[0].notification_token], messageInfo);
																			}
																		});
																		user.updateOne({ _id: ObjectId(userId) }, { $push: { rooms: responseData._id._id } }, function (err, userUpdateDone) {
																			if (err) {
																				res.json({ error: true, message: "Database Error!! update user for room", "data": err });
																			} else {
																				res.json({ error: false, message: "group created successfully!!", "data": responseData });
																			}
																		});
																	}
																}
															});
														}
													});
												}
											}
										});
									}
								}
							});
						} else {
							res.json({ error: true, message: "Latitude/longitude must only contain numeric value", data: "" });
						}
					} else {
						res.json({ error: true, message: "Latitude/longitude must only contain numeric value", data: "" });
					}
				} else {
					var errorMsg = [];
					if(userId == '') {
						errorMsg.push('User id is required');
					}
					if(groupName == '') {
						errorMsg.push('Group name is required');
					}
					if(display_name == '') {
						errorMsg.push('Display name is required');
					}
					if(address == '') {
						errorMsg.push('Group address is required');
					}
                    if(category_id == '') {
                        errorMsg.push('Category is required');
                    }
					if(keyword == '') {
						errorMsg.push('Keyword is required');
					}
					if(latitude == '') {
						errorMsg.push('Latitude is required');
					}
					if(longitude == '') {
						errorMsg.push('Longitude is required');
					}
					
					res.json({ error: true, message: errorMsg.join(", "), data: errorMsg });
				}
			} else {
                res.json({ error: true, message: 'Unauthorized access', data: '' });
            }
        });
    });

    app.post('/updateGroupInfo', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var userId = req.body.id ? req.body.id : '';
                var groupId = req.body.group_id ? req.body.group_id : '';
                var groupName = req.body.group_name ? req.body.group_name : '';
                var groupIcon = req.body.group_image_url ? req.body.group_image_url : '';
                var group_address = req.body.group_address ? req.body.group_address : '';
				var category_id = req.body.category_id ? req.body.category_id : '';
                var keyword = req.body.keyword ? req.body.keyword : '';
                var working_hour = req.body.working_hour ? req.body.working_hour : '';
                var day_of_working = req.body.day_of_working ? req.body.day_of_working : '';
                var cover_photo = req.body.cover_photo ? req.body.cover_photo : '';
                var latitude = req.body.latitude ? parseFloat(req.body.latitude) : '';
				var longitude = req.body.longitude ? parseFloat(req.body.longitude) : '';
				
				if(userId != '' && groupId != '' && groupName != '' && group_address != '' && category_id != '' && keyword != '' && latitude != '' && longitude != '') {
					if(latitude != '' && longitude != '') {
						if(typeof latitude == 'number' && typeof longitude == 'number') {
							room.findOne({ _id: ObjectId(groupId) }, function (err, roomInfo) {
								if (err) {
									res.json({ eupdatingrror: true, message: "Database Error!! fetch room info." });
								} else {
									var oldName = roomInfo.name;
									var oldIcon = roomInfo.icon_url;

									room.updateOne({ _id: ObjectId(groupId) }, { $set: { group_address: group_address, name: groupName,category_id:category_id, keyword: keyword,working_hour:working_hour,day_of_working:day_of_working,cover_photo:cover_photo, latitude: latitude, longitude: longitude, icon_url: groupIcon, location: {type : "Point",	coordinates : [latitude, longitude] }, updated_at: Date.now().toString() } },
										function (err, roomUpdateDone) {
											if (err) {
												res.json({ error: true, message: "Database Error!! updating room info." });
											} else {
                                                // res.json("tis inside result2")
												commonMethods.data.getRoom(groupId, (mInfo) => {
													if (mInfo) {
														//////////console.log("it is called in update room line 607 and data is ", mInfo);
														io.to(groupId).emit("updateRoom", { roomId: mInfo });
													}
												});

												user.findOne({ _id: ObjectId(userId) }, function (err, userInfo) {
													if (err) {
														res.json({ error: true, message: "Database Error!! fetch user info." });
													} else {
														room.aggregate([{ $match: { _id: ObjectId(groupId) } }, { $unwind: "$users" },
														{ $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
														{ $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
														{
															$group: {
																'_id': {
																	"_id": "$_id","location":"$location","category_id":"$category_id","keyword":"$keyword","working_hour":"$working_hour","day_of_working":"$day_of_working","cover_photo":"$cover_photo","name": "$name", "icon_url": "$icon_url", "created_by": "$created_by",
																	"created_at": "$created_at", "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "history_enable": "$history_enable", "group_address": "$group_address"
																},
																'users': { '$push': '$users' }
															}
														}],
															function (err, dataaa) {
																if (err) {
																	res.json({ error: true, message: "Database Error!! fetch user info.", data: err });
																} else {
																	if (groupName != oldName || groupIcon != oldIcon) {
																		var responseData = dataaa[0];
																		if (groupName != oldName) {
																			var currentTime = Date.now().toString();
																			var messageDataName = new message({
																				user_id: userId,
																				room_id: ObjectId(groupId),
																				content_type: "alert",
																				retain_count: roomInfo.users.length,
																				content: userInfo.first_name + ' ' + userInfo.last_name + " changed group name from " + oldName + " to " + groupName,
																				created_at: currentTime,
																				updated_at: currentTime,
																				read_id: userId
																			});

																			messageDataName.save(function (err, messageInfo) {
																				if (err) {
																					res.json({ error: true, message: "Database Error!! save name change message", data: err });
																				} else {
																					responseData.users.forEach(userss => {
																						if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
																							sendNotification([userss.user_id[0].notification_token], messageDataName);
																						}
																					});

																					io.to(groupId).emit("onMessageReceived", messageDataName);


																				}
																			});

																		}
																		if (groupIcon != oldIcon) {

																			commonMethods.data.getRoom(groupId, (mInfo) => {
																				if (mInfo) {
																					io.to(groupId).emit("updateRoom", { roomId: mInfo });
																				}
																			});

																			deleteFile(oldIcon);
																			var currentTime = Date.now().toString();
																			var messageData = new message({
																				user_id: userId,
																				room_id: ObjectId(groupId),
																				content_type: "alert",
																				retain_count: roomInfo.users.length,
																				content: userInfo.first_name + ' ' + userInfo.last_name + " changed group's icon",
																				created_at: currentTime,
																				updated_at: currentTime
																			});

																			messageData.save(function (err, messageInfo) {
																				if (err) {
																					res.json({ error: true, message: "Database Error!! save image change message", data: err });
																				} else {
																					try {
																						responseData.users.forEach(userss => {
																							if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
																								sendNotification([userss.user_id[0].notification_token], messageData);
																							}
																						});
																					} catch (e) { }

																					io.to(groupId).emit("onMessageReceived", messageData);
																					// var info = commonMethods.data.getRoom(groupId);



																				}
																			});
																		}
																		commonMethods.data.getRoom(groupId, (updatedroominfo) => {
																			if(updatedroominfo) {
																				//////////console.log("it is called in update room line 607 and data is ", mInfo);
																				res.json({ error: false, message: "Group Info Update Successfully!!",data:updatedroominfo});
																			}else{
																				res.json({ error: false, message: "data not found!!" });
																			}
																		});
																		
																	} else {
																		res.json({ error: false, message: "Nothing to update1!!" });
																	}
                                                                    // res.json({ error: false, message: "Nothing to update!!" });
																}
															});
													}
												});
											}
										})
								}        
							});
						} else {
							res.json({ error: true, message: "Latitude/longitude must only contain numeric value", "data": "" });
						}
					} else {
						res.json({ error: true, message: "Latitude/longitude is required", "data": "" });
					}
				} else {
					var errorMsg = [];
					if(userId == '') {
						errorMsg.push('User id is required');
					}
					if(groupId == '') {
						errorMsg.push('Group Id is required');
					}
					if(groupName == '') {
						errorMsg.push('Group name is required');
					}
					if(group_address == '') {
						errorMsg.push('Group address is required');
					}
                    if(category_id == '') {
                        errorMsg.push('Category is required');
                    }
					if(keyword == '') {
						errorMsg.push('Keyword is required');
					}
					if(latitude == '') {
						errorMsg.push('Latitude is required');
					}
					if(longitude == '') {
						errorMsg.push('Longitude is required');
					}
					res.json({ error: true, message: "Data is missing", "data": errorMsg });
				}
			} else {
                res.json(result);
            }
        });
    });

    app.post('/addMember', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var userId = req.body.id;  //who add the user
                var userName = req.body.name;
                var groupId = req.body.group_id;
                var invitedUserId = req.body.invited_user_id;  //the member who added
                var display_name = req.body.display_name;
                var from_link = req.body.from_link;
                var access_token = req.body.access_token;

                // user.find({}, function (err, data) {
                //     data.forEach(usr => {
                //         var userId = req.body.id;  //who add the user
                //         var userName = req.body.name;
                //         var groupId = req.body.group_id;
                //         var invitedUserId = usr._id;  //the member who added
                //         var display_name = "abcd";
                //         var from_link = "false";
                //         var access_token = usr.access_token;



                //var accTok = authentication.data.encryptText(access_token);

                if (from_link == "true") {
                    //console.log("in true from_link");
                    
                    room.findOne({ _id: ObjectId(groupId) }, function (err, data1) {
                        if (err) {

                        } else {
                            if (data1) {
                                // res.json({ error: false, message: "we are in if" });
                                // room.findOne({invitedUserId : {$in: data1.users}},function(err,data11){
                                //     if(err){
                                //         //console.log("Eror>>>>>>>>>>>>>>>",err);
                                //     }else{
                                //         if(data11){
                                //             //console.log("user found in room already<>>>>>>>>>>",data11);
                                //         }else{
                                user.findOne({ _id: ObjectId(data1.created_by) }, function (err, data2) {
                                    // res.json({ error: false, message:data2});
                                    if (err) {
                                        res.json({ error: false, message: "dont know who created group" });
                                    } else {
                                        if (data2) {
                                            // res.json({ error: false, message:"i am in"  });
                                            //console.log("params acess token is ", accTok);
                                            //console.log("database acess token is ", data2.access_token);
                                            // if (accTok == data2.access_token) {
                                                //console.log("Access token was verified");
                                                // room.findOne({_id: ObjectId(groupId) }, function (err, Info) {
                                                    room.findOne({ _id: ObjectId(groupId) }, function (err, Info) {
                                                    if (err) {
                                                        ////////////console.log("Error in addMember");
                                                        res.json({ error: false, message:Info});
                                                    } else {
                                                        
                                                        if (Info) {

                                                            var users = Info.users;
                                                            if (users.length != 500) {
                                                                
                                                                user.findOne({ _id: ObjectId(invitedUserId) }, function (err, invitedUserInfo) {
                                                                    // res.json({ error: false, message:invitedUserInfo});
                                                                    if (err) {
                                                                        res.json({ error: true, message: "Database Error!! fetch inviteduserInfo", data: err });
                                                                    } else {
                                                                            // res.json({ error: false, message:invitedUserInfo});
                                                                        if (!invitedUserInfo.is_blocked) {
                                                                                // res.json({ error: false, message:invitedUserInfo});
                                                                               commonMethods.data.joinRoom(invitedUserId, groupId, userId, display_name, io, from_link, function (result2) {
                                                                                // res.json({ error: false, message:result2});
                                                                                if (result2 == true) {
                                                                                    var message = display_name + " joined from this group's invited link";
                                                                                    if (data2.device_type == "Android") {
                                                                                        sendAlertNotification([data2.notification_token], message);
                                                                                    }
                                                                                    else {
                                                                                        //console.log("in here here we go line 736");
                                                                                        // sendNotificationForIOS([invitedUserInfo.notification_token], invitedUserInfo, message, "alert", invitedUserInfo.first_name)
                                                                                        //console.log("notification id of data2 is here>>>>>>", data2.notification_token);

                                                                                        sendNotificationForIOS([data2.notification_token], groupId, message, "alert", invitedUserInfo.first_name)
                                                                                    }

                                                                                    res.json({ error: false, message: "You have been added in new group" });
                                                                                } else {
                                                                                    res.json(result2);
                                                                                }
                                                                            });
                                                                        } else {
                                                                            res.json({ error: true, message: "Failed to add User(PE)" });
                                                                        }

                                                                    }
                                                                });
                                                            } else {
                                                                res.json({ error: true, message: "Max limit of 500 users reached" });
                                                            }
                                                        }
                                                    }
                                                    // res.json({ error: false, message:"error"});
                                                });


                                            // } else {
                                            //     res.json({ error: true, message: "invalid access token" });
                                            // }
                                            //         }
                                            //     }
                                            // })
                                        }
                                    }

                                })

                            }
                        }
                    })
                } else {
                    //console.log("groupid is >>>>>>", groupId);
                    room.findOne({ _id: ObjectId(groupId) }, function (err, Info) {
                        if (err) {
                            ////////////console.log("Error in addMember");
                        } else {
                            ////console.log("userInfo in add member is>>>>>>", Info)
                            if (Info) {
                                var users = Info.users;
                                if (users.length != 500) {
                                    user.findOne({ _id: ObjectId(invitedUserId) }, function (err, invitedUserInfo) {
                                        if (err) {
                                            res.json({ error: true, message: "Database Error!! fetch inviteduserInfo", data: err });
                                        } else {

                                            if (!invitedUserInfo.is_blocked) {

                                                commonMethods.data.joinRoom(invitedUserId, groupId, userId, display_name, io, from_link, function (result2) {
                                                    if (result2 == true) {
                                                        user.findOne({ _id: ObjectId(userId) }, function (err, userInfo1) {
                                                            if (err) {

                                                            } else {
                                                                if (userInfo1) {
                                                                    //console.log("userInfo1 is >>>>>>>>>>>>", userInfo1)

                                                                    var message = userName + ' added you';

                                                                    var roomInfo = { "name": Info.name, "_id": Info._id, "created_at": Info.created_at, "created_by": Info.created_by, "open_to_all": Info.open_to_all };

                                                                    var userInfo11 = { "_id": userInfo1._id, "first_name": userInfo1.first_name, "last_name": userInfo1.last_name, "phone": userInfo1.phone, "created_at": userInfo1.created_at, "profile": userInfo1.profile_image_url }

                                                                    var details = { "message": message, "roomInfo": roomInfo, "userInfo": userInfo11 };


                                                                    if (invitedUserInfo.device_type == "Android") {

                                                                        //console.log("notification_token for android is here >>>>>", invitedUserInfo.notification_token);
                                                                        //console.log("notification for the android is here>>>>>>", details);

                                                                        sendAlertNotification([invitedUserInfo.notification_token], details);

                                                                    }
                                                                    else {
                                                                        //console.log("in here here we go line 736");
                                                                        sendNotificationForIOS([invitedUserInfo.notification_token], groupId, message, "message", userInfo1.first_name + ' ' + userInfo1.last_name + ' (' + Info.name + ')')
                                                                    }

                                                                    res.json({ error: false, message: "You have been added in new group" });
                                                                }
                                                            }

                                                        })


                                                    } else {
                                                        res.json(result2);
                                                    }
                                                });
                                            } else {
                                                res.json({ error: true, message: "Failed to add User(PE)" });
                                            }

                                        }
                                    });
                                } else {
                                    res.json({ error: true, message: "Max limit of 500 users reached" });
                                }
                            }

                        }

                    })

                }
            } else {
                res.json(result1);
            }
        })

        //     })
        // })
    });

    app.post('/acceptRequest', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var userId = req.body.id;
                var groupId = req.body.group_id;
                var inviterId = req.body.inviter_id;
                var inviterName = req.body.inviter_name;
                var inviteGroupId = req.body.invite_group_id;

                commonMethods.data.clearInvitations(userId, inviteGroupId, function (result) {
                    if (result == true) {
                        commonMethods.data.leaveRoom(userId, groupId, '', io, '', function (result2) {
                            if (result2) {
                                commonMethods.data.joinRoom(userId, inviteGroupId, inviterId, io, '', function (result3) {
                                    if (result3) {
                                        res.json({ error: false, message: "Invitation accepted successfully!!" });
                                    } else {
                                        res.json(result3);
                                    }
                                });
                            } else {
                                res.json(result2);
                            }
                        });
                    } else {
                        res.json(result);
                    }
                });
            } else {
                res.json(result1);
            }
        })
    });

    app.post('/denyRequest', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var userId = req.body.id;
                var inviteGroupId = req.body.invite_group_id;

                commonMethods.data.clearInvitations(userId, inviteGroupId, function (result2) {
                    if (result2 == true) {
                        res.json({ error: false, message: "Deny Request Successfully!!" });
                    } else {
                        res.json(result2);
                    }
                });
            } else {
                res.json(result1);
            }
        })
    });

    app.post('/blockRequest', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var userId = req.body.id;
                var inviterId = req.body.inviter_id;
                //var inviteGroupId = req.body.invite_group_id;

                user.updateOne({ _id: ObjectId(userId) }, { $push: { blocked: ObjectId(inviterId) } }, function (err, userUpdateDone) {
                    if (err) {
                        res.json({ error: true, message: "Database Error!! user update for blocked", data: err });
                    } else {
                        user.updateOne({ _id: ObjectId(inviterId) }, { $push: { blocks: ObjectId(userId) } }, function (err, updateDone) {
                            if (err) {
                                res.json({ error: true, message: "Database Error!! inviter user update for blocks", data: err });
                            } else {
                                res.json({ error: false, message: "Block Request Successfully!!" });
                            }
                        });
                    }
                });
            } else {
                res.json(result1);
            }
        });
    });

    app.post('/unBlockRequest', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var userId = req.body.id;
                var friendId = req.body.friend_id;

                user.updateOne({ _id: ObjectId(userId) }, { $pull: { blocked: ObjectId(friendId) } }, function (err, userUpdateDone) {
                    if (err) {
                        res.json({ error: true, message: "Database Error!! user update for blocked", data: err });
                    } else {
                        user.updateOne({ _id: ObjectId(friendId) }, { $pull: { blocks: ObjectId(userId) } }, function (err, updateDone) {
                            if (err) {
                                res.json({ error: true, message: "Database Error!! inviter user update for blocks", data: err });
                            } else {
                                res.json({ error: false, message: "Block Request Successfully!!" });
                            }
                        });
                    }
                });
            } else {
                res.json(result1);
            }
        })
    });

    app.post('/removeFromGroup', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var removedUserId = req.body.removed_user_id;
                var userId = req.body.id;
                var groupId = req.body.group_id;
                var display_name = req.body.display_name;

                commonMethods.data.leaveRoom(removedUserId, groupId, userId, io, display_name, function (result2) {
                    if (result2 == true) {
                        user.findOne({ _id: ObjectId(userId) }, function (err, removedByUser) {
                            if (err) {
                                res.json({ error: true, message: "Database Error!! fetch removedby user info" });
                            } else {
                                user.findOne({ _id: ObjectId(removedUserId) }, function (err, removedUsrInfo) {
                                    if (err) {
                                        res.json({ error: true, message: "Database Error!! fetch user info" });
                                    } else {
                                        if (removedUsrInfo.is_login) {
                                            if (removedUsrInfo.socket_id != '') {



                                                commonMethods.data.getRoom(groupId, (mInfo) => {
                                                    if (mInfo) {
                                                        io.to(removedUsrInfo.socket_id).emit("updateRoom", { roomId: mInfo });
                                                    }
                                                });
                                            }
                                        }

                                        room.findOne({ _id: ObjectId(groupId) }, function (err, gInfo) {
                                            if (err) {

                                            } else {
                                                if (gInfo) {

                                                    var notificationMessage = removedByUser.first_name + ' ' + removedByUser.last_name + ' has removed you.';

                                                    var notificationMessage1 = removedByUser.first_name + ' ' + removedByUser.last_name + ' has removed you.';

                                                    var gDetails = { "open_to_all": gInfo.open_to_all, "name": gInfo.name, "created_at": gInfo.created_at, "_id": gInfo._id, "created_by": gInfo.created_by };

                                                    if (removedUsrInfo.device_type == "Android") {
                                                        // ////////////console.log("in here123")

                                                        // sendAlertNotification([removedUsrInfo.notification_token], notificationMessage);

                                                        sendNoti([removedUsrInfo.notification_token], notificationMessage1, gDetails);
                                                    }
                                                    else {
                                                        // ////////////console.log("in her e here we go line 876");
                                                        sendNotificationForIOS([removedUsrInfo.notification_token], groupId, notificationMessage, "message", removedByUser.first_name + ' ' + removedByUser.last_name + ' (' + gInfo.name + ') ')
                                                    }
                                                    res.json({ error: false, message: "User removed Successfully!!" });

                                                }

                                            }

                                        })

                                    }
                                });
                            }
                        });
                    } else {
                        res.json(result2);
                    }
                })
            } else {
                res.json(result1);
            }
        });
    });

    app.post('/muteRoom', function (req, res) {
        // authentication.data.authenticateToken(req, function (result1) {
        //     if (result1 == true) {
        var userId = req.body.id;
        var room_id = req.body.roomId;
        //var inviteGroupId = req.body.invite_group_id;
        //console.log("muteRoom called>>>>>>>>>>>>>>>>>>>>>>>>>>");

        user.findOne({ _id: ObjectId(userId), muted_room: ObjectId(room_id) }, function (err, mutedData) {
            if (err) {
                //console.log("Error occurred ", err);
            }
            else {
                if (!mutedData) {
                    user.updateOne({ _id: ObjectId(userId) }, { $push: { muted_room: ObjectId(room_id) } }, function (err, userUpdateDone) {
                        if (err) {
                            //console.log("Error>>>>>>>>>>>>>>>>", err);
                            res.json({ error: true, message: "Database Error!! user update for room mute", data: err });
                        } else {
                            res.json({ error: false, message: "Room Muted Successfully!!" });
                        }
                    });
                }
                else {
                    res.json({ error: true, message: "Already Muted." })
                }
            }
        })

        //     } else {
        //         res.json(result1);
        //     }
        // });
    });

    app.post('/unmuteRoom', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var userId = req.body.id;
                var room_id = req.body.roomId;
                //var inviteGroupId = req.body.invite_group_id;

                //console.log("unMuteRoom called>>>>>>>>>>>>>>>>>>>>>>>>>>");

                user.findOne({ _id: ObjectId(userId), muted_room: ObjectId(room_id) }, function (err, mutedData) {
                    if (err) {
                        //console.log("Error occurred ", err);
                    }
                    else {
                        if (mutedData) {
                            user.updateOne({ _id: ObjectId(userId) }, { $pull: { muted_room: ObjectId(room_id) } }, function (err, userUpdateDone) {
                                if (err) {
                                    res.json({ error: true, message: "Database Error!! user update for muted", data: err });
                                } else {
                                    res.json({ error: false, message: "Room Unmute Successfully!!" });
                                }
                            });
                        }
                        else {
                            res.json({ error: true, message: "Not nuted earlier" })
                        }
                    }
                })

            } else {
                res.json(result1);
            }
        });
    });

    app.post("/sendMessage", function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
                res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
                console.log("urlencoded text in sendMessage>>>>>>>>",req.body);
                var sender_id = req.body.id;
                var room_id = req.body.room_id;
                var message_content = req.body.message_content;
                var file_url = req.body.file_url;
                var file_size = req.body.file_size;
                var content_type = req.body.content_type;
                var thumbnail_data = req.body.thumbnail_data;

                console.log("room id in sendMessage>>>>>>>>>", room_id);

                const used = process.memoryUsage().heapUsed / 1024 / 1024;
                console.log(`The script uses approximately>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${used} MB`);

                thumbnail_data = thumbnail_data.replaceAll(" ", '+');
                var parent_msg = req.body.parent_msg;
                var parent_msg_id;
                if (parent_msg == "" && parent_msg == undefined)
                    parenfalset_msg_id = "";
                else
                    parent_msg_id = parent_msg;

                room.aggregate([{ $unwind: "$users" }, { $match: { $and: [{ _id: ObjectId(room_id) }, { "users.status": "joined" }] } },
                { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                {
                    $group: {
                        '_id': {
                            "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
                            "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "history_enable": "$history_enable"
                        },
                        'users': { '$push': '$users' }
                    }
                }], function (err, responseData) {
                    date = new Date();
                    
                    if (err) {
                        res.json({ error: true, message: "err" });
                    } else {
                        ////console.log("Send Message Parent Message Id ", parent_msg_id);

                        responseData = responseData[0];
                        // res.json({"ResponseData":responseData});
                        console.log("responseData  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", responseData);
                        // res.json({"mmmm":responseData})
                        //create read id's
                        var read = [];
                        read.push(sender_id);

                        //create delivered to
                        let deliveredTo = [];
                        deliveredTo.push(sender_id);


                        readInfoObject = [];
                        // res.json({"mmmm":"dfdsfsdf"})
                        responseData.users.forEach(element => {
                            // console.log("element.user_id[0]._id"+element.user_id[0]._id);
                            // res.json({"mmmm":element.user_id[0]._id})
                            if (element.user_id[0]._id != sender_id) {
                                obj = { user_id: ObjectId(element.user_id[0]._id), status: "pending", date: new Date().getTime().toString() }
                                readInfoObject.push(obj);
                            }
                        })


                        // //console.log("m content in message is>>>>>>>>>",message_content);
                        var currentTime = Date.now().toString();
                        var messageData = new message({
                            sender_id: ObjectId(sender_id),
                            room_id: ObjectId(room_id),
                            content_type: content_type,
                            file_url: file_url,
                            file_size: file_size,
                            thumbnail_data: thumbnail_data,
                            retain_count: responseData.users.length,
                            content: message_content,
                            read_id: read,
                            delivered_to: deliveredTo,
                            parent_msg: parent_msg_id,
                            read_info: readInfoObject,
                            created_at: currentTime,
                            updated_at: currentTime
                        });

                        messageData.save(function (err, messageInfo) {
                            if (err) {
                                res.json({ error: true, message: err });
                            } else {

                                commonMethods.data.updateReadIdDeliveredTo(room_id, messageInfo._id, io);
                                res.json({ error: false, message: messageInfo });
                                // ////////////console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>> saving data ", messageInfo);
                                //get the joined memebers of this room
                                room.findOne({ _id: ObjectId(room_id) }, { users: 1, _id: 0 }, function (err, rooms) {

                                    //iterate the user array
                                    var iosUserNotificationToken = [];
                                    var androidUserNotificationToken = [];

                                    var count = rooms.users.length;
                                    // res.json({"roomdata":rooms})

                                    for (let i = 0; i < rooms.users.length; i++) {

                                        //if user is joined
                                        if (rooms.users[i].status == 'joined') {
                                            // res.json({"1513":"1513"});
                                            user.findOne({ _id: ObjectId(rooms.users[i].user_id) }, function (err, userData) {
                                                console.log("Response >>>>>>>>>>>>", count);
                                                console.log("Response >>>>>>>>>>>>", userData);
                                                if (err) {
                                                    count--;
                                                    console.log("5 Checking count value ", count);
                                                    if (count <= 0) {
                                                        callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                    }
                                                }
                                                if (!userData) {
                                                    count--;
                                                    console.log("6 Checking count value ", count);
                                                    if (count <= 0) {
                                                        callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                    }
                                                }

                                                //if user is online
                                                if (userData.online_status == true && userData.socket_id != '') {
                                                    //send message to socket
                                                    // res.json({"messageInfo in sendMessage>>>>>>>>":userData.socket_id});
                                                    console.log("4 Checking count value ", count);
                                                    io.to(userData.socket_id).emit("onMessageReceived", messageInfo);
                                                    
                                                    count--;
                                                    console.log("4 Checking count value ", count);
                                                    if (count <= 0) {
                                                        callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                    }
                                                }
                                                else {
                                                    if (userData.device_type == "Android") {
                                                        user.findOne({ _id: ObjectId(sender_id) }, function (err, senderData) {
                                                            if (err) {
                                                                console.log("33 Checking count value ");
                                                            } else {
                                                                //console.log("finding user in send message>>>>>>>>>>>>>>>>", senderData);
                                                                count--;
                                                                if (senderData) {
                                                                    //console.log("finding user in send message if user find>>>>>>>>>>>>>>>>", senderData);
                                                                    if (userData.notification_token != senderData.notification_token) {
                                                                        console.log("finding user in send message>>>>>>>>>>>>>>>>", senderData);
                                                                        //if (userData.muted_room.indexOf(room_id) < 0) {

                                                                        androidUserNotificationToken.push(userData.notification_token);


                                                                        console.log("her we go line 1229", androidUserNotificationToken);
                                                                        console.log("3 Checking count value ", count);
                                                                        if (count <= 0) {
                                                                            callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                                        }

                                                                        // } else {
                                                                        //     if (count <= 0) {
                                                                        //         callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                                        //     }
                                                                        // }
                                                                    }
                                                                    else {
                                                                        if (count <= 0) {
                                                                            callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                                        }
                                                                    }
                                                                }
                                                                else {
                                                                    androidUserNotificationToken.push(userData.notification_token);
                                                                    console.log("3 Checking count value ", count);
                                                                    if (count <= 0) {
                                                                        callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                                    }
                                                                }
                                                            }
                                                        })
                                                    } else {

                                                        user.findOne({ _id: ObjectId(sender_id) }, function (err, senderData) {
                                                            if (err) {
                                                                //console.log("22 Checking count value "); 
                                                            } else {
                                                                count--;
                                                                if (userData.notification_token != senderData.notification_token) {

                                                                    // console.log("In if of the muted room1111", userData.muted_room.indexOf(room_id), "groupId>>>>>>>", room_id, "mutedRoom>>>>>>>>>>>>", userData.muted_room);

                                                                    // if (userData.muted_room.indexOf(room_id) < 0) {
                                                                    //console.log("In if of the muted room", userData.muted_room.indexOf(room_id), "groupId>>>>>>>", room_id, "mutedRoom>>>>>>>>>>>>", userData.muted_room);
                                                                    console.log("hello"+userData.notification_token);
                                                                    iosUserNotificationToken.push(userData.notification_token);
                                                                    console.log("her we go line 1229", iosUserNotificationToken);

                                                                    // res.json({"1 Checking count value ":count})
                                                                    if (count <= 0) {
                                                                        callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                                    }

                                                                    // } else {
                                                                    //     if (count <= 0) {
                                                                    //         callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                                                    //     }
                                                                    // }
                                                                }

                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        } else {
                                            count--;
                                            console.log("1 Checking count value ", count);
                                            // res.json({"1 Checking count value ":count});
                                            if (count <= 0) {
                                                // res.json({"dfsfd":"dhd"});
                                                callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id)
                                            }
                                        }
                                    }

                                    function callMe(iosUserNotificationToken, androidUserNotificationToken, sender_id) {

                                        //console.log("sending notification>>>>>>>messageInfo", messageInfo);
                                        // console.log("sending notification>>>>>>>iosnotifi", iosUserNotificationToken);
                                        // console.log("sending notification>>>>>>>androidnotifi", androidUserNotificationToken);
                                        // console.log("sending notification>>>>>>>androidnotifi", sender_id);

                                        user.findOne({ _id: ObjectId(sender_id) }, function (err, senderData) {

                                            console.log("here we go line 1253");
                                           
                                            if (err) {
                                                console.log("error occurred on line 1264 ", err);
                                            }
                                            else {
                                                messageInfo.thumbnail_data = "";
                                                if (content_type == "text") { }
                                                else {
                                                    if (content_type == "image") {
                                                        message_content = "📷 Photo";
                                                    }
                                                    else {
                                                        if (content_type == "video") {
                                                            message_content = "📹 Video";
                                                        }
                                                        else {
                                                            message_content = "📄 Document";
                                                        }
                                                    }
                                                }
                                                console.log("in th line 1266");
                                                // res.json({"notify":"bnotify"});
                                                sendNotification(androidUserNotificationToken, messageInfo);
                                                
                                                console.log("lenght===="+androidUserNotificationToken.length);

                                                for (let k = 0; k < iosUserNotificationToken.length; k++) {
                                                    console.log("new one done"+iosUserNotificationToken[k]);
                                                    // var message = { 
                                                    //     app_id: "6cd39ff4-2cd7-4227-a90d-0333a3e900e6",
                                                    //     content_available: true,
                                                    //     data: messageInfo,
                                                    //     content:messageInfo,
                                                    //     include_player_ids: [ '4a62f523-c48c-4bc8-9e7f-c785d772587b' ]
                                                    //   };
                                                    // sendNotificationForIOS(message);
                                                    var name_check = responseData._id.name;
                                                    var is_single_chat = true;
                                                    if(name_check.indexOf('(') > -1)
                                                    {
                                                        // is_single_chat=true;
                                                        sendNotificationForIOS(iosUserNotificationToken[k], messageInfo, message_content, "message", responseData._id.name, is_single_chat);
                                                    }else{
                                                        is_single_chat=false;
                                                        // console.log(messageInfo);
                                                        sendNotificationForIOS(iosUserNotificationToken[k], messageInfo, message_content, "message", senderData.first_name + " " + senderData.last_name + "(" + responseData._id.name + ")", is_single_chat);
                                                    }
                                                }

                                            }
                                        });

                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                res.json(result1);
            }
        });
    });

    app.post('/likeMessage', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var userId = req.body.id;
                var messageId = req.body.message_id;
                var type = req.body.type;

                message.findOne({ _id: ObjectId(messageId) }, function (err, info) {
                    if (err) {
                        ////////////console.log("Error");
                    } else {
                        if (info == null) {
                            res.json({ error: true, message: "Message not found" });
                            return;
                        } else {

                            commonMethods.data.updateMessageForLike(messageId, userId, type, function (result1) {
                                if (result1 == true) {
                                    message.findOne({ _id: ObjectId(messageId) }, function (err, messageInfo) {
                                        if (err) {
                                            res.json({ error: true, message: "Database Error!! fetch message info", data: err });
                                        } else {
                                            //////////////console.log(messageInfo);   
                                            //res.json({ error: false, message: type + " Successfully", data: messageInfo }); 

                                            let user_ids = messageInfo.like_id;
                                            let room_id = messageInfo.room_id;

                                            room.aggregate([{ $unwind: "$users" }, { $match: { $and: [{ _id: ObjectId(messageInfo.room_id) }, { "users.status": "joined" }] } },
                                            { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                                            { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                                            {
                                                $group: {
                                                    '_id': {
                                                        "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
                                                        "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "history_enable": "$history_enable"
                                                    },
                                                    'users': { '$push': '$users' }
                                                }
                                            }],
                                                function (err, responseData) {
                                                    if (err) {
                                                        res.json({ error: true, message: "Database Error!! fetch room info", data: err });
                                                    } else {
                                                        responseData = responseData[0];

                                                        responseData.users.forEach(userss => {
                                                            if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                                                if (userss.user_id[0].device_type == "Android") {
                                                                    sendNotification([userss.user_id[0].notification_token], messageInfo);
                                                                }
                                                                // else{
                                                                //     sendNotificationForIOS([userss.user_id[0].notification_token], messageInfo, userName + ' made ' + monitorInfo.first_name + ' ' + monitorInfo.last_name + ' E-Monitor' , "message", monitorInfo.first_name);
                                                                // }
                                                            }
                                                        });

                                                        ////////////console.log("room id is this ", messageInfo.room_id);
                                                        io.to(messageInfo.room_id).emit("updateLike", { user_ids: user_ids, message_id: messageId, room_id: room_id });
                                                        ////////////console.log("hey>>>>",'>>>>>>>>>>>>>>>>>>>>>>',messageId,'>>>>>>>>>>>>>>>>>>>>>>',room_id);
                                                        res.json({ error: false, message: type + " Successfully", data: messageInfo });
                                                    }
                                                });
                                        }
                                    });
                                } else {
                                    res.json(result1);
                                }
                            })
                        }
                    }
                })

            } else {
                res.json(result);
            }
        });
    });

    var groupCount = 0;
    app.post('/updateProfile', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var userId = req.body.id;
                var firstName = req.body.first_name;
                var lastName = req.body.last_name;
                var imageUrl = req.body.image_url;
                var usrAbout = req.body.about;

                user.findOne({ _id: ObjectId(userId) }, function (err, userInfo) {
                    if (err) {
                        res.json({ error: true, message: "Databasnexte Error!! fetch user info" });
                    } else {
                        var oldFirstName = userInfo.first_name;
                        var oldLastName = userInfo.last_name;
                        var oldImageUrl = userInfo.profile_image_url;

                        user.updateOne({ _id: ObjectId(userId) }, { $set: { first_name: firstName, last_name: lastName, profile_image_url: imageUrl, about: usrAbout } },
                            function (err, updateDone) {
                                if (err) {
                                    res.json({ error: true, message: "Database Error!! updating user info" });
                                } else {
                                    // delete previous image from s3
                                    if (oldImageUrl.length > 45) {
                                        if (oldImageUrl != imageUrl) {
                                            deleteFile(oldImageUrl);
                                        }
                                    }

                                    userInfo.rooms.forEach(roomId => {
                                        updateProfileEvent(roomId);
                                    });
                                    // if(firstName != oldFirstName || lastName != oldLastName) {
                                    //     let i = 1;   
                                    //     groupCount = userInfo.rooms.length;
                                    //     userInfo.rooms.forEach(roomId => {                                                                                                             
                                    //         sendUpdateProfileMessage(i, userId, roomId, oldFirstName, oldLastName, firstName, lastName, function(result2) {
                                    //             if(result2 == groupCount) {
                                    //                 ////////////console.log("UPDATEPROFILE => ", groupCount + ' - ' + result2);
                                    //                 res.json({error: false, message: "Profile updated sucessfully!!"});
                                    //             }
                                    //         });
                                    //         i++;                                        
                                    //     }); 
                                    // } else {
                                    res.json({ error: false, message: "Profile updated sucessfully!!" });
                                    // }                                                   
                                }
                            });
                    }
                });
            } else {
                res.json(result);
            }
        });
    });

    app.post('/makeNewMonitor', function (req, res) {
        //console.log("makeNewMonitor is called>>>>>>>>");
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var userId = req.body.id;
                var userName = req.body.user_name;
                var groupId = req.body.group_id;
                var monitorId = req.body.new_monitor_id;
                var display_name = req.body.display_name;

                room.updateOne({ _id: ObjectId(groupId), "users.user_id": ObjectId(monitorId) }, { $set: { "users.$.is_admin": true, updated_at: Date.now().toString() } }, function (err, updateDone) {
                    if (err) {
                        res.json({ error: true, message: "Database Error!! room update", data: err });
                    } else {

                        commonMethods.data.getRoom(groupId, (mInfo) => {
                            if (mInfo) {
                                io.to(groupId).emit("updateRoom", { roomId: mInfo });
                            }
                        });

                        user.findOne({ _id: monitorId }, function (err, monitorInfo) {
                            if (err) {
                                res.json({ error: true, message: "Database Error!! fetch new monitor info", data: err });
                            } else {
                                room.aggregate([{ $match: { _id: ObjectId(groupId) } }, { $unwind: "$users" },
                                { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                                { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                                {
                                    $group: {
                                        '_id': {
                                            "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by",
                                            "created_at": "$created_at", "is_group": "$is_group", "invitation_blocked": "$invitation_blocked",
                                            "history_enable": "$history_enable"
                                        }, 'users': { '$push': '$users' }
                                    }
                                }], function (err, dataaa) {
                                    if (err) {
                                        res.json({ error: true, message: "Database Error!! fetch room info" });
                                    } else {
                                        if (dataaa) {
                                            var responseData = dataaa[0];
                                            var currentTime = Date.now().toString();
                                            var messageData = new message({
                                                sender_id: ObjectId(userId),
                                                room_id: ObjectId(groupId),
                                                content_type: "alert",
                                                retain_count: responseData.users.length,
                                                content: userName + ' made ' + display_name + ' E-Monitor',
                                                created_at: currentTime,
                                                updated_at: currentTime
                                            });

                                            messageData.save(function (err, messageInfo) {
                                                if (err) {
                                                    next({ error: true, message: "Database Error!! save message", data: err });
                                                } else {
                                                    responseData.users.forEach(userss => {
                                                        if (userss.user_id[0].socket_id != '' && userss.status == 'joined') {
                                                            io.to(userss.user_id[0].socket_id).emit("onMessageReceived", messageInfo);

                                                        } else if (userss.user_id[0].notification_token != '' && userss.status == 'joined' && userss.user_id[0]._id != monitorInfo._id) {

                                                            //console.log("hello from userInfo1111>>>>>>>>>", userss.user_id[0].muted_room, "groupId>>>>>>>>>>>>>>>", userss.user_id[0].muted_room.indexOf(groupId), "grollsk;;km>>>>>>>", groupId);

                                                            // if (userss.user_id[0].muted_room.indexOf(groupId) < 0 ) {

                                                            // //console.log("hello from userInfo>>>>>>>>>", userss.user_id[0].muted_room, "groupId>>>>>>>>>>>>>>>",userss.user_id[0].muted_room.indexOf(groupId),"grollsk;;km>>>>>>>",groupId);
                                                            sendNotification([userss.user_id[0].notification_token], messageInfo);


                                                            sendNotificationForIOS([userss.user_id[0].notification_token], messageInfo, userName + ' made ' + display_name + ' E-Monitor111', "message", monitorInfo.first_name);
                                                            // } else {

                                                            // }

                                                        }
                                                    });
                                                    if (monitorInfo.device_type == "Android") {
                                                        // if (monitorInfo.muted_room.indexOf(groupId) < 0) {
                                                        sendAlertNotification([monitorInfo.notification_token], userName + ' made you E-Monitor');
                                                        // }

                                                    } else {
                                                        // if (monitorInfo.muted_room.indexOf(groupId) < 0) {
                                                        //console.log("hello from monitorinfo>>>>>>>>>", monitorInfo.muted_room, "groupId>>>>>>>>>", groupId, "grollsk;;km>>>>>>>", groupId);
                                                        sendNotificationForIOS([monitorInfo.notification_token], messageInfo, userName + ' made ' + display_name + ' E-Monitor111', "message", monitorInfo.first_name);
                                                        // }

                                                    }

                                                    res.json({ error: false, message: "Made E-Monitor successfully!!" });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                res.json(result);
            }
        });
    });

    app.post('/reportUser', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var byUserId = req.body.id;
                var toUserId = req.body.to_user_id;
                var groupId = req.body.group_id;
                var reportedFor = req.body.reported_for;
                if (reportedFor != undefined) {
                    if (reportedFor == null) {
                        reportedFor = "";
                    }
                } else {
                    reportedFor = "";
                }

                if (reportedFor == null) {
                    reportedFor = "";
                }


                var reportData = new report({
                    by_user_id: ObjectId(byUserId),
                    to_user_id: ObjectId(toUserId),
                    room_id: ObjectId(groupId),
                    reported_for: reportedFor,
                    created_at: Date.now().toString()
                });

                reportData.save(function (err, done) {
                    if (err) {
                        res.json({ error: true, message: "Database Error!! save report", data: err });
                    } else {
                        res.json({ error: false, message: "User Reported Successfully!!" });
                    }
                })
            } else {
                res.json(result);
            }
        });
    });

    app.post('/reportMessage', function (req, res) {
        // authentication.data.authenticateToken(req, function(result) {
        //     if (result == true) {
        var byUserId = req.body.id;
        var toUserId = req.body.to_user_id;
        var groupId = req.body.group_id;
        var reportedFor = "message";
        var messageId = req.body.message_id;
        var messageContent = req.body.message_content;

        if (reportedFor != undefined) {
            if (reportedFor == null) {
                reportedFor = "";
            }
        } else {
            reportedFor = "";
        }

        if (reportedFor == null) {
            reportedFor = "";
        }

        var reportData = new report({
            by_user_id: ObjectId(byUserId),
            to_user_id: ObjectId(toUserId),
            room_id: ObjectId(groupId),
            reported_for: reportedFor,
            message_id: messageId,
            message_content: messageContent + "",
            created_at: Date.now().toString()
        });

        reportData.save(function (err, done) {
            if (err) {
                res.json({ error: true, message: "Database Error!! save report", data: err });
            } else {
                res.json({ error: false, message: "Message Reported Successfully!!" });
            }
        })
        // } else {
        //     res.json(result);
        // }
        // });
    })

    app.post('/fetchBlockUsers', function (req, res) {
        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var userId = req.body.id;
                user.aggregate({ $match: { _id: ObjectId(userId) } }, { $unwind: "$blocks" },
                    { $lookup: { from: "users", localField: "blocks", foreignField: "_id", as: "blocks" } },
                    { $group: { '_id': {}, 'blocks': { '$push': '$blocks' } } }, { $project: { '_id': 0, 'blocks': 1 } },
                    function (err, blocksResult) {
                        if (err) {
                            res.json({ error: true, message: "Database Error!! fetch blocks", data: err });
                        } else {
                            user.aggregate({ $match: { _id: ObjectId(userId) } }, { $unwind: "$blocked" },
                                { $lookup: { from: "users", localField: "blocked", foreignField: "_id", as: "blocked" } },
                                { $group: { '_id': {}, 'blocked': { '$push': '$blocked' } } }, { $project: { '_id': 0, 'blocked': 1 } },
                                function (err, blockedResult) {
                                    if (err) {
                                        res.json({ error: true, message: "Database Error!! fetch blocked", data: err });
                                    } else {
                                        var blocksArray = [];
                                        var blockedArray = [];
                                        if (blocksResult.length > 0) {
                                            blocksArray = blocksResult[0].blocks[0];
                                        }
                                        if (blockedResult.length > 0) {
                                            blockedArray = blockedResult[0].blocked[0];
                                        }
                                        res.json({ error: false, message: "blocks", blocks: blocksArray, blocked: blockedArray });
                                    }
                                });
                        }
                    });
            } else {
                res.json(result);
            }
        });
    });

    // upload profile pic image
    app.post('/uploadProfileImage', function (req, res) {
        //var timeStamp = Date.now().toString();
        var s3key = "ProfilePictures/" + 'image__' + Date.now() + '_';
        var s3keyURL = "ProfilePictures/" + 'image__' + Date.now() + '_';
        var taggingValue = "auto-delete=false";

        /////////// Config for S3 Bucket with naming convention
        var upload = multer({
            storage: multerS3({
                s3: s3,
                bucket: 'tecmaths',
                acl: 'public-read',
                tagging: taggingValue,
                contentType: multerS3.AUTO_CONTENT_TYPE,

                metadata: function (req, file, cb) {
                    //////////////console.log("testing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ", file.fieldname);
                    cb(null, { fieldName: file.fieldname });
                },
                key: function (req, file, cb) {
                    s3keyURL = s3key + file.originalname;
                    //////////////console.log('fileUrl>>>>>>>>>>>>>>>>>', s3keyURL);
                    cb(null, s3keyURL);
                }
            })
        });
        ////////// End of Config for S3 Bucket

        ////////// Actual upload to S3
        upload.single('file')(req, res, function (err, data) {
            if (err) {
                res.json({ "error": true, "message": "Some error while uploading files.", "data": err })
                return;
            }
            //////////////console.log("testing >>>>>>>>>>>>>>>>>>> ", req.file);
            var fileUrl = 'https://s3.ap-south-1.amazonaws.com/tecmaths/' + s3keyURL;
            res.json({ "error": false, "message": "File upload successfully", "file_url": fileUrl });
            ///////// End Upload to s3//////
        });

    });

    // upload Groups pic image
    app.post('/uploadGroupImage', function (req, res) {
        //var timeStamp = Date.now().toString();
        var s3key = "Groups/" + 'image__' + Date.now() + '_';
        var s3keyURL = "Groups/" + 'image__' + Date.now() + '_';
        var taggingValue = "auto-delete=false";

        /////////// Config for S3 Bucket with naming convention
        var upload = multer({
            storage: multerS3({
                s3: s3,
                bucket: 'tecmaths',
                acl: 'public-read',
                tagging: taggingValue,
                contentType: multerS3.AUTO_CONTENT_TYPE,

                metadata: function (req, file, cb) {
                    //////////////console.log("testing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ", file.fieldname);
                    cb(null, { fieldName: file.fieldname });
                },
                key: function (req, file, cb) {
                    s3keyURL = s3key + file.originalname;
                    //////////////console.log('fileUrl>>>>>>>>>>>>>>>>>', s3keyURL);
                    cb(null, s3keyURL);
                }
            })
        });
        ////////// End of Config for S3 Bucket

        ////////// Actual upload to S3
        upload.single('file')(req, res, function (err, data) {
            if (err) {
                res.json({ "error": true, "message": "Some error while uploading files.", "data": err })
                return;
            }
            //////////////console.log("testing >>>>>>>>>>>>>>>>>>> ", req.file);
            var fileUrl = 'https://s3.ap-south-1.amazonaws.com/tecmaths/' + s3keyURL;
            res.json({ "error": false, "message": "File upload successfully", "file_url": fileUrl });
            ///////// End Upload to s3//////
        });

    });

    // upload chat file
    app.post('/uploadChatFile', function (req, res) {
        //var timeStamp = Date.now().toString();
        var s3key = "ChatImages/" + 'image__' + Date.now() + '_';
        var s3keyURL = "ChatImages/" + 'image__' + Date.now() + '_';
        var taggingValue = { 'auto-delete': true };
        // ////////////console.log("sending image ", taggingValue);
        /////////// Config for S3 Bucket with naming convention
        var upload = multer({
            storage: multerS3({
                s3: s3,
                bucket: 'tecmaths',
                acl: 'public-read',
                etag: taggingValue,
                tags: [{ "auto-delete": true }],
                contentType: multerS3.AUTO_CONTENT_TYPE,

                metadata: function (req, file, cb) {
                    //////////////console.log("testing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ", file.fieldname);
                    cb(null, { fieldName: file.fieldname });
                },
                key: function (req, file, cb) {
                    s3keyURL = s3key + file.originalname;
                    //////////////console.log('fileUrl>>>>>>>>>>>>>>>>>', s3keyURL);
                    cb(null, s3keyURL);
                }
            })
        });
        ////////// End of Config for S3 Bucket

        ////////// Actual upload to S3
        upload.single('file')(req, res, function (err, data) {
            if (err) {
                res.json({ "error": true, "message": "Some error while uploading files.", "data": err })
                return;
            }
            //////////////console.log("testing >>>>>>>>>>>>>>>>>>> ", req.file);
            var fileUrl = 'https://s3.ap-south-1.amazonaws.com/tecmaths/' + s3keyURL;
            res.json({ "error": false, "message": "File upload successfully", "file_url": fileUrl });
            ///////// End Upload to s3//////
        });

    });

    function deleteFile(filename) {
		if(filename != '') {
			filename = filename.substring(45);
			////////////console.log(">>>>>>>>>>>>", filename);
			var params = {
				Bucket: 'tecmaths',
				Key: filename
			};
			s3.deleteObject(params, function (err, data) {
				if (data) {
					////////////console.log("File deleted successfully");
					//next(true);
				}
				else {
					////////////console.log("Check if you have sufficient permissions : " + err);
					//next(true);
				}
			});
		}
    }

    // generate event on profile updated
    function updateProfileEvent(groupId) {
        ////////////console.log("group id ", groupId)
        room.aggregate([{ $match: { _id: ObjectId(groupId) } }, { $unwind: "$users" },
        { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
        { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
        {
            $group: {
                '_id': {
                    "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by",
                    "created_at": "$created_at", "is_group": "$is_group", "invitation_blocked": "$invitation_blocked",
                    "history_enable": "$history_enable"
                }, 'users': { '$push': '$users' }
            }
        }], function (err, dataaa) {
            if (err) {

            } else {
                if (dataaa.length > 0) {
                    var responseData = dataaa[0];
                    ////////////console.log(">>>> users ", dataaa);
                    responseData.users.forEach(userss => {
                        try {
                            // ////////////console.log("test");
                            if (userss.user_id[0].socket_id != '' && userss.status == 'joined') {
                                io.nsps['/'].sockets[userss.user_id[0].socket_id].join(groupId);
                                io.to(userss.user_id[0].socket_id).emit("userProfileUpdated", "User Profile updated");
                            }
                        } catch (e) { }
                    });
                }
            }
        });
    }

    function sendUpdateProfileMessage(index, userId, groupId, oldFirstName, oldLastName, firstName, lastName, next) {
        if (groupId == constants.PUBLIC_GROUP_ID) {
            next(index);
            return;
        }

        room.aggregate([{ $match: { _id: ObjectId(groupId) } }, { $unwind: "$users" },
        { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
        { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
        {
            $group: {
                '_id': {
                    "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by",
                    "created_at": "$created_at", "is_group": "$is_group", "invitation_blocked": "$invitation_blocked",
                    "history_enable": "$history_enable"
                }, 'users': { '$push': '$users' }
            }
        }], function (err, dataaa) {
            if (err) {
                // ////////////console.log("UPDATEPROFILE => ", "Database Error!! fetch room info for index -- " + index + "  Error : " + err);
                next(index);
            } else {
                if (dataaa) {
                    var responseData = dataaa[0];
                    // ////////////console.log(">>>>>>>>11111", responseData);
                    var currentTime = Date.now().toString();
                    var messageData = new message({
                        sender_id: userId,
                        room_id: ObjectId(groupId),
                        content_type: "alert",
                        retain_count: responseData.users.length,
                        content: oldFirstName + ' ' + oldLastName + ' changed profile name to ' + firstName + ' ' + lastName,
                        created_at: currentTime,
                        updated_at: currentTime
                    });

                    messageData.save(function (err, messageInfo) {
                        if (err) {
                            // ////////////console.log("UPDATEPROFILE => ", "Database Error!! save message for index -- " + index + "  Error : " + err);
                            next(index);
                        } else {
                            responseData.users.forEach(userss => {
                                try {
                                    if (userss.user_id[0].socket_id != '' && userss.status == 'joined') {
                                        io.nsps['/'].sockets[userss.user_id[0].socket_id].join(groupId);
                                        io.to(userss.user_id[0].socket_id).emit("onMessageReceived", messageInfo);
                                    } else if (userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                        sendNotification([userss.user_id[0].notification_token], messageInfo);

                                        sendNotificationForIOS([userss.user_id[0].notification_token], messageInfo, oldFirstName + ' ' + oldLastName + ' changed profile name to ' + firstName + ' ' + lastName, "message", firstName + " " + lastName);
                                    }
                                } catch (e) {
                                    // ////////////console.log("EXCEPTION >>>", "notification send from catch block!!");
                                    sendNotification([userss.user_id[0].notification_token], messageInfo);
                                    //need to ask
                                    sendNotificationForIOS([userss.user_id[0].notification_token], messageInfo, oldFirstName + ' ' + oldLastName + ' changed profile name to ' + firstName + ' ' + lastName, "message", firstName + " " + lastName);
                                }
                            });
                            next(index);
                        }
                    });
                }
            }
        });
    }

    app.post('/unreadMessage', function (req, res) {
        room_id = req.body.room_id;
        user_id = req.body.user_id;

        room.find({ _id: ObjectId(room_id) }, function (err, data) {
            if (err) {
                ////////////console.log("error in unread message on line 305 on day ", Date.now())
            }
            else {
                if (data.length == 1) {
                    // console.log("user id in unread mesage is", user_id);
                    if (user_id.length == 24) {

                        //console.log("user id length is 24");
                        user.findOne({ _id: ObjectId(user_id) }, function (err, userDataFound) {
                            if (err) {
                                //console.log("error is here on line 1778 ", err);
                                res.json({ error: true, message: "user not found" });
                            }
                            else {
                                message.find({ room_id: ObjectId(room_id), 'read_info.user_id': { $ne: user_id }, created_at: { $gt: userDataFound.last_login } }, function (err, msgdata) {
                                    if (err) {
                                        ////////////console.log("error is in unreadMessage line 310 ", Date.now());
                                        res.json({ error: true, message: "Error occurred" });
                                    }
                                    else {
                                        // ////////////console.log("data found", data);
                                        res.json({ error: false, message: "All unread messages", data: msgdata });
                                    }
                                })
                            }
                        })
                    }

                }
            }
        })
    })

    app.post('/getUsers', function (req, res) {
        var usrs_ids = req.body.usrs_ids.toString();
        var id = usrs_ids.split(",");

        user.find({ 'user_id': { $in: id } }, function (err, docs) {
            res.json(docs);
        });
    });

    app.post('/getUserRooms', function (req, res) {
        var user_id = req.body.user_id;

        room.aggregate([{ $match: { "users.user_id": user_id } }, { $unwind: "$users" },
        { $lookup: { from: "users", localField: "users.user_id", foreignField: "user_id", as: "users.user_id" } },
        { $lookup: { from: "users", localField: "created_by", foreignField: "user_id", as: "created_by" } },
        {
            $group: {
                '_id': {
                    "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
                    "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "history_enable": "$history_enable"
                },
                'users': { '$push': '$users' }
            }
        }], function (err, data) {
            if (err) {
                ////////////console.log(err)
            }
            else
                res.json(data);
        });
    });

    app.post("/getMessage", function (req, res) {
        //console.log("im getMessage");

        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        param = req.body;

        message.findOne({ _id: ObjectId(param.message_id) }, function (err, messageData) {
            if (err) {
                // ////////////console.log("Error", err);
            } else {
                res.json({ "messageData": messageData });
            }
        });

    });

    app.post("/getMessages", function (req, res) {
        //console.log("in getMessages");

        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        var room_id = req.body.room_id;
        var created_at = req.body.time;



        message.aggregate([{ $match: { room_id: ObjectId(room_id), created_at: { $lt: created_at } } }, { $sort: { created_at: -1 } }, { $limit: 50 }], function (err, messages) {
            if (err) {
                // ////////////console.log("Error", err);
            } else {
                res.json({ "messages": messages });
            }
        });

    });

    app.post('/roomDataUpdate', function (req, res) {
        var room_id = req.body.room_id;
        // ////////////console.log(room_id);
        delete req.body['room_id'];

        var updateData = req.body;

        room.findOne({ _id: ObjectId(room_id) }, function (err, roomInfo) {
            // ////////////console.log("VJ!", "Inside Find");

            if (err) {
                res.json({ error: true, message: err });
            } else {
                var old_name = roomInfo.name;
                // ////////////console.log("old name ", old_name);
                var old_icon = roomInfo.icon_url;
                room.update({ _id: ObjectId(room_id) }, { $set: updateData }, function (err, data) {
                    // ////////////console.log("VJ!", "Here inside UPDATION");

                    if (err) {
                        // ////////////console.log("check data here ", data);
                        // ////////////console.log("check ", err);
                    } else {
                        user.findOne({ user_id: updateData.user_id }, function (err, userInfo) {

                            if (err) {
                                res.json({ error: true, message: err });
                            } else {
                                room.aggregate([{ $match: { _id: ObjectId(room_id) } }, { $unwind: "$users" },
                                { $lookup: { from: "users", localField: "users.user_id", foreignField: "user_id", as: "users.user_id" } },
                                { $lookup: { from: "users", localField: "created_by", foreignField: "user_id", as: "created_by" } },
                                {
                                    $group: {
                                        '_id': {
                                            "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by",
                                            "created_at": "$created_at", "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "history_enable": "$history_enable"
                                        },
                                        'users': { '$push': '$users' }
                                    }
                                }], function (err, dataaa) {
                                    if (err) {
                                        res.json(err);
                                    } else {
                                        if (updateData.name != old_name || updateData.icon_url != old_icon) {
                                            var responseData = dataaa[0];
                                            // ////////////console.log(">>>>>>>>>>>>>>>>>>>>>>>>> ", responseData);

                                            // ////////////console.log("test again ", updateData.name);
                                            if (updateData.name != old_name && updateData.name != undefined) {
                                                // ////////////console.log("Room Name Update successfully");
                                                var messageDataName = new message({
                                                    user_id: updateData.user_id,
                                                    room_id: ObjectId(room_id),
                                                    content_type: "alert",
                                                    retain_count: roomInfo.users.length,
                                                    content: userInfo.first_name + ' ' + userInfo.last_name + " changed group name from " + old_name + " to " + updateData.name,
                                                    created_at: Date.now().toString()
                                                });

                                                messageDataName.save(function (err, messageInfo) {
                                                    if (err) {
                                                        // ////////////console.log("Inside if message" + err);
                                                        res.json(err);
                                                    } else {
                                                        // ////////////console.log("user name messageInfo ", messageInfo);
                                                        responseData.users.forEach(userss => {
                                                            // ////////////console.log(userss.user_id[0].first_name);
                                                            if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                                                if (userss.user_id[0].device_type == "Android") {
                                                                    sendNotification([userss.user_id[0].notification_token], messageDataName);
                                                                }
                                                                else {
                                                                    var name = userInfo.first_name + ' ' + userInfo.last_name;
                                                                    sendNotificationForIOS([userss.user_id[0].notification_token], messageData, userInfo.first_name + ' ' + userInfo.last_name + " changed group name from " + old_name + " to " + updateData.name, "message", name);
                                                                }

                                                            }
                                                        });

                                                        io.to(room_id).emit("onMessageReceived", messageDataName);
                                                    }
                                                });
                                            }

                                            if (updateData.icon_url != old_icon) {
                                                // ////////////console.log("VJ!", "Here inside updateICON");

                                                // ////////////console.log("Room Icon Update successfully");
                                                var messageData = new message({
                                                    user_id: updateData.user_id,
                                                    room_id: ObjectId(room_id),
                                                    content_type: "alert",
                                                    retain_count: roomInfo.users.length,
                                                    content: userInfo.first_name + ' ' + userInfo.last_name + " changed group's icon",
                                                    created_at: Date.now().toString()
                                                });

                                                messageData.save(function (err, messageInfo) {
                                                    // ////////////console.log("VJ!", "Here inside SAVE");
                                                    if (err) {
                                                        // ////////////console.log("VJ!", "Here inside if SAVE");
                                                        // ////////////console.log("Inside if message" + err);
                                                        res.json(err);
                                                    } else {
                                                        // ////////////console.log(messageInfo);
                                                        // ////////////console.log("VJ!", "Here inside ELSE SAVE");

                                                        responseData.users.forEach(userss => {
                                                            // ////////////console.log(userss.user_id[0].first_name);
                                                            if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                                                if (userss.user_id[0].device_type == "Android") {
                                                                    sendNotification([userss.user_id[0].notification_token], messageData);
                                                                }
                                                                else {
                                                                    var name = userInfo.first_name + ' ' + userInfo.last_name;
                                                                    sendNotificationForIOS([userss.user_id[0].notification_token], messageData, userInfo.first_name + ' ' + userInfo.last_name + " changed group's icon", "message", name);
                                                                }
                                                            }
                                                            // ////////////console.log("VJ!", "Here inside SEND NOTIFICATION SAVE");

                                                        });
                                                        io.to(room_id).emit("oldFirstNameonMessageReceived", messageData);
                                                        // ////////////console.log("VJ!", "Here inside SAVE after onMessageReceived");

                                                    }
                                                });
                                            }
                                        } else {
                                            // ////////////console.log("Nothing to update");
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
                res.json({ error: false, message: roomInfo });
            }
        });
    });

    app.post("/updateDeletedID", function (req, res) {
        var message_ids = req.body.message_ids.toString();
        var user_id = req.body.user_id;
        var message_id = [];

        message_ids.split(",").forEach(message_idd => {

            message_id: message_idd

            message_id.push(message_idd);
        });
        // res.json(message_id.length);
        message_id.forEach(m_id => {
            message.update({ _id: ObjectId(m_id) }, { $push: { deleted_id: user_id } }, function (err, messageInfo) {
                if (err) {
                    res.json(err);
                } else {
                    res.end();
                }
            });
        });
    });

    app.post('/openToAll', function (req, res) {
        //console.log("Enter to openToAll API>>>>>>>>>>", Date.now());
        authentication.data.authenticateToken(req, function (result1) {
            if (result1) {
                room_id = req.body.room_id;
                is_true = req.body.is_true;
                sender_id = req.body.user_id;
                var user_name = req.body.name

                room.update({ _id: ObjectId(room_id) }, { $set: { open_to_all: is_true + "", updated_at: Date.now().toString() } }, function (err, done) {
                    if (err) {
                        res.json({ error: true, message: "Error occurred while updating." });
                    }
                    else {

                        // res.json({error:false, message : "Update Success", done : done, is_true: is_true});
                        // ////////////console.log("room id  is  ", room_id)
                        // ////////////console.log("done is  ", done)

                        room.aggregate([{ $unwind: "$users" }, { $match: { $and: [{ _id: ObjectId(room_id) }, { "users.status": "joined" }] } },
                        { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                        { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                        {
                            $group: {
                                '_id': {
                                    "_id": "$_id",
                                    "name": "$name",
                                    "icon_url": "$icon_url",
                                    "created_by": "$created_by",
                                    "created_at": "$created_at",
                                    "is_group": "$is_group",
                                    "invitation_blocked": "$invitation_blocked",
                                    "history_enable": "$history_enable"
                                },
                                'users': { '$push': '$users' }
                            }
                        }
                        ], function (err, responseData) {
                            if (err) {
                                res.json({ error: true, message: err });
                            }
                            else {
                                // ////////////console.log("is_true is here iss >>>>> ", is_true);
                                if (is_true == "1" && user_name == null) {
                                    roomUpdate = "Everyone is able to send the message";
                                }
                                else if (is_true == "1" && user_name != null) {
                                    roomUpdate = user_name + " made it : All can send messages";

                                }
                                else if (user_name == null) {
                                    roomUpdate = "Only admin  is able to send the message";
                                }
                                else {
                                    roomUpdate = user_name + " made it : Only admin can send messages";

                                }

                                var messageData = new message({
                                    sender_id: sender_id,
                                    room_id: ObjectId(room_id),
                                    content_type: "alert",
                                    file_url: "",
                                    file_size: "",
                                    thumbnail_data: "",
                                    retain_count: 0,
                                    content: roomUpdate,
                                    parent_msg: "",
                                    created_at: Date.now().toString(),
                                    updated_at: Date.now().toString()
                                });

                                messageData.save(function (err, messageInfo) {
                                    if (err) {
                                        // ////////////console.log("error while saving message");
                                    }
                                    else {
                                        // ////////////console.log("testing message data ", messageInfo);
                                        responseData[0].users.forEach(userss => {
                                            // ////////////console.log(userss, " This is socket ID");
                                            if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                                if (userss.user_id[0].device_type == "Android") {
                                                    // ////////////console.log("in sending notification token is ",userss.user_id[0].notification_token);
                                                    sendOtherNotification([userss.user_id[0].notification_token], { openToAll: is_true, message: roomUpdate, room_id: room_id });

                                                    sendNotification([userss.user_id[0].notification_token], messageInfo);
                                                }

                                                else {
                                                    var name = userss.user_id[0].first_name + userss.user_id[0].last_name;
                                                    sendNotificationForIOS([userss.user_id[0].notification_token], { openToAll: is_true, message: roomUpdate, room_id: room_id }, "text", "other", name);

                                                    sendNotificationForIOS([userss.user_id[0].notification_token], messageInfo, roomUpdate, "alert", name);
                                                }
                                            }
                                        });
                                        //console.log("Processing openToAll API>>>>>>>>>>>>>>", Date.now());
                                        // ////////////console.log(io.to(room_id), "room details");
                                        io.to(room_id).emit("onOpenToAll", { openToAll: is_true, message: roomUpdate, room_id: room_id });

                                        io.to(room_id).emit("onMessageReceived", messageInfo);

                                        // ////////////console.log(messageInfo);

                                        res.json({ error: false, message: roomUpdate, data: { openToAll: is_true, message: roomUpdate, room_id: room_id } });
                                        //console.log("Processing 2 to openToAll API>>>>>>>>>>", Date.now());
                                    }
                                });
                            }
                        });
                    }
                });
            }
            else {
                res.json(result1)
            }
        })
        //console.log("Exit from openToAll API>>>>>>>>>>>>>>", Date.now());
    });

    // {_id : ObjectId("5b9d0cbfcdc28f23bd744c67") }, 
    // {$push : {delivered_to : ["5b9cb5c6bf94783bd97eec7b", "5b9cb84bbf94783bd97eec85"]}}
    app.post('/deliveredTo', function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {

            ////////////console.log('result >>>>>> ', result1);
            ////////////console.log('msg ids >>>>', req.body.msg_ids);

            if (result1) {
                msg_ids = req.body.msg_ids;
                user_id = req.body.user_id;
                ////////////console.log('user id is ', user_id);

                all_msgs = msg_ids.split(",");
                ////////////console.log("all messages are here ", all_msgs);
                let i = 0;

                all_msgs.forEach((msg, i) => {
                    ////////////console.log("ID>>>>>>>>>>>>>>>>>>", msg);
                    if (msg.length == 24) {
                        message.update({ _id: ObjectId(msg), delivered_to: { $ne: user_id } }, { $push: { delivered_to: user_id } }, function (err, done) {
                            if (err) {
                                res.json({ err: true, message: "Something went wrong." });
                            }
                            else {
                                if (done) {
                                    i++;

                                    if (i == all_msgs.length) {
                                        ////////////console.log("Result is DONE");
                                        res.json({ error: false, message: "done" });
                                    }
                                }

                                ////////////console.log("here it is update ", i, " done is ", done, " main msg length ", all_msgs.length);


                            }
                        });
                    }
                });
            }
            else {

                res.json(result1)
            }
        });
    });

    app.post("/updateLastLogin", function (req, res) {
        authentication.data.authenticateToken(req, function (result1) {
            if (result1) {
                user_id = req.body.user_id;

                user.update({ _id: ObjectId(user_id) }, { $set: { last_login: Date.now().toString() } }, function (err, done) {
                    if (err) {
                        ////////////console.log({ error: true, message: "User not found" });
                        res.json({ error: true, message: "User not found" });
                    }
                    else {
                        res.json({ error: false, message: "User time updated" });
                    }
                })
            }
            else {
                res.json(result1)
            }
        });
    });

    ////////////////////////////////////////////for admin it starts here///////////////////////////////////////////////////////

    var serverAddress = "http://52.66.4.80:3060/"
    // var serverAddress = "http://192.168.0.26:3060/"
    var perPageLimit = 50000;

    app.get('/students', function (req, res) {
        var pageNo = req.query.page;
        if (pageNo == undefined)
            pageNo = 1;
        pageNo = pageNo - 1 + 1;
        var skipp = perPageLimit * (pageNo - 1);
        var limitt = perPageLimit;

        user.count({}, function (err, totalCount) {
            if (err) {
                res.json({ error: true, message: "Database Error!!!, count" });
            } else {
                user.aggregate({ $match: {} }, { $sort: { created_at: -1 } }, { $lookup: { from: "rooms", localField: "rooms", foreignField: "_id", as: "rooms" } }, { $skip: skipp }, { $limit: limitt },
                    function (err, userss) {
                        if (err) {
                            res.json({ error: true, message: "Database Error!!! fetch" });
                        } else {
                            var totalPages = Math.ceil(totalCount / perPageLimit)
                            var lastPageUrl = serverAddress + "students?page=" + pageNo;
                            var nextPageUrl = null;
                            var previousPageUrl = null;
                            if (totalPages > pageNo)
                                lastPageUrl = serverAddress + "students?page=" + totalPages;
                            if (totalPages > pageNo)
                                nextPageUrl = serverAddress + "students?page=" + (pageNo + 1);
                            if (pageNo > 1)
                                previousPageUrl = serverAddress + "students?page=" + (pageNo - 1);
                            res.json({
                                current_page: pageNo,
                                data: userss,
                                first_page_url: serverAddress + "students?page=1",
                                from: skipp + 1,
                                last_page: totalPages,
                                last_page_url: lastPageUrl,
                                next_page_url: nextPageUrl,
                                per_page: perPageLimit,
                                prev_page_url: previousPageUrl,
                                to: skipp + userss.length,
                                total: totalCount,
                                path: serverAddress + "students"
                            });
                        }
                    });
            }
        });
    });

    //service to block student
    app.get('/student/block', function (req, res) {
        var userId = req.query.usr_id;
        user.updateOne({ _id: ObjectId(userId) }, { $set: { is_blocked: true } }, function (err, done) {
            if (err) {
                res.json({ error: true, data: err });
            } else {
                user.findOne({ _id: ObjectId(userId) }, function (err, userInfo) {
                    if (userInfo['device_type'] == 'Android') {
                        sendAlertNotification([userInfo.notification_token], 'TecConnect Admin removed you.');
                    }
                    else {
                        //ids, messageDetail, messageToBeShown, type = "message", titleForIOS
                        sendNotificationForIOS([userInfo.notification_token], 'TecConnect Admin removed you.', 'TecConnect Admin removed you.', "message", "TecConnect Admin");
                    }
                    res.json({ error: false });
                });
            }
        });
    });

    // service to activate student
    app.get('/student/activate', function (req, res) {
        var userId = req.query.usr_id;
        user.update({ _id: ObjectId(userId) }, { $set: { is_blocked: false } }, function (err, done) {
            if (err) {
                res.json({ error: true, data: err });
            } else {
                res.json({ error: false });
            }
        });
    });

    // show student per student detail
    app.get('/student', function (req, res) {
        var userId = req.query.usr_id;
        ////////////console.log("userid >>>> ", userId);
        user.findOne({ _id: ObjectId(userId) }, function (err, dataa) {
            if (err) {
                res.json({ error: true, message: err });
            } else {
                ////////////console.log("goooooooooooooooo", dataa);
                ////////////console.log("!!!", dataa.rooms[0])
                getUserAdminStatus(userId, dataa.rooms[0], function (result) {
                    ////////////console.log("!!!", result);
                    res.json({ data: dataa, is_admin: result });
                });
            }
        })
    });

    // service to know no. of blocked students
    app.get('/students/blocked', function (req, res) {
        var pageNo = req.query.page;
        if (pageNo == undefined)
            pageNo = 1;
        pageNo = pageNo - 1 + 1;
        var skipp = perPageLimit * (pageNo - 1);
        var limitt = perPageLimit;

        user.count({ is_blocked: true }, function (err, totalCount) {
            if (err) {
                res.json({ error: true, message: "Database Error!!!, count" });
            } else {
                user.aggregate({ $match: { is_blocked: true } }, { $lookup: { from: "rooms", localField: "rooms", foreignField: "_id", as: "rooms" } },

                    { $skip: skipp }, { $limit: limitt },
                    function (err, userss) {
                        if (err) {
                            res.json({ error: true, message: "Database Error!!! fetch" });
                        } else {
                            var totalPages = Math.ceil(totalCount / perPageLimit)
                            var lastPageUrl = serverAddress + "students/blocked?page=" + pageNo;
                            var nextPageUrl = null;
                            var previousPageUrl = null;
                            if (totalPages > pageNo)
                                lastPageUrl = serverAddress + "students/blocked?page=" + totalPages;
                            if (totalPages > pageNo)
                                nextPageUrl = serverAddress + "students/blocked?page=" + (pageNo + 1);
                            if (pageNo > 1)
                                previousPageUrl = serverAddress + "students/blocked?page=" + (pageNo - 1);
                            res.json({
                                current_page: pageNo,
                                data: userss,
                                first_page_url: serverAddress + "students/blocked?page=1",
                                from: skipp + 1,
                                last_page: totalPages,
                                last_page_url: lastPageUrl,
                                next_page_url: nextPageUrl,
                                per_page: perPageLimit,
                                prev_page_url: previousPageUrl,
                                to: skipp + userss.length,
                                total: totalCount,
                                path: serverAddress + "students/blocked"
                            });
                        }
                    });
            }
        });
    });

    app.get('/studentDetails', function (req, res) {
        var userId = req.query.usr_id;
        // ////////////console.log("userid >>>> ", userId);
        user.findOne({ _id: ObjectId(userId) }, function (err, dataa) {
            if (err) {
                res.json({ error: true, message: err });
            } else {
                // ////////////console.log("goooooooooooooooo ", dataa);
                room.aggregate({ $unwind: "$users" },
                    { $match: { "users.user_id": ObjectId(userId) } },
                    { $project: { name: 1, _id: 0, "users.status": 1 } }, function (err, data) {
                        if (err) {
                            // ////////////console.log("error while fetching room ", err);
                            res.json({ error: true, message: "gone totally" });
                        }
                        else {
                            roomData = new Array();
                            for (i = 0; i < data.length; i++) {
                                // ////////////console.log("room data >>>>>>> ",data[i].name);
                                roomData[i] = { "name": data[i].name, "status": data[i].users.status };
                            }
                            roomData[i] = { "name": "Public", "status": "joined" }

                            // ////////////console.log("room names are >> ",roomNames)
                            // ////////////console.log("sending response");

                            res.json({ data: dataa, roomData: roomData });
                        }
                    })
            }
        })
    });

	app.get('/groups_OLD', function (req, res) {
        var pageNo = req.query.page;
        if (pageNo == undefined)
            pageNo = 1;
        pageNo = pageNo - 1 + 1;
        var skipp = perPageLimit * (pageNo - 1);
        var limitt = perPageLimit;

        room.count({}, function (err, totalCount) {
            if (err) {
                res.json({ error: true, message: "Database Error!!!, count" });
            } else {
                room.aggregate({ $match: {} }, { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } }, { $skip: skipp }, { $limit: limitt }, function (err, userss) {
                    if (err) {
                        res.json({ error: true, message: "Database Error!!! fetch" });
                    } else {
                        var arr = [];
                        var index = 0;
                        //console.log("userss in groups>>>>>>>>>>>>>>>>>>",userss.length);
                        userss.forEach(usrr => {
                            ////console.log("userss in groups>>>>>>>>>>>>>>>>>>",usrr._id);
                            var obj = {
                                group_id: usrr._id,
                                group_name: usrr.name,
                                total_members: usrr.users.length,
                                creator_id: usrr.created_by[0]._id,
                                first_name: usrr.created_by[0].first_name,
                                last_name: usrr.created_by[0].last_name,
                                created_on: usrr.created_at
                            };
                            arr[index] = obj;
                            index++;
                        });

                        var totalPages = Math.ceil(totalCount / perPageLimit)
                        var lastPageUrl = serverAddress + "groups?page=" + pageNo;
                        var nextPageUrl = null;
                        var previousPageUrl = null;
                        if (totalPages > pageNo)
                            lastPageUrl = serverAddress + "groups?page=" + totalPages;
                        if (totalPages > pageNo)
                            nextPageUrl = serverAddress + "groups?page=" + (pageNo + 1);
                        if (pageNo > 1)
                            previousPageUrl = serverAddress + "groups?page=" + (pageNo - 1);
                        res.json({
                            current_page: pageNo,
                            data: arr,
                            first_page_url: serverAddress + "groups?page=1",
                            from: skipp + 1,
                            last_page: totalPages,
                            last_page_url: lastPageUrl,
                            next_page_url: nextPageUrl,
                            per_page: perPageLimit,
                            prev_page_url: previousPageUrl,
                            to: skipp + userss.length,
                            total: totalCount,
                            path: serverAddress + "groups"
                        });
                    }
                });
            }
        });
    });


//backup
  //   app.post('/groups', function (req, res) {
		// var pageNo = req.body.page > 1 ? parseInt(req.body.page) : 1;
		// var pageLimit = req.body.limit > 1 ? parseInt(req.body.limit) : 10;
		// var offset = pageLimit * (pageNo - 1);
		// var pageOffset = offset > 1 ? offset : 0;
		// var search_key = req.body.search_key ? req.body.search_key : '';
		// var latitude = req.body.latitude ? parseFloat(req.body.latitude) : '';
		// var longitude = req.body.longitude ? parseFloat(req.body.longitude) : '';
		// var maximum_distance = req.body.maximum_distance > 0 ? parseInt(req.body.maximum_distance) : 10000;
        
  //       room.count({}, function (err, totalCount) {
  //           if (err) {
  //               res.json({ success: false, message: "No data found!!!" });
  //           } else {
		// 		/*
		// 		var q = room.find({
		// 			$or: [
		// 				{ keyword: new RegExp(search_key, 'i') },
		// 				{ "location": {
		// 						$near: {
		// 							$geometry: {
		// 								type: "Point" ,
		// 								coordinates: [ latitude , longitude ]
		// 							},
		// 							$maxDistance: maximum_distance,
		// 							$minDistance: 0
		// 						}
		// 				   }
		// 				}
		// 			]
		// 		});
				
		// 		var q = room.find({ keyword: new RegExp(search_key, 'i') }).limit(pageLimit).skip(pageOffset);
				
		// 		var q = room.find(
		// 		{
		// 			location: {
		// 				$near: {
		// 					$geometry: {
		// 						type: "Point" ,
		// 						coordinates: [ latitude , longitude ],
		// 						"query": {
		// 							"keyword": new RegExp(search_key, 'i')
		// 						}
		// 					},
		// 					$maxDistance: maximum_distance,
		// 					$minDistance: 0
		// 				}
		// 		   }
		// 		});
				
		// 		q.exec(function(err, result) {
		// 			console.log(result);
		// 			res.json({
		// 				limit: pageLimit,
		// 				offset: pageOffset,
		// 				data: result
		// 			});
		// 		});
		// 		*/


  //               // var MongoClient = require('mongodb').MongoClient;
  //               // var url = "mongodb://127.0.0.1:27017/";

                
		// 		var aggregate = room.aggregate();

  //              /* aggregate.lookup({ from: "review", localField: "group_id", foreignField: "room_id", as: "ratingGroup"});
  //               aggregate.group({ _id : "group_id", foreignField: "room_id", as: "ratingGroup"});*/
 				
		// 		if(latitude != "" && longitude != "") {
		// 			aggregate.near({
		// 				near: { type: "Point", coordinates: [ latitude, longitude ] },
		// 				maxDistance: maximum_distance,
		// 				minDistance: 0,
		// 				includeLocs: "dist.location",
		// 				distanceField: "dist.calculated",
		// 				spherical: true
		// 			});
		// 		}
		// 		if(search_key != '') {
		// 			aggregate.match({keyword:{$regex:search_key, $options: 'i'}});
		// 		}
		// 		aggregate.lookup({ from: "users", localField: "created_by", foreignField: "_id", as: "created_by" });
		// 	    aggregate.lookup({ from:"reviews",localField:"_id",foreignField:"room_id",as:"reviewInfo"});
  //               // aggregate.avg({"average_rating":$reviewInfo.rating});
  //               // aggregate.lookup({from: "reviews", localField: "_id", foreignField: "room_id", as: "review_info" })
  //               // aggregate.avg({"$reviewInfo.rating"});
  //               // aggregate.avg()
  //               // { $project :{"average_rating":{ $avg:"$reviewInfo.rating"}}}
  //               // aggregate.project({average_rating:{ $avg:"$reviewInfo.rating"}});
  //               // aggregate.avg("$reviewInfo.rating");
  //               aggregate.skip(pageOffset);
		// 		aggregate.limit(pageLimit);
		// 		aggregate.exec(function(err,userss){
		// 			if (err) {
  //                       res.json({ success: false, message: "Database Error!!! fetch", data: err });
  //                   } else 
		// 			{
  //                       var arr = [];
  //                       var index = 0;
  //                       var sum=0;
  //                       var avg=0;
  //                       var count=0;
  //                       userss.forEach(usrr => {
  //                           // reviewInfo.forEach(rat => {
  //                           //      sum=sum+rat.reviewInfo.rating;
  //                           // });
  //                           console.log(usrr);
  //                           var obj = {
  //                               group_id: usrr._id,
  //                               group_name: usrr.name,
  //                               group_icon_url:usrr.icon_url,
		// 						keyword: usrr.keyword,
  //                               total_members: usrr.users.length,
  //                               creator_id: usrr.created_by.length > 0 ? usrr.created_by[0]._id : '',
  //                               first_name: usrr.created_by.length > 0 ? usrr.created_by[0].first_name : '',
  //                               last_name: usrr.created_by.length > 0 ? usrr.created_by[0].last_name : '',
  //                               created_on: usrr.created_at,
  //                               reviewInfo:usrr.reviewInfo,
  //                           };
  //                           arr[index] = obj;
  //                           index++;
  //                       });
  //                       var totalPages = Math.ceil(totalCount / pageLimit)
  //                       var lastPageUrl = serverAddress + "groups?page=" + pageNo;
  //                       var nextPageUrl = null;
  //                       var previousPageUrl = null;
  //                       if (totalPages > pageNo)
  //                           lastPageUrl = serverAddress + "groups?page=" + totalPages;
  //                       if (totalPages > pageNo)
  //                           nextPageUrl = serverAddress + "groups?page=" + (pageNo + 1);
  //                       if (pageNo > 1)
  //                           previousPageUrl = serverAddress + "groups?page=" + (pageNo - 1);
  //                       res.json({
		// 					success: true,
  //                           current_page: pageNo,
  //                           data: arr,
  //                           first_page_url: serverAddress + "groups?page=1",
  //                           from: pageOffset + 1,
  //                           last_page: totalPages,
  //                           last_page_url: lastPageUrl,
  //                           next_page_url: nextPageUrl,
  //                           per_page: pageLimit,
  //                           prev_page_url: previousPageUrl,
  //                           to: pageOffset + userss.length,
  //                           total: totalCount,
  //                           path: serverAddress + "groups",
  //                       });
  //                   }
		// 		})
  //           }
  //       });
  //   });

app.post('/groups', function (req, res) {
        var pageNo = req.body.page > 1 ? parseInt(req.body.page) : 1;
        var pageLimit = req.body.limit > 1 ? parseInt(req.body.limit) : 10;
        var offset = pageLimit * (pageNo - 1);
        var pageOffset = offset > 1 ? offset : 0;
        var search_key = req.body.search_key ? req.body.search_key : '';
        var groupName = req.body.groupName ? req.body.groupName : '';
        var latitude = req.body.latitude ? parseFloat(req.body.latitude) : '';
        var longitude = req.body.longitude ? parseFloat(req.body.longitude) : '';
        var maximum_distance = req.body.maximum_distance > 0 ? parseInt(req.body.maximum_distance) : 10000;
        var category_id = req.body.category_id ? req.body.category_id : '';

        room.count({}, function (err, totalCount) {
            if (err) {
                res.json({ success: false, message: "No data found!!!" });
            } else {
                /*
                var q = room.find({
                    $or: [
                        { keyword: new RegExp(search_key, 'i') },
                        { "location": {
                                $near: {
                                    $geometry: {
                                        type: "Point" ,
                                        coordinates: [ latitude , longitude ]
                                    },
                                    $maxDistance: maximum_distance,
                                    $minDistance: 0
                                }
                           }
                        }
                    ]
                });
                
                var q = room.find({ keyword: new RegExp(search_key, 'i') }).limit(pageLimit).skip(pageOffset);
                
                var q = room.find(
                {
                    location: {
                        $near: {
                            $geometry: {
                                type: "Point" ,
                                coordinates: [ latitude , longitude ],
                                "query": {
                                    "keyword": new RegExp(search_key, 'i')
                                }
                            },
                            $maxDistance: maximum_distance,
                            $minDistance: 0
                        }
                   }
                });
                
                q.exec(function(err, result) {
                    console.log(result);
                    res.json({
                        limit: pageLimit,
                        offset: pageOffset,
                        data: result
                    });
                });
                */


                // var MongoClient = require('mongodb').MongoClient;
                // var url = "mongodb://127.0.0.1:27017/";

                
                // var aggregate = room.aggregate();

               /* aggregate.lookup({ from: "review", localField: "group_id", foreignField: "room_id", as: "ratingGroup"});
                aggregate.group({ _id : "group_id", foreignField: "room_id", as: "ratingGroup"});*/              
                if(latitude != "" && longitude != "") {
                    var aggregate = room.aggregate();
                    aggregate.near({
                        near: { type: "Point", coordinates: [ latitude, longitude ] },
                        maxDistance: 3000,
                        minDistance: 0,
                        includeLocs: "dist.location",
                        distanceField: "dist.calculated",
                        spherical: true
                    });

                }
                if(search_key != '') {

                    // aggregate.match({keyword:{$regex:search_key, $options: 'i'}});
                    room.aggregate({
                            $geoNear: {
                                near: {type: "Point", coordinates: [ latitude , longitude ]},
                                distanceField: "dist.calculated",
                                maxDistance:10000,
                                query: { keyword:{$regex:search_key, $options:'i'} },
                                spherical: true
                            }}, 
                            { $lookup:{ from: "users", localField: "created_by", foreignField: "_id", as: "created_by" }},{ $lookup:{ from:"reviews",localField:"_id",foreignField:"room_id",as:"reviewInfo"}},{ $project :{"average_rating":{ $avg:"$reviewInfo.rating"},"total_member":{ $size:"$users" },"reviewInfo":1,"group_address":1,"one_to_one_group":1,"users":1,"created_at":1,"updated_at":1,"location":1,"keyword":1,"is_deleted":1,"invitation_blocked":1,"history_enable":1,"is_group":1,"open_to_all":1,"invitations":1,"created_by":1,"icon_url":1,"name":1,"__v":1 }},{$sort:{"average_rating":-1,"total_member":-1}},{ $skip: pageOffset }, { $limit: pageLimit },
                        function(err,userss){
                        if (err) {
                            res.json({ success: false, message: "Database Error!!! fetch", data: err });
                        } else 
                        {
                        // res.json({"ghjgj":userss});
                            if(userss!='')
                            {
                                // res.json({"ghjgj":userss});
                                var arr = [];
                                var index = 0;
                                var sum=0;
                                var avg=0;
                                var count=0;
                                userss.forEach(usrr => {
                                    // res.json({message:usrr.one_to_one_group});
                                    var obj = {
                                        group_id: usrr._id,
                                        group_name: usrr.name,
                                        group_icon_url:usrr.icon_url,
                                        keyword: usrr.keyword,
                                        one_to_one_group:usrr.one_to_one_group,
                                        group_address:usrr.group_address,
                                        working_hour:usrr.working_hour,
                                        is_deleted:usrr.is_deleted,
                                        day_of_working:usrr.day_of_working,
                                        cover_photo:usrr.cover_photo,
                                        location:usrr.location,
                                        total_members: usrr.users.length,
                                        creator_id: usrr.created_by.length > 0 ? usrr.created_by[0]._id : '',
                                        first_name: usrr.created_by.length > 0 ? usrr.created_by[0].first_name : '',
                                        last_name: usrr.created_by.length > 0 ? usrr.created_by[0].last_name : '',
                                        created_on: usrr.created_at,
                                        reviewInfo:usrr.reviewInfo,
                                        average_rating:usrr.average_rating,
                                    };
                                    arr[index] = obj;
                                    index++;
                                });
                                var totalPages = Math.ceil(totalCount / pageLimit)
                                var lastPageUrl = serverAddress + "groups?page=" + pageNo;
                                var nextPageUrl = null;
                                var previousPageUrl = null;
                                if (totalPages > pageNo)
                                    lastPageUrl = serverAddress + "groups?page=" + totalPages;
                                if (totalPages > pageNo)
                                    nextPageUrl = serverAddress + "groups?page=" + (pageNo + 1);
                                if (pageNo > 1)
                                    previousPageUrl = serverAddress + "groups?page=" + (pageNo - 1);
                                res.json({
                                    success: true,
                                    current_page: pageNo,
                                    data: arr,
                                    first_page_url: serverAddress + "groups?page=1",
                                    from: pageOffset + 1,
                                    last_page: totalPages,
                                    last_page_url: lastPageUrl,
                                    next_page_url: nextPageUrl,
                                    per_page: pageLimit,
                                    prev_page_url: previousPageUrl,
                                    to: pageOffset + userss.length,
                                    total: totalCount,
                                    path: serverAddress + "groups",
                                });
                            }else{
                            room.aggregate({
                                $geoNear: {
                                    near: {type: "Point", coordinates: [ latitude , longitude ]},
                                    distanceField: "dist.calculated",
                                    maxDistance:10000,
                                    query: { name:{$regex:search_key, $options:'i'} },
                                    spherical: true
                                }}, 
                            { $lookup:{ from: "users", localField: "created_by", foreignField: "_id", as: "created_by" }},{ $lookup:{ from:"reviews",localField:"_id",foreignField:"room_id",as:"reviewInfo"}},{ $project :{"average_rating":{ $avg:"$reviewInfo.rating"},"total_member":{ $size:"$users" },"reviewInfo":1,"group_address":1,"one_to_one_group":1,"users":1,"created_at":1,"updated_at":1,"location":1,"keyword":1,"is_deleted":1,"invitation_blocked":1,"history_enable":1,"is_group":1,"open_to_all":1,"invitations":1,"created_by":1,"icon_url":1,"name":1,"__v":1 }},{$sort:{"average_rating":-1,"total_member":-1}},{ $skip: pageOffset }, { $limit: pageLimit },
                        function(err,userss){
                        if (err) {
                            res.json({ success: false, message: "Database Error!!! fetch", data: err });
                        } else 
                        {
                           
                                var arr = [];
                                var index = 0;
                                var sum=0;
                                var avg=0;
                                var count=0;
                                res.json({message:userss});
                                userss.forEach(usrr => {
                                    // res.json({message:usrr.one_to_one_group});
                                    var obj = {
                                        group_id: usrr._id,
                                        group_name: usrr.name,
                                        group_icon_url:usrr.icon_url,
                                        keyword: usrr.keyword,
                                        one_to_one_group:usrr.one_to_one_group,
                                        group_address:usrr.group_address,
                                        is_deleted:usrr.is_deleted,
                                        working_hour:usrr.working_hour,
                                        day_of_working:usrr.day_of_working,
                                        cover_photo:usrr.cover_photo,
                                        location:usrr.location,
                                        total_members: usrr.users.length,
                                        creator_id: usrr.created_by.length > 0 ? usrr.created_by[0]._id : '',
                                        first_name: usrr.created_by.length > 0 ? usrr.created_by[0].first_name : '',
                                        last_name: usrr.created_by.length > 0 ? usrr.created_by[0].last_name : '',
                                        created_on: usrr.created_at,
                                        reviewInfo:usrr.reviewInfo,
                                        average_rating:usrr.average_rating,
                                    };
                                    arr[index] = obj;
                                    index++;
                                });
                                var totalPages = Math.ceil(totalCount / pageLimit)
                                var lastPageUrl = serverAddress + "groups?page=" + pageNo;
                                var nextPageUrl = null;
                                var previousPageUrl = null;
                                if (totalPages > pageNo)
                                    lastPageUrl = serverAddress + "groups?page=" + totalPages;
                                if (totalPages > pageNo)
                                    nextPageUrl = serverAddress + "groups?page=" + (pageNo + 1);
                                if (pageNo > 1)
                                    previousPageUrl = serverAddress + "groups?page=" + (pageNo - 1);
                                res.json({
                                    success: true,
                                    current_page: pageNo,
                                    data: arr,
                                    first_page_url: serverAddress + "groups?page=1",
                                    from: pageOffset + 1,
                                    last_page: totalPages,
                                    last_page_url: lastPageUrl,
                                    next_page_url: nextPageUrl,
                                    per_page: pageLimit,
                                    prev_page_url: previousPageUrl,
                                    to: pageOffset + userss.length,
                                    total: totalCount,
                                    path: serverAddress + "groups",
                                });    
                            }
                    })      

                        }    
                        }
                })
                //   // room.aggregate({ $match: {keyword:{$regex:search_key, $options:'i'}}})
                }
                else if(category_id != '')
                {
                    room.aggregate({
                        $geoNear: {
                            near: {type: "Point", coordinates: [ latitude , longitude ]},
                            distanceField: "dist.calculated",
                            maxDistance:10000,
                            query: { category_id:category_id },
                            spherical: true
                        }}, 
                    { $lookup:{ from: "users", localField: "created_by", foreignField: "_id", as: "created_by" }},{ $lookup:{ from:"reviews",localField:"_id",foreignField:"room_id",as:"reviewInfo"}},{ $project :{"average_rating":{ $avg:"$reviewInfo.rating"},"total_member":{ $size:"$users" },"reviewInfo":1,"group_address":1,"one_to_one_group":1,"users":1,"created_at":1,"updated_at":1,"location":1,"keyword":1,"is_deleted":1,"invitation_blocked":1,"history_enable":1,"is_group":1,"open_to_all":1,"invitations":1,"created_by":1,"icon_url":1,"name":1,"__v":1 }},{$sort:{"average_rating":-1,"total_member":-1}},{ $skip: pageOffset }, { $limit: pageLimit },
                function(err,userss){
                if (err) {
                    res.json({ success: false, message: "Database Error!!! fetch", data: err });
                } else 
                {
                   
                        var arr = [];
                        var index = 0;
                        var sum=0;
                        var avg=0;
                        var count=0;
                        userss.forEach(usrr => {
                            // res.json({"message":usrr});
                            var obj = {
                                group_id: usrr._id,
                                group_name: usrr.name,
                                group_icon_url:usrr.icon_url,
                                keyword: usrr.keyword,
                                one_to_one_group:usrr.one_to_one_group,
                                group_address:usrr.group_address,
                                is_deleted:usrr.is_deleted,
                                working_hour:usrr.working_hour,
                                day_of_working:usrr.day_of_working,
                                cover_photo:usrr.cover_photo,
                                location:usrr.location,
                                total_members: usrr.users.length,
                                creator_id: usrr.created_by.length > 0 ? usrr.created_by[0]._id : '',
                                first_name: usrr.created_by.length > 0 ? usrr.created_by[0].first_name : '',
                                last_name: usrr.created_by.length > 0 ? usrr.created_by[0].last_name : '',
                                created_on: usrr.created_at,
                                reviewInfo:usrr.reviewInfo,
                                average_rating:usrr.average_rating,
                            };
                            arr[index] = obj;
                            index++;
                        });
                        var totalPages = Math.ceil(totalCount / pageLimit)
                        var lastPageUrl = serverAddress + "groups?page=" + pageNo;
                        var nextPageUrl = null;
                        var previousPageUrl = null;
                        if (totalPages > pageNo)
                            lastPageUrl = serverAddress + "groups?page=" + totalPages;
                        if (totalPages > pageNo)
                            nextPageUrl = serverAddress + "groups?page=" + (pageNo + 1);
                        if (pageNo > 1)
                            previousPageUrl = serverAddress + "groups?page=" + (pageNo - 1);
                        res.json({
                            success: true,
                            current_page: pageNo,
                            data: arr,
                            first_page_url: serverAddress + "groups?page=1",
                            from: pageOffset + 1,
                            last_page: totalPages,
                            last_page_url: lastPageUrl,
                            next_page_url: nextPageUrl,
                            per_page: pageLimit,
                            prev_page_url: previousPageUrl,
                            to: pageOffset + userss.length,
                            total: totalCount,
                            path: serverAddress + "groups",
                        });    
                    }
            })      

                }
                else{
                // aggregate.lookup({ from: "users", localField: "created_by", foreignField: "_id", as: "created_by" });
                // aggregate.lookup({ from:"reviews",localField:"_id",foreignField:"room_id",as:"reviewInfo"});
                // aggregate.avg({"average_rating":$reviewInfo.rating});
                // aggregate.lookup({from: "reviews", localField: "_id", foreignField: "room_id", as: "review_info" })
                // aggregate.avg({"$reviewInfo.rating"});
                // aggregate.avg()
                // { $project :{"average_rating":{ $avg:"$reviewInfo.rating"}}}
                // aggregate.project({average_rating:{ $avg:"$reviewInfo.rating"}});
                // aggregate.avg("$reviewInfo.rating");
                // aggregate.skip(pageOffset);
                // aggregate.limit(pageLimit);
                // res.json({ error: true, message: "Database Error!!! fetch2" });
                if(latitude == "" && longitude == "") {
                    room.aggregate({ $lookup:{ from: "users", localField: "created_by", foreignField: "_id", as: "created_by" }},{ $lookup:{ from:"reviews",localField:"_id",foreignField:"room_id",as:"reviewInfo"}},{ $project :{"average_rating":{ $avg:"$reviewInfo.rating"},"reviewInfo":1,"group_address":1,"one_to_one_group":1,"users":1,"created_at":1,"updated_at":1,"location":1,"keyword":1,"is_deleted":1,"invitation_blocked":1,"history_enable":1,"is_group":1,"open_to_all":1,"invitations":1,"created_by":1,"icon_url":1,"name":1,"__v":1 }},{$sort:{"average_rating":-1}},{ $skip: pageOffset }, { $limit: pageLimit },
                    function(err,userss){
                    if (err) {
                        res.json({ success: false, message: "Database Error!!! fetch", data: err });
                    } else 
                    {
                        var arr = [];
                        var index = 0;
                        var sum=0;
                        var avg=0;
                        var count=0;
                        userss.forEach(usrr => {
                            // res.json({message:usrr});
                            var obj = {
                                group_id: usrr._id,
                                group_name: usrr.name,
                                group_icon_url:usrr.icon_url,
                                keyword: usrr.keyword,
                                one_to_one_group:usrr.one_to_one_group,
                                group_address:usrr.group_address,
                                is_deleted:usrr.is_deleted,
                                working_hour:usrr.working_hour,
                                day_of_working:usrr.day_of_working,
                                cover_photo:usrr.cover_photo,
                                location:usrr.location,
                                total_members: usrr.users.length,
                                creator_id: usrr.created_by.length > 0 ? usrr.created_by[0]._id : '',
                                first_name: usrr.created_by.length > 0 ? usrr.created_by[0].first_name : '',
                                last_name: usrr.created_by.length > 0 ? usrr.created_by[0].last_name : '',
                                created_on: usrr.created_at,
                                reviewInfo:usrr.reviewInfo,
                                average_rating:usrr.average_rating,
                            };
                            arr[index] = obj;
                            index++;
                        });
                        var totalPages = Math.ceil(totalCount / pageLimit)
                        var lastPageUrl = serverAddress + "groups?page=" + pageNo;
                        var nextPageUrl = null;
                        var previousPageUrl = null;
                        if (totalPages > pageNo)
                            lastPageUrl = serverAddress + "groups?page=" + totalPages;
                        if (totalPages > pageNo)
                            nextPageUrl = serverAddress + "groups?page=" + (pageNo + 1);
                        if (pageNo > 1)
                            previousPageUrl = serverAddress + "groups?page=" + (pageNo - 1);
                        res.json({
                            success: true,
                            current_page: pageNo,
                            data: arr,
                            first_page_url: serverAddress + "groups?page=1",
                            from: pageOffset + 1,
                            last_page: totalPages,
                            last_page_url: lastPageUrl,
                            next_page_url: nextPageUrl,
                            per_page: pageLimit,
                            prev_page_url: previousPageUrl,
                            to: pageOffset + userss.length,
                            total: totalCount,
                            path: serverAddress + "groups",
                        });
                    }
                })     

                }else{
                room.aggregate( {
                    $geoNear: {
                      near: {type: "Point", coordinates: [ latitude , longitude ]},
                      distanceField: "dist.calculated",
                      maxDistance:10000,
                      spherical: true
                  }},{ $lookup:{ from: "users", localField: "created_by", foreignField: "_id", as: "created_by" }},{ $lookup:{ from:"reviews",localField:"_id",foreignField:"room_id",as:"reviewInfo"}},{ $project :{"average_rating":{ $avg:"$reviewInfo.rating"},"total_member":{ $size:"$users" },"reviewInfo":1,"group_address":1,"one_to_one_group":1,"users":1,"created_at":1,"updated_at":1,"location":1,"keyword":1,"is_deleted":1,"invitation_blocked":1,"history_enable":1,"is_group":1,"open_to_all":1,"invitations":1,"created_by":1,"icon_url":1,"name":1,"__v":1 }},{$sort:{"average_rating":-1,"total_member":-1}},{ $skip: pageOffset }, { $limit: pageLimit },
                function(err,userss){
                    if (err) {
                        res.json({ success: false, message: "Database Error!!! fetch", data: err });
                    } else 
                    {
                        var arr = [];
                        var index = 0;
                        var sum=0;
                        var avg=0;
                        var count=0;
                        userss.forEach(usrr => {
                            // res.json({message:usrr});
                            var obj = {
                                group_id: usrr._id,
                                group_name: usrr.name,
                                group_icon_url:usrr.icon_url,
                                keyword: usrr.keyword,
                                one_to_one_group:usrr.one_to_one_group,
                                group_address:usrr.group_address,
                                is_deleted:usrr.is_deleted,
                                total_members: usrr.users.length,
                                location:usrr.location,
                                creator_id: usrr.created_by.length > 0 ? usrr.created_by[0]._id : '',
                                first_name: usrr.created_by.length > 0 ? usrr.created_by[0].first_name : '',
                                last_name: usrr.created_by.length > 0 ? usrr.created_by[0].last_name : '',
                                created_on: usrr.created_at,
                                reviewInfo:usrr.reviewInfo,
                                average_rating:usrr.average_rating,
                            };
                            arr[index] = obj;
                            index++;
                        });
                        var totalPages = Math.ceil(totalCount / pageLimit)
                        var lastPageUrl = serverAddress + "groups?page=" + pageNo;
                        var nextPageUrl = null;
                        var previousPageUrl = null;
                        if (totalPages > pageNo)
                            lastPageUrl = serverAddress + "groups?page=" + totalPages;
                        if (totalPages > pageNo)
                            nextPageUrl = serverAddress + "groups?page=" + (pageNo + 1);
                        if (pageNo > 1)
                            previousPageUrl = serverAddress + "groups?page=" + (pageNo - 1);
                        res.json({
                            success: true,
                            current_page: pageNo,
                            data: arr,
                            first_page_url: serverAddress + "groups?page=1",
                            from: pageOffset + 1,
                            last_page: totalPages,
                            last_page_url: lastPageUrl,
                            next_page_url: nextPageUrl,
                            per_page: pageLimit,
                            prev_page_url: previousPageUrl,
                            to: pageOffset + userss.length,
                            total: totalCount,
                            path: serverAddress + "groups",
                        });
                        // res.json({success: true});
                    }
                })
                }
                }
            }
        });
    });
 
app.post('/groupDetail', function (req, res) {
        var group_id = req.body.group_id ? req.body.group_id : '';
                room.aggregate({ $match:{_id:ObjectId(group_id)}},{ $lookup:{ from: "users", localField: "created_by", foreignField: "_id", as: "created_by" }},{ $lookup:{ from:"reviews",localField:"_id",foreignField:"room_id",as:"reviewInfo"}},{ $project :{"average_rating":{ $avg:"$reviewInfo.rating"},"reviewInfo":1,"catelogues":1,"category_id":1,"group_address":1,"cover_photo":1,"working_hour":1,"day_of_working":1,"users":1,"created_at":1,"updated_at":1,"location":1,"keyword":1,"is_deleted":1,"invitation_blocked":1,"history_enable":1,"is_group":1,"open_to_all":1,"invitations":1,"created_by":1,"icon_url":1,"name":1,"__v":1 }},{$sort:{"average_rating":-1}},
                function(err,userss){
                    if (err) {
                        res.json({ success: false, message: "Database Error!!! fetch", data: err });
                    } else 
                    {
                        Catalogue.find({room_id:group_id},
                        function(err,catelogues){
                            if(err){
                                res.json({ success: false, message: "Database Error!!! catelog from using groupID", data: err });
                            }else{
                                for(var i in userss)
                                {
                                    var user = userss[i];
                                    var cat_id =user.category_id;
                                    var avg_rat=parseInt(user.average_rating);
                                }
                                Category.find({_id:cat_id},
                                function(err,categories){
                                    if(err){
                                        res.json({ success: false, message: "Database Error!!! not getting category", data: err });
                                    }else{
                                        res.json({success:true,"groupDetail":userss,"avg_rat":avg_rat,"catelogues":catelogues,"categories":categories})
                                    }
                                })
                                
                            }
                        })
                        
                    }
                })
    });

app.post('/reviewDetail', function (req, res) {
        var group_id = req.body.group_id ? req.body.group_id : '';
        review.aggregate({ $match:{room_id:ObjectId(group_id)}},{ $lookup:{ from: "users", localField: "user_id", foreignField: "_id", as: "userss" }},
                function(err,userss){
                    if (err) {
                        res.json({ success: false, message: "Database Error!!! fetch", data: err });
                    } else 
                    {
                        res.json({"ReviewData":userss})
                        // Catalogue.find({room_id:group_id},
                        // function(err,catelogues){
                        //     if(err){
                        //         res.json({ success: false, message: "Database Error!!! catelog from using groupID", data: err });
                        //     }else{
                        //         res.json({success:true,"groupDetail":userss,"catelogues":catelogues})
                        //     }
                        // })
                        
                    }
                })
    });    

    

   app.post('/nearGroup',function(req,res){
        var latitude = req.body.latitude ? parseFloat(req.body.latitude) : '';
        var longitude = req.body.longitude ? parseFloat(req.body.longitude) : '';
        room.find({
            location:
              { $near :
                 {
                   $geometry: { type: "Point",  coordinates: [ latitude, longitude ] },
                   $maxDistance: 3000,
                   $minDistance: 0
                 }
              }
          },
        function(err, data){
            if(err)
            {
                res.json({error:true,"Message":"not getting nearby position"})
            }else{
                res.json({success:true,"data":data})
            }
        }    
        
        )

   })

    app.get('/group', function (req, res) {
        var groupId = req.query.group_id;
        room.aggregate({ $match: { _id: ObjectId(groupId) } }, { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
            function (err, dataa) {
                if (err) {
                    res.json({ error: true, message: err });
                } else {
                    res.json({ data: dataa });
                }
            })
    });

    app.get('/group/members', function (req, res) {
        var groupId = req.query.group_id;
        var pageNo = req.query.page;
        if (pageNo == undefined)
            pageNo = 1;
        pageNo = pageNo - 1 + 1;
        var skipp = perPageLimit * (pageNo - 1);
        var limitt = perPageLimit;

        room.aggregate({ $match: { _id: ObjectId(groupId) } }, { $project: { count: { $size: "$users" } } }, function (err, countInfo) {
            if (err) {
                res.json({ error: true, message: err });
            } else {
                var totalCount = countInfo[0].count;
                room.aggregate([{ $match: { _id: ObjectId(groupId) } }, { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users_data" } }, { $project: { users_data: { $slice: ["$users_data", skipp, limitt] } } }],
                    function (err, dataa) {
                        if (err) {
                            dataa
                            res.json({ error: true, message: err });
                        } else {
                            var totalPages = Math.ceil(totalCount / perPageLimit)
                            var lastPageUrl = serverAddress + "group/members?group_id=" + groupId + "&page=" + pageNo;
                            var nextPageUrl = null;
                            var previousPageUrl = null;
                            if (totalPages > pageNo)
                                lastPageUrl = serverAddress + "group/members?group_id=" + groupId + "&page=" + totalPages;
                            if (totalPages > pageNo)
                                nextPageUrl = serverAddress + "group/members?group_id=" + groupId + "&page=" + (pageNo + 1);
                            if (pageNo > 1)
                                previousPageUrl = serverAddress + "group/members?group_id=" + groupId + "&page=" + (pageNo - 1);
                            res.json({
                                current_page: pageNo,
                                data: dataa[0].users_data,
                                first_page_url: serverAddress + "group/members?group_id=" + groupId + "&page=1",
                                from: skipp + 1,
                                last_page: totalPages,
                                last_page_url: lastPageUrl,
                                next_page_url: nextPageUrl,
                                per_page: perPageLimit,
                                prev_page_url: previousPageUrl,
                                to: skipp + dataa[0].users_data.length,
                                total: totalCount,
                                path: serverAddress + "group/members"
                            });
                        }
                    });
            }
        });
    });


    app.get('/group/messages', function (req, res) {
        var groupId = req.query.group_id;
        message.aggregate([{ $match: { room_id: ObjectId(groupId) } },
        { $lookup: { from: "users", localField: "sender_id", foreignField: "_id", as: "userDetails" } }],
            function (err, messagess) {
                if (err) {
                    res.json({ error: true, message: err });
                } else {
                    room.findOne({ _id: ObjectId(groupId) }, function (err, data) {
                        if (err) {
                            ////////////console.log("error occured ", err);
                            res.json({ error: true, message: err });
                        }
                        else {
                            res.json({ data: messagess, groupDetails: data });
                        }
                    })
                }
            });
    });

    //report 
    app.get('/report', function (req, res) {
        var reportId = req.query.report_id;
        report.aggregate({ $match: { _id: ObjectId(reportId) } }, { $lookup: { from: "users", localField: "to_user_id", foreignField: "_id", as: "to_user_id" } }, { $lookup: { from: "users", localField: "by_user_id", foreignField: "_id", as: "by_user_id" } }, {
            $project: {
                _id: 1,
                created_at: 1,
                reported_for: 1,
                room_id: 1,
                message_content: 1,
                message_id: 1,
                by_user_id: {
                    _id: 1,
                    last_name: 1,
                    first_name: 1,
                    phone: 1
                },
                to_user_id: {
                    _id: 1,
                    last_name: 1,
                    first_name: 1,
                    phone: 1
                }
            }
        },
            function (err, reportInfo) {
                if (err) {
                    res.json({ error: true, message: "Database Error!!! fetch" });
                } else {
                    res.json({ data: reportInfo });
                }
            })
    });

    //reports are here/////////
    app.get('/reports', function (req, res) {
        forMessage = req.query.message;
        var pageNo = req.query.page;
        if (pageNo == undefined)
            pageNo = 1;
        pageNo = pageNo - 1 + 1;
        var skipp = perPageLimit * (pageNo - 1);
        var limitt = perPageLimit;

        report.count({}, function (err, totalCount) {
            if (err) {
                res.json({ error: true, message: "Database Error!!!, count", error: err });
            } else {

                if (forMessage == "true") {
                    str = { reported_for: "message" }
                }
                else {
                    str = { reported_for: { $ne: "message" } }
                }
                report.aggregate({ $match: str }, { $lookup: { from: "users", localField: "to_user_id", foreignField: "_id", as: "to_user_id" } }, { $lookup: { from: "users", localField: "by_user_id", foreignField: "_id", as: "by_user_id" } }, { $skip: skipp }, { $limit: limitt }, {
                    $project: {
                        _id: 1,
                        created_at: 1,
                        description: 1,
                        reported_for: 1,
                        message_content: 1,
                        message_id: 1,
                        room_id: 1,
                        by_user_id: {
                            _id: 1,
                            last_name: 1,
                            first_name: 1
                        },
                        to_user_id: {
                            _id: 1,
                            last_name: 1,
                            first_name: 1
                        }
                    }
                },
                    function (err, reportss) {
                        if (err) {
                            res.json({ error: true, message: "Database Error!!! fetch", error: err });
                        } else {
                            var totalPages = Math.ceil(totalCount / perPageLimit)
                            var lastPageUrl = serverAddress + "reports?page=" + pageNo;
                            var nextPageUrl = null;
                            var previousPageUrl = null;
                            if (totalPages > pageNo)
                                lastPageUrl = serverAddress + "reports?page=" + totalPages;
                            if (totalPages > pageNo)
                                nextPageUrl = serverAddress + "reports?page=" + (pageNo + 1);
                            if (pageNo > 1)
                                previousPageUrl = serverAddress + "reports?page=" + (pageNo - 1);
                            res.json({
                                current_page: pageNo,
                                data: reportss,
                                first_page_url: serverAddress + "reports?page=1",
                                from: 1,
                                last_page: totalPages,
                                last_page_url: lastPageUrl,
                                next_page_url: nextPageUrl,
                                per_page: perPageLimit,
                                prev_page_url: previousPageUrl,
                                to: reportss.length,
                                total: totalCount,
                                path: serverAddress + "reports"
                            });
                        }
                    });
            }
        });
    });

    app.get('/testDate', function (req, res) {
        str = "2018-08-18 11:46:00";

        date1 = new Date();
        // ////////////console.log("date 1 ", date1.toUTCString());

        date2 = new Date(str);
        // ////////////console.log("date 2 ", date2.toUTCString());

        res.send(date2);

    })

    app.get('/fetchAllGroups', function (req, res) {
		var searchKey = req.query.search_key;
		var pageNo = req.query.page > 1 ? parseInt(req.query.page) : 0;
		var pageLimit = req.query.limit > 1 ? parseInt(req.query.limit) : 0;
		var offset = pageLimit * (pageNo - 1);
		var pageOffset = offset > 1 ? offset : 0;
        var q = room.find({ keyword: new RegExp(searchKey, 'i') }, {
            created_at: 0,
            updated_at: 0,
            invitation_blocked: 0,
            history_enable: 0,
            is_group: 0,
            invitations: 0,
            users: 0,
            created_by: 0,
            icon_url: 0,
            __v: 0
        }).limit(pageLimit).skip(pageOffset);
		
		q.exec(function(err, groupsInfo) {
			if (err) {
                res.json({ error: true, message: err });
            } else {
                res.json({ admin_id: constants.ADMIN_ID, data: groupsInfo });
            }
		});
    });

    app.post("/sendMessageByAdmin", function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var scheduleTime = req.body.scheduleTime;
            var room_id = req.body.room_id;

            room_id_arr = room_id.split(",");

            // ////////////console.log("checking public room array in ", room_id_arr.indexOf("5b27477e4b20cb8fab0d6b3a"));

            if (scheduleTime == "immediateBroadcast") {
                ////////////console.log("in true");
                let counter = room_id_arr.length;
                for (let i = 0; i < room_id_arr.length; i++) {
                    commonMethods.data.sendTest(req, null, room_id_arr[i], "", io, function (result) {
                        if (!result) {
                            ////////////console.log("breakinggggggg");
                        }

                        if (counter == 1) {
                            res.send("true");
                        } else {
                            counter--;
                        };
                    });
                }
            } else {
                let currentTime = Date.now().toString()
                ////////////console.log("schedule time is ", typeof scheduleTime)
                date = new Date(parseInt(scheduleTime));
                let scheduleMessage = new ScheduleJob({
                    sender_id: req.body.id,
                    room_ids_to_sent: room_id_arr,
                    content_type: req.body.content_type,
                    file_url: req.body.file_url,
                    file_size: req.body.file_size,
                    created_at: currentTime,
                    updated_at: currentTime,  /// new added
                    thumbnail_data: req.body.thumbnail_data,
                    content: req.body.message_content,
                    scheduled_at: scheduleTime
                })

                scheduleMessage.save((err, scheduleMessageSave) => {
                    if (err) {
                        ////////////console.log("schedule message save ", err);
                    }
                    else {
                        if (scheduleMessageSave) {
                            ////////////console.log("save id is ", scheduleMessageSave._id);
                            ////////////console.log("save date is ", date);

                            var j = schedule.scheduleJob(date, function () {
                                let counter = room_id_arr.length;
                                for (let i = 0; i < room_id_arr.length; i++) {
                                    ////////////console.log("log here we gooo ");
                                    commonMethods.data.sendTest(req, null, room_id_arr[i], scheduleMessageSave._id, io, function (result) {
                                        ////////////console.log("schedule message ", result);
                                    });
                                }
                            });
                        }
                        else {
                            ////////////console.log("something went wrong");
                        }
                    }
                })
                res.send("true");

            }
        }
    });

    app.post("/sendPublicMessageForCategory", function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var scheduleTime = req.body.scheduleTime;

            // //console.log("request in public message in category");

            if (scheduleTime == "immediateBroadcast") {

                category_ids = req.body.category_ids.split(",");
                let counter = category_ids.length;

                for (let i = 0; i < category_ids.length; i++) {
                    commonMethods.data.sendTestForCategory(req, null, category_ids[i], "", io, function (result) {
                        if (!result) {
                            // //console.log("breakinggggggg");
                        }
                        if (counter == 1) {
                            res.send("true");
                        } else {
                            counter--;
                        };
                    });
                }
            } else {
                category_ids = req.body.category_ids.split(",");

                // //console.log("schedule time  is ",scheduleTime );
                // //console.log("in false category id is ", category_ids);
                let currentTime = Date.now().toString()
                // //console.log("type of schedule time is ", typeof scheduleTime)
                date = new Date(parseInt(scheduleTime));
                let scheduleMessage = new ScheduleJob({
                    sender_id: req.body.id,
                    room_ids_to_sent: ["5b27477e4b20cb8fab0d6b3a"],
                    categories: category_ids,
                    content_type: req.body.content_type,
                    file_url: req.body.file_url,
                    file_size: req.body.file_size,
                    created_at: currentTime,
                    updated_at: currentTime,  /// new added
                    thumbnail_data: req.body.thumbnail_data,
                    content: req.body.message_content,
                    scheduled_at: scheduleTime
                })

                scheduleMessage.save((err, scheduleMessageSave) => {
                    if (err) {
                        //console.log("schedule message save err", err);
                    }
                    else {
                        if (scheduleMessageSave) {
                            // //console.log("save id is ", scheduleMessageSave._id);
                            // //console.log("save date is ", date);

                            var j = schedule.scheduleJob(date, function () {
                                let counter = category_ids.length;
                                for (let i = 0; i < category_ids.length; i++) {
                                    // //console.log("log here we gooo for schedule messages ",counter);
                                    commonMethods.data.sendTestForCategory(req, null, category_ids[i], scheduleMessageSave._id, io, function (result) {
                                        // //console.log("schedule message result", result);
                                    });
                                }
                            });
                        }
                        else {
                            //console.log("something went wrong");
                        }
                    }
                })
                res.send("true");
            }
        }
    })

    app.post('/deleteFromGroup', function (req, res) {
        //console.log("Enter Delete From Group >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", Date.now());
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var userId = req.body.id;
                var groupId = req.body.group_id;


                room.find({ _id: ObjectId(groupId) }, function (err, roomData) {
                    if (err) {
                        res.json({ error: true, message: err })
                    } else {
                        if (roomData.length > 0) {
                            if (roomData[0].users.length == 1) {
                                message.deleteMany({ room_id: ObjectId(groupId) }, function (err, deleteOne) {
                                    if (err) {
                                        ////////////console.log("error at line 2687 while removing messages of a room ", err);
                                    }
                                })
                                room.remove({ _id: ObjectId(groupId) }, function (err, deleteDone) {
                                    if (err) {
                                        res.json({ error: true, message: err })
                                    } else {
                                        res.json({ error: false, message: "delete from group done" })
                                    }
                                })
                            } else {

                                // socket.leave(userId);

                                ////////////console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>UserId", userId);
                                //console.log("Processing Delete From Group >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", Date.now());
                                room.update({ _id: ObjectId(groupId) }, { $pull: { users: { user_id: ObjectId(userId) } } }, function (err, removeDone) {
                                    if (err) {
                                        res.json({ error: true, message: err })
                                    } else {

                                        //////////console.log("remove done in deleteFrom group")
                                        res.json({ error: false, message: "delete from group done" })
                                    }
                                })
                            }

                        } else {
                            //////////console.log("IN else of remove from group");
                            res.json({ error: false, message: "delete from group done" })

                        }
                    }
                })
            } else {
                res.json({ result1 })
            }
        })
        //console.log("Exit Delete From Group >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", Date.now());
    })

    app.post('/dissolveGroup', function (req, res) {
        ////////////console.log("api_key>>>>>", req.body.api_key)
        //console.log("Enter Dissolve Group >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", Date.now());
        authentication.data.authenticateToken(req, function (result1) {
            if (result1 == true) {
                var userId = req.body.id;
                var groupId = req.body.group_id;
                var removedByName = req.body.removed_by_name;
                // //////////console.log("group id", groupId)

                room.find({ _id: ObjectId(groupId) }, { users: 1, _id: 0 }, function (err, data) {
                    if (err) {
                        res.json({ error: true, message: err })
                    }
                    else {
                        //////////console.log("user id in dissolve group", data[0])
                        if (data.length > 0) {
                            let completionCount = data[0].users.length;

                            room.updateMany({ _id: ObjectId(groupId), "users.user_id": ObjectId(userId) }, { $set: { "is_deleted": true, "users.$.status": "left", updated_at: Date.now().toString() } },
                                function (err, roomUpdateDone) {
                                    //room.deleteOne({_id:ObjectId(groupId)}, function(err,removed){
                                    if (err) {
                                        res.json({ error: true, message: err })
                                    }
                                    else {

                                        io.to(groupId).emit("dissolveRoom", { roomId: groupId });

                                        //sending message on dissolve
                                        room.aggregate([{ $match: { _id: ObjectId(groupId) } }, { $unwind: "$users" },
                                        { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                                        { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                                        {
                                            $group: {
                                                '_id': {
                                                    "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by", "created_at": "$created_at",
                                                    "is_group": "$is_group", "invitation_blocked": "$invitation_blocked", "history_enable": "$history_enable"
                                                }, 'users': { '$push': '$users' }
                                            }
                                        }],
                                            function (err, data) {
                                                if (err) {
                                                    next({ "error": true, "message": "Database Error!!!!" });
                                                } else {
                                                    var responseData = data[0];
                                                    //  ////////////console.log("here we go >>>>>>>>>> ", responseData);
                                                    if (responseData != undefined) {

                                                        var currentTime = Date.now().toString();
                                                        var messageData = new message({
                                                            sender_id: ObjectId(userId),
                                                            room_id: ObjectId(groupId),
                                                            content_type: "alert",
                                                            retain_count: responseData.users.length,
                                                            content: removedByName + " has dissolved the group",
                                                            created_at: currentTime,
                                                            updated_at: currentTime
                                                        });
                                                        //console.log("Processing Dissolve Group >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", Date.now());
                                                        messageData.save(function (err, messageInfo) {
                                                            if (err) {
                                                                next({ "error": true, "message": "Database Error! save message.", data: err });
                                                            } else {
                                                                message.aggregate([{ $match: { _id: ObjectId(messageInfo._id) } },
                                                                { $lookup: { from: "users", localField: "sender_id", foreignField: "user_id", as: "sender_id" } }],
                                                                    function (err, messageInformation) {
                                                                        if (err) {
                                                                            res.json({ "error": true, "message": "Database Error! fetch message info." });
                                                                        } else {
                                                                            if (Array.isArray(messageInformation)) {
                                                                                var messageDetail = messageInformation[0];
                                                                                responseData.users.forEach(userss => {
                                                                                    try {
                                                                                        if (userss.user_id[0].socket_id == '' && userss.user_id[0].notification_token != '' && userss.status == 'joined') {
                                                                                            sendNotification([userss.user_id[0].notification_token], messageDetail);
                                                                                        }
                                                                                    } catch (e) { }
                                                                                });
                                                                                try {
                                                                                    if (userInfo.socket_id != '')
                                                                                        io.nsps['/'].sockets[userInfo.socket_id].leave(groupId);
                                                                                } catch (e) { }
                                                                                res.json({ error: false, message: "removed", data: roomUpdateDone })

                                                                                var usersss = data[0].users;
                                                                                //////////console.log("usersss are hare ",usersss);

                                                                                usersss.forEach(userss => {
                                                                                    //////////console.log("userss id is ",userss.user_id[0]._id); 

                                                                                    user.findOne({ _id: ObjectId(userss.user_id[0]._id) }, function (err, roomDataInfo) {
                                                                                        if (err) {
                                                                                            //////////console.log("Error",err);
                                                                                        } else {
                                                                                            //////////console.log("roomDataInfo",roomDataInfo);
                                                                                            if (roomDataInfo) {

                                                                                                if (roomDataInfo.socket_id != '') {
                                                                                                    //////////console.log("emiting onMessageReceived in dissolveGroup",roomDataInfo.socket_id);
                                                                                                    io.to(roomDataInfo.socket_id).emit("onMessageReceived", messageDetail);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    })

                                                                                })

                                                                            }
                                                                        }
                                                                    });
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        ///////////////////////////////////////////////////////////////////////


                                    }
                                });
                        }
                        else {
                            res.json({ error: false, message: "Group Dissolve" });
                        }

                    }
                })
            }
            else {
                res.json({ result1 })
            }
        }
        )
        //console.log("Exit Dissolve Group >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", Date.now());
    })

    //////////////category///////////
    app.post('/addCategory', function (req, res) {
        var categoryName = req.body.name ? req.body.name : '';
        var categoryImage = req.body.image ? req.body.image : '';
        var categoryType=req.body.type ? req.body.type : '';
        category = new Category({
            name: categoryName,
            created_at: Date.now().toString(),
            image: categoryImage,
            type:categoryType
        })
        
        category.save(function (err, done) {
            if (err) {
                //////////////console.log("error occurred ", err)
                res.json({ error: true, message: err });
            }
            else {
                res.json({ error: false, message: "category saved " });
            }
        });
    })
    
    app.get('/fetchCategory', function (req, res) {
            var slider_images=[];
            slider_images[0]="https://tecmaths.s3.ap-south-1.amazonaws.com/sliderImages/slider3.jpeg";
            slider_images[1]="https://tecmaths.s3.ap-south-1.amazonaws.com/sliderImages/slider2.jpeg";
            slider_images[2]="https://tecmaths.s3.ap-south-1.amazonaws.com/sliderImages/slider1.jpeg";
            slider_images[3]="https://tecmaths.s3.ap-south-1.amazonaws.com/sliderImages/slider4.jpeg";
            var where = {};
            if(req.query.category_id){
                var category_id = req.query.category_id;
                if (category_id != '') {
                    var where = {_id:category_id}
                }
            }   
            Category.find(where, function(err, data) {
                if (err) {
                //////////////console.log("error occurred ", err);
                }else{
                    if (data.length > 0) {
                        res.json({ error: false, message: "Category Data Available", data: data,"slider_images":slider_images });
                }else {
                    res.json({ error: true, message: "No categories found." });
                }
            }
        })
    })

    app.post('/updateCategory',function (req , res) {
        // res.json({"jhjh":"fdfd"});
        categoryId = req.body.categoryId;

        // res.json({ error: false, message: categoryId});
        categoryName = req.body.name;
        categoryType = req.body.type;
        var categoryImage = req.body.image ? req.body.image : '';
        
        if (categoryId == '' ||  categoryName == '' ) {
            res.json({ error: true, message: "All Fields Are Required" ,data:req.body});
        }else{
            if(categoryImage != ""){
                Category.update( { _id: ObjectId(categoryId) }, {
                    $set: { name: categoryName,image: categoryImage,type:categoryType}}, 
                    function (err, data2) {
                        if (err) {
                            res.json({ error: true, message: err });
                        }else {
                            res.json({ error: false, message: "Category Updated"});
                        }
                });
            }else{
                // res.json({ error: false, message: categoryId});
                Category.update( { _id: ObjectId(categoryId) }, {
                    $set: { name: categoryName,type:categoryType}}, 
                    function (err, data2) {
                        if (err) {
                            res.json({ error: true, message: err });
                        }else {
                            res.json({ error: false, message: "Category Updated" });
                        }
                }); 
            }
        }
    });

app.post('/deleteCategory',function (req , res) {
    categoryId = req.body.categoryId;
     if (categoryId == '') {
        res.json({ error: true, message: "Category Id Is Required", data:req.body});
    }else{
        Category.deleteOne( { _id: ObjectId(categoryId) },
            function (err, data2) {
                if (err) {
                    res.json({ error: true, message: err });
                }else {
                    res.json({ error: false, message: "Category Deleted" });
                }
            });
        }
    });

    app.post('/deleteCategoryKeyword',function (req , res) {
        categorykeywordId = req.body.categorykeywordId;
         if (categorykeywordId == '') {
            res.json({ error: true, message: "Category keyword Id Is Required", data:req.body});
        }else{
            categoryKeywords.deleteOne( { _id: ObjectId(categorykeywordId) },
                function (err, data2) {
                    if (err) {
                        res.json({ error: true, message: err });
                    }else {
                        res.json({ error: false, message: "Category keyword Deleted" });
                    }
                });
            }
        });
    
    
      app.post('/addCategoryKeyword', function (req, res) {
           categoryKeywordsObj = new categoryKeywords({
                categoryId:req.body.categoryID,
                name: req.body.name,
                created_at: Date.now().toString()
            })
    
            categoryKeywordsObj.save(function (err, done) {
                if (err) {
                    res.json({ error: true, message: err });
                }
                else {
                    res.json({ error: false, message: "Category Keyword Saved" });
                }
            });
        })
    
        app.post('/fetchKeywordBycategory', function (req, res) {
            // console.log(req.body);.
            let where = {};
            if (req.body.categoryId != '') {
                where = { "categoryId":req.body.categoryId};
            }else if(req.body.keywordsId != ''){
                where = { "_id":req.body.keywordsId};
            }
            categoryKeywords.find(where, function (err, data) {
                if (err) {
                    //////////////console.log("error occurred ", err);
                }
                else {
                    if (data.length > 0) {
                        res.json({ error: false, message: "All category Keywords ", data: data });
                    }
                    else {
                        res.json({ error: true, message: "No categories keywords found." });
                    }
                }
            })
        })
    
    
         app.post('/updateCategoryKeyword',function (req , res) {
            categoryId = req.body.categoryId;
            keywordId = req.body.keywordId;
            keywordName = req.body.name;
            if (keywordId == '' || categoryId == '' ||  keywordName == '') {
                res.json({ error: true, message: "All Fields Are Required" ,data:req.body});
            }else{
                categoryKeywords.update( { _id: ObjectId(keywordId) }, {
                    $set: { name: keywordName,categoryId:categoryId}}, 
                    function (err, data2) {
                        if (err) {
                            res.json({ error: true, message: err });
                        }else {
                            res.json({ error: false, message: "Category keyword Update" });
                        }
                    });
                }
        });
    
          app.post('/fetchKeywordBycategoryJoin', function (req, res) {
            // console.log(req.body);.
            let where = {};
    
            categoryKeywords.find(where, function (err, data) {
                if (err) {
                    //////////////console.log("error occurred ", err);
                }
                else {
                    if (data.length > 0) {
                        res.json({ error: false, message: "All category Keywords ", data: data });
                    }
                    else {
                        res.json({ error: true, message: "No categories keywords found." });
                    }
                }
            })
        })
    
        app.get('/fetchKeywordWithCategory', function (req, res) {
            
            Category.find({}, function (err, data) {
                let responseData = [];
                 if (err) {
                    //////////////console.log("error occurred ", err);
                }
                else {
                    if (data.length > 0) {
    
                        data.forEach( function(categoryElement,index){
                            let categoryData = categoryElement._doc; //{keywords : []};
    
                           //categoryData = Object.assign(categoryData, categoryElement._doc);
                            
                           // let categoryData = {categoryInfo : categoryElement._doc, keywords : []};
    
                            categoryKeywords.find({ "categoryId":categoryElement._id}, function (err, keywordData) {                      
                                if (err) {
                                    //////////////console.log("error occurred ", err);
                                }else {
                                     if (keywordData.length > 0) {
                                        
                                         let catData = Object.assign(categoryData, {keywords: keywordData});
                                         responseData[index] = catData;
                                         console.log('keywordData', categoryData);                                  
                                      }else {
                                       let catData = Object.assign(categoryData, {keywords: []});
                                         responseData[index] = catData;
                                         console.log('keywordData', keywordData);   
                                     }
                                }
                            })
                           
                            
                         }) 
                         res.json({ error: false, message: "All category with Keywords ", data: responseData });
                    }
                    else {
                        res.json({ error: true, message: "No categories keyword found." });
                    }
                }
            })
           
        })
       
    
        app.post('/fetchAllCategoryWithUser', function (req, res) {
            user_id = req.body.user_id;
            Category.find({}, function (err, categoryData) {
                if (err) {
                    //////////////console.log("error occurred ", err);
                }
                else {
                    user.findOne({ _id: ObjectId(user_id) }, function (err, userData) {
                        if (err) {
                            //////////////console.log("error occurred while fetching user in fetchAllCategoryWithUser ", err);
                        }
                        else {
                            if (userData) {
                                userCategoryList = userData.categories;
                                //////////////console.log("sending response");
                                res.json({ error: false, allCategories: categoryData, subscribedCategories: userCategoryList })
                            } else {
                                res.json({ error: true, message: "User not found" })
                            }
    
                        }
                    })
                }
            })
        })
    
        app.post('/addCategoryToUser', function (req, res) {
            user_id = req.body.user_id;
            category_id = req.body.category_id;
            to_add = req.body.to_add; //add or remove
    
            if (to_add == "true") {
                user.update({ _id: ObjectId(user_id) }, { $push: { categories: ObjectId(category_id) } }, function (err, message) {
                    if (err) {
                        //////////////console.log("error on line 2843 ", err);
                        res.json({ error: true, message: "Error occurred", data: err });
                    }
                    else {
                        res.json({ error: false, message: "Success", data: message });
                    }
    
                })
            }
            else {
                user.update({ _id: ObjectId(user_id) }, { $pull: { categories: ObjectId(category_id) } }, function (err, message) {
                    if (err) {
                        //////////////console.log("error on line 2843 ", err);
                        res.json({ error: true, message: "Error occurred", data: err });
                    }
                    else {
                        res.json({ error: false, message: "Success", data: message });
                    }
    
                })
            }
        })
    
        ///////////////////////////////////////////////////////////////////////////////////////////////
        app.get('/setDeliveredAll', function (req, res) {
    
            message.updateMany({}, { $set: { delivered_to: [] } }, function (err, singleRoomData) {
                if (err) {
                    //////////////console.log("error occurred ", err)
                }
                else {
                    //////////////console.log("Room update is done ", singleRoomData);
                    room.find({}, { users: 1 }, function (err, roomsData) {
                        if (err) {
    
                            //////////////console.log("here is errr ", err)
                        }
                        else {
                            if (roomsData) {
                                roomsData.forEach(roomElement => {
                                    usersArray = [];
                                    for (i = 0; i < roomElement.users.length; i++) {
                                        usersArray[i] = roomElement.users[i].user_id;
                                    }
    
                                    message.updateMany({ room_id: roomElement._id }, { $set: { delivered_to: usersArray } }, function (err, messageUpdate) {
                                        //////////////console.log("message updated here ", messageUpdate);
                                    });
                                })
                            }
                        }
                    })
                }
            });
    
        })
    
        app.get('/retOTP', function (req, res) {
            ////////////////console.log('retOTP');
            otp.find({ phone: '9460994860' }, function (err, otpData) {
                if (err) {
                    res.json({ 'error': true, 'message': "Failed to find OTP" });
                }
                else {
                    if (otpData.length > 0) {
                        res.json({ 'error': false, 'otp': otpData[0].otp });
                    }
                    else {
                        res.json({ 'error': 'Please send OTP first.' });
                    }
                }
            });
        })
    
        //for updating rooms to is_deleted
        app.get('/addDeletedInRooms', function (req, res) {
            room.find({}, { "users": 1 }, function (err, data) {
                if (err) {
                    //////////////console.log("error is ", error)
                }
                else {
                    let leftUserCount = 0;
                    data.forEach(element => {
                        for (i = 0; i < element.users.length; i++) {
                            if (element.users[i].status == "left") {
                                leftUserCount++;
                                if (leftUserCount == element.users.length) {
                                    //////////////console.log("left user count is ", leftUserCount, " element user length is ", element.users.length)
                                    //////////////console.log(" room id is ", element._id);
                                    room.update({ _id: element._id }, { $set: { is_deleted: true } }, function (err, data) {
                                        //////////////console.log(err);
                                        //////////////console.log("data is ", data);
                                    })
                                }
                            }
                        }
                    })
                }
            })
        })
    
        //for updating rooms to is_deleted
        app.get('/setMarkReadAll', function (req, res) {
    
            message.updateMany({}, { $set: { read_id: [] } }, function (err, singleRoomData) {
                if (err) {
                    //////////////console.log("error occurred ", err)
                }
                else {
                    //////////////console.log("Room update is done ", singleRoomData);
                    room.find({}, { users: 1 }, function (err, roomsData) {
                        if (err) {
    
                            //////////////console.log("here is errr ", err)
                        }
                        else {
                            if (roomsData) {
                                roomsData.forEach(roomElement => {
                                    usersArray = [];
                                    for (i = 0; i < roomElement.users.length; i++) {
                                        usersArray[i] = roomElement.users[i].user_id;
                                    }
    
                                    message.updateMany({ room_id: roomElement._id }, { $set: { read_id: usersArray } }, function (err, messageUpdate) {
                                        //////////////console.log("message updated here ", messageUpdate);
                                    });
                                })
                            }
                        }
                    })
                }
            });
    
        });
    
        app.get('/testTime', function (req, res) {
            // currentDate = Date.now();
            //     //////////////console.log("current date in public messages ", currentDate);
            //     currentDate = parseInt(currentDate) - (86400000*2);
            //     currentDate = currentDate + "";
            //     message.aggregate([{ $match: { room_id: ObjectId("5b27477e4b20cb8fab0d6b3a"), updated_at: { $gt: currentDate } } }, {$sort : {created_at : -1} }, 
            //         { $group: { _id: "$room_id", messages: { $push: "$$ROOT" } } }], function (err, messages) {
            //         if (err) {
            //             //////////////console.log(err);
            //         } else {
            //             res.json({ messageData: messages });
            //         }
            //     });
    
            var d = new Date();
            d.setDate(d.getDate() - 2);
            //////////////console.log("first >> ", d);
    
            //////////////console.log("in utc here ", d.toUTCString());
            var ddd = new Date(d.toUTCString());
    
            var dd = new Date(ddd.getFullYear(), ddd.getMonth(), ddd.getDate());
            //////////////console.log("new date >>", dd);
    
            //////////////console.log("main time >> ", dd.getTime() / 1000);
    
            res.json({ error: false, message: "main date is " + dd.getTime() });
        })
    
        // delete for all by creator
        app.post('/deleteAllMessageByCreator', function (req, res) {
            var message_ids = req.body.message_ids.toString();
            var room_id = req.body.room_id;
            var user_id = req.body.user_id;
            var message_id = [];
    
            message_ids.split(",").forEach(message_idd => {
    
                message_id.push(ObjectId(message_idd));
            });
    
    
            room.findOne({ _id: ObjectId(room_id) }, function (err, messageInfo) {
                if (err) {
                    //////////////console.log("Error Occured in find ", err);
                    res.json({ error: true, message: "error in finding room" });
                } else {
                    if (messageInfo == null) {
                        res.json({ error: true, message: "room not found!" });
                        return;
    
                    } else {
                        if (messageInfo.created_by == user_id) {
                            message.updateMany({ _id: { '$in': message_id } }, { $set: { is_withdraw: true, updated_at: Date.now().toString() } }, function (err, info) {
                                if (err) {
                                    //////////////console.log("Error Occured in delete ", err);
                                    res.json({ error: true, message: "error in deleting messages" });
    
                                } else {
                                    //////////////console.log("Message Deleted For All1");
                                    res.json({ error: false, message: "messages successfully deleted" });
    
    
                                    var users = messageInfo.users;
                                    users.forEach(user1 => {
                                        user.find({ _id: ObjectId(user1.user_id) }, function (err, findInfo) {
                                            if (err) {
    
                                            } else {
                                                if (findInfo[0].socket_id != '' && findInfo[0].socket_id != "undefined") {
                                                    io.to(findInfo[0].socket_id).emit("withdrawMessage", { room_id: room_id, message_id: message_ids, is_withdraw: 1, sender_id: user_id });
                                                    //////////////console.log("emit1 >>>>> ",findInfo[0].socket_id, " >>>>>>>>>>>>>>>>> ", room_id);
                                                } else {
                                                    if (findInfo[0].device_type == "Android") {
    
                                                        //console.log("notification_token for android is here111>>>>>", findInfo[0].notification_token);
                                                        // //console.log("notification for the android is here111>>>>>>",  detail);
                                                        var message = "withdraw";
                                                        var detail = { "room_id": room_id, "messageId": message_ids };
    
                                                        sendNoti([findInfo[0].notification_token], message, detail);
    
                                                    }
                                                    else {
                                                        //console.log("in here here we go line 3588");
                                                        var message = "withdraw";
                                                        var detail = { "room_id": room_id, "messageId": message_ids };
                                                        sendNotificationForIOS([findInfo[0].notification_token], detail, message, "message", '')
                                                    }
    
                                                }
    
    
                                            }
                                        })
                                    })
    
                                }
    
                            });
                        } else {
                            res.json({ error: false, message: "you are not a creator" });
                        }
                        //////////////console.log("Delete ID Update");
                    }
                }
            });
    
    
            //////////////console.log("No Data Updated");
        })
    
        // delete for all by sender
        app.post('/deleteAllMessage', function (req, res) {
    
            var message_ids = req.body.message_ids.toString();
            var user_id = req.body.user_id;
            var room_id = req.body.room_id;
            var message_id = [];
    
            //////////////console.log("hello",message_ids);
    
            message_ids.split(",").forEach(message_idd => {
    
                message_id.push(message_idd);
            });
    
            let i = 0;
            message_id.forEach((m_id, i) => {
                //////////////console.log("message id is: ", m_id)
                if (m_id.length == 24) {
                    message.findOne({ _id: ObjectId(m_id) }, function (err, messageInfo) {
                        if (err) {
                            //////////////console.log("Error Occured in find ", err);
                            res.json({ error: true, message: "error in finding message" });
                        } else if (!messageInfo) {
                            res.json({ error: true, message: "No message found" })
                        } else {
                            i++
                            if (messageInfo.sender_id == user_id) {
                                message.updateOne({ _id: ObjectId(m_id) }, { $set: { is_withdraw: true, updated_at: Date.now().toString() } }, function (err, info) {
                                    if (err) {
                                        //////////////console.log("Error Occured in delete ", err);
                                        res.json({ error: true, message: "error in deleting message" });
                                    } else {
                                        // //////////////console.log("info in condidtion ",user_id.socket_id );
                                        //////////////console.log("length",i,message_id.length);
                                        if (i == message_id.length) {
    
                                            res.json({ error: false, message: "message successfully deleted" });
                                        }
    
                                        room.findOne({ _id: ObjectId(room_id) }, function (err, messageInfo1) {
                                            if (err) {
                                                //////////////console.log("Error>>>",err);
                                            } else {
                                                if (messageInfo1 != null) {
                                                    var users = messageInfo1.users;
                                                    //////////////console.log("in messageInfo1");
                                                    users.forEach(user1 => {
                                                        user.find({ _id: ObjectId(user1.user_id) }, function (err, findInfo) {
                                                            if (err) {
    
                                                            } else {
                                                                if (i == message_id.length) {
                                                                    if (findInfo[0].socket_id != '' && findInfo[0].socket_id != "undefined") {
                                                                        io.to(findInfo[0].socket_id).emit("withdrawMessage", { room_id: room_id, message_id: message_ids, is_withdraw: 1, sender_id: user_id });
                                                                        //console.log("emit1 >>>>> ", findInfo[0].socket_id, " >>>>>>>>>>>>>>>>> ", room_id);
                                                                    } else {
                                                                        if (findInfo[0].device_type == "Android") {
    
                                                                            //console.log("notification_token for android is here111>>>>>", findInfo[0].notification_token);
                                                                            // //console.log("notification for the android is here111>>>>>>",  detail);
                                                                            var message = "withdraw";
                                                                            var detail = { "room_id": room_id, "messageId": message_ids };
    
                                                                            sendNoti([findInfo[0].notification_token], message, detail);
    
                                                                        }
                                                                        else {
                                                                            //console.log("in here here we go line 3588");
                                                                            var message = "withdraw";
                                                                            var detail = { "room_id": room_id, "messageId": message_ids };
                                                                            sendNotificationForIOS([findInfo[0].notification_token], detail, message, "message", '')
                                                                        }
    
                                                                    }
    
                                                                }
    
                                                            }
                                                        })
    
                                                        // //////////////console.log("emit2",user);
                                                        // //////////////console.log("emit2",users);
                                                    })
    
                                                } else {
                                                    //////////////console.log("Not done");
                                                }
                                            }
    
                                        })
                                    }
                                });
                            } else {
                                // //////////////console.log("length",i,message_id.length);
                                if (i == message_id.length) {
                                    res.json({ error: true, message: "you are not a sender" });
                                }
    
                            }
                        }
                    });
    
                } else {
                    //console.log("hihihi>>>>>>>>>>>");
                    res.json({ error: true, message: "faild to delete message" });
                }
    
            });
            //////////////console.log("No Data Updated");
        })
    
        //for removing e-monitor by creator
        app.post('/removeEmonitor', function (req, res) {
            var room_id = req.body.room_id;
            var user_id = req.body.user_id;
            var creator_id = req.body.creator_id;
            var display_name = req.body.display_name;
            var userName = req.body.user_name;
    
            room.findOne({ _id: ObjectId(room_id) }, function (err, roomInfo) {
                if (err) {
                    //////////////console.log("error in finding room ", err);
                    res.json({ error: true, message: "error in roomInfo" });
                } else {
                    //////////////console.log("in roomInfo ", roomInfo);
                    if (roomInfo.created_by == creator_id) {
                        var users = roomInfo.users;
    
                        room.update({ _id: ObjectId(room_id), 'users.user_id': ObjectId(user_id) }, { $set: { 'users.$.is_admin': false, updated_at: Date.now().toString() } }, function (err, done) {
                            if (err) {
                                res.json({ error: true, message: "error in update room" });
                            } else {
    
    
                                commonMethods.data.getRoom(room_id, (mInfo) => {
                                    if (mInfo) {
                                        io.to(room_id).emit("updateRoom", { roomId: mInfo });
                                    }
                                });
    
                                //////////////console.log("in hum", done);
                                res.json({ error: false, message: "removed E-Monitor successfully!!" });
    
                                user.findOne({ _id: ObjectId(user_id) }, function (err, monitorInfo) {
                                    if (err) {
                                        res.json({ error: true, message: "Database Error!! fetch new monitor info" });
                                    } else {
                                        room.aggregate([{ $match: { _id: ObjectId(room_id) } }, { $unwind: "$users" },
                                        { $lookup: { from: "users", localField: "users.user_id", foreignField: "_id", as: "users.user_id" } },
                                        { $lookup: { from: "users", localField: "created_by", foreignField: "_id", as: "created_by" } },
                                        {
                                            $group: {
                                                '_id': {
                                                    "_id": "$_id", "name": "$name", "icon_url": "$icon_url", "created_by": "$created_by",
                                                    "created_at": "$created_at", "is_group": "$is_group", "invitation_blocked": "$invitation_blocked",
                                                    "history_enable": "$history_enable"
                                                }, 'users': { '$push': '$users' }
                                            }
                                        }], function (err, dataaa) {
                                            if (err) {
                                                res.json({ error: true, message: "Database Error!! fetch room info" });
                                            } else {
                                                if (dataaa) {
                                                    var responseData = dataaa[0];
                                                    var currentTime = Date.now().toString();
                                                    var messageData = new message({
                                                        sender_id: ObjectId(user_id),
                                                        room_id: ObjectId(room_id),
                                                        content_type: "alert",
                                                        retain_count: responseData.users.length,
                                                        content: userName + ' removed ' + display_name + ' from E-Monitor',
                                                        created_at: currentTime,
                                                        updated_at: currentTime
                                                    });
    
                                                    messageData.save(function (err, messageInfo) {
                                                        if (err) {
                                                            next({ error: true, message: "Database Error!! save message", data: err });
                                                        } else {
                                                            responseData.users.forEach(userss => {
                                                                if (userss.user_id[0].socket_id != '' && userss.status == 'joined') {
                                                                    io.to(userss.user_id[0].socket_id).emit("onMessageReceived", messageInfo);
    
                                                                } else if (userss.user_id[0].notification_token != '' && userss.status == 'joined' && userss.user_id[0]._id != monitorInfo._id) {
    
                                                                    // if (userss.user_id[0].muted_room.indexOf(ObjectId(room_id)) < 0) {
                                                                    sendNotification([userss.user_id[0].notification_token], messageInfo);
    
    
                                                                    sendNotificationForIOS([userss.user_id[0].notification_token], messageInfo, userName + ' removed ' + display_name + ' from E-Monitor111', "message", monitorInfo.first_name);
                                                                    // }
    
                                                                }
                                                            });
                                                            if (monitorInfo.device_type == "Android") {
                                                                // if (monitorInfo.muted_room.indexOf(ObjectId(room_id)) < 0) {
                                                                sendAlertNotification([monitorInfo.notification_token], userName + ' removed you from E-Monitor');
                                                                // }
                                                            } else {
                                                                // if (monitorInfo.muted_room.indexOf(ObjectId(room_id)) < 0) {
                                                                sendNotificationForIOS([monitorInfo.notification_token], messageInfo, userName + ' removed ' + display_name + ' from E-Monitor111 ', "message", monitorInfo.first_name);
                                                                // }
                                                            }
    
    
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        })
    
    
                    } else {
                        res.json({ error: false, message: "creator_id and user_id not match in a room" });
                    }
                }
    
            });
        })
    
        app.post('/updateReadInfo', function (req, res) {
            var user_id = req.body.user_id;
            var message_ids = req.body.message_ids.toString();
            var status = req.body.status;
            var messages = [];
    
            console.log("ids is here>>>>>>", message_ids, '>>>>>>>>>>', user_id, '>>>>>>>>', status);
    
            message_ids.split(",").forEach(message_idd => {
    
                messages.push(message_idd);
            });
            let i = 0;
            //////////////console.log("messages",messages);
    
            if (messages) {
    
                messages.forEach((message_id, i) => {
    
                    if (message_id.length == 24) {
                        //console.log("if message id length are 12 >>>>>>>");
    
                        message.findOne({ _id: ObjectId(message_id) }, function (err, findInfo1) {
                            if (err) {
                                //////////////console.log("error in finding message",err);
                            } else {
                                i++;
                                if (findInfo1) {
    
    
                                    message.updateOne({ _id: ObjectId(message_id), 'read_info.user_id': ObjectId(user_id) }, { $set: { 'read_info.$.status': status, 'read_info.$.date': Date.now().toString(), updated_at: Date.now().toString() } }, function (err, Info) {
                                        if (err) {
                                            res.json({ error: true, message: "error in finding message" });
                                        } else {
                                            if (Info == null) {
                                                //////////////console.log("hell no");
    
                                                return;
                                            } else {
                                                //////////////console.log("hell");
                                                if (i == messages.length) {
                                                    res.json({ error: false, message: "readInfo updated!" });
                                                }
    
    
    
    
                                                room.findOne({ _id: ObjectId(findInfo1.room_id) }, function (err, messageInfo1) {
                                                    if (err) {
    
                                                    } else {
                                                        if (messageInfo1 != null) {
                                                            var users = messageInfo1.users;
    
                                                            users.forEach(user1 => {
                                                                user.findOne({ _id: ObjectId(user1.user_id) }, function (err, findInfo) {
                                                                    if (err) {
    
                                                                    } else {
    
                                                                        message.findOne({ _id: ObjectId(message_id) }, function (err, findInfo2) {
                                                                            if (err) {
    
                                                                            } else {
    
                                                                            }
    
                                                                            if (findInfo.socket_id !== "undefined" && findInfo.socket_id !== '') {
                                                                                //console.log("emiting readInfo");
    
                                                                                io.to(findInfo.socket_id).emit("updateReadInfo", { status: status, messageId: message_id, user_id: req.body.user_id, date: Date.now().toString() });
    
                                                                            }
    
                                                                        })
                                                                    }
                                                                })
                                                            })
    
                                                        } else {
    
                                                        }
                                                    }
    
                                                })
                                            }
                                        }
                                    });
                                } else {
    
                                }
                            }
                        })
                    }
    
                })
            }
    
    
        })

    app.get('/VOIP', function (req, res) {
        var i = 5;
        // ////console.log("hello from voip");
        if (i == 5) {
            // //console.log("if i is 5");

            sendNotificationForIOSVOIP.mSend();

            res.json({ error: false, message: "done" });
        } else {
            res.json({ error: true, message: "done" });
        }

    })

    // app.get('/testNotif', (req, res) => {
    //     sendNotificationForIOS(['94b29374-a128-4e42-967b-fbd0ac18e42d'], {test : "test"}, "MESSAGE", type = "message", "TITLE")
    // })

    app.post('/refreshAccessToken', function (req, res) {

        authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var userId = req.body.id;
                var newAccessToken = authentication.data.generateAccessToken();

                //console.log("new encrypted access token is >>>>>>>> ", newAccessToken);

                user.updateOne({ _id: ObjectId(userId) }, { $set: { access_token: newAccessToken } }, function (err, userData) {
                    if (err) {
                        //console.log("error occurred ", err);
                    }
                    else {
                        if (userData) {

                            res.json({ error: false, message: "Access Token refreshed", access_token: newAccessToken });
                        } else {
                            res.json({ error: true, message: "Access Token not refreshed" });
                        }

                    }
                })

            } else {
                res.json({ error: true, message: "Invalid Access Token!" });
            }
        })
    })

    app.post('/getAWSCredential', function (req, res) {

        //console.log("out");
        if (authentication.data.authenticateKey(req, res)) {


            //console.log("in the if");
            res.json({ error: false, message: "aws credential", pool_id: "ap-south-1:d4a8d416-1302-42e2-8e03-64c05f588ac9" })

            //AWS_ACCESS_KEY: "AKIAIRIKC4CF4U3EPGHQ" AWS_SECRET_KEY: "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"
        }


    })

    app.post('/deepLink', function (req, res) {

        var androidLink = req.body.android_link;
        var iosLink = req.body.ios_link;
        var type = req.body.type;
	});

//    app.post('/deepLink', function(req, res){



  //      var androidLink = req.body.android_link;
    //    var iosLink = req.body.ios_link;
      //  var type = req.body.type;

      //  if (type == "Android") {
        //    res.json({ "error": false, "message": "Android Link", "link": androidLink });
      //  } else {
        //    res.json({ "error": false, "message": "Ios Link", "link": iosLink });
      //  }
   // })

    app.post('/templeteImages', function (req, res){

        let linkArray = [];

        let link = "https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/";
        // res.json({"messa":"hello"});
        // linkArray.push(link + "image_1_light.jpg");
        // linkArray.push(link + "image_2_light.jpg");
        linkArray.push(link + "image_3_light.jpeg");
        
        // linkArray.push(link + "image_4_light.jpeg");
        linkArray.push(link + "image_5_light.jpeg");
        linkArray.push(link + "image_6_light.jpeg");
        linkArray.push(link + "image_7_light.jpeg");
        linkArray.push(link + "image_8_light.jpeg");
        linkArray.push(link + "image_9_light.jpeg");
        linkArray.push(link + "image_10_light.jpeg");
        linkArray.push(link + "image_11_light.jpeg");
        linkArray.push(link + "image_12_light.jpeg");
        linkArray.push(link + "image_13_light.jpeg");
        linkArray.push(link + "image_14_light.jpeg");
        linkArray.push(link + "image_15_light.jpeg");
        linkArray.push(link + "image_16_light.jpg");
        linkArray.push(link + "image_17_light.jpg");
        linkArray.push(link + "image_18_light.jpg");
        linkArray.push(link + "image_19_light.jpg");
        linkArray.push(link + "image_20_light.jpg");
        linkArray.push(link + "image_21_light.jpg");
        if (linkArray.length == 18){
            res.json({ "error": false, "message": "Templete Links", "links": linkArray });
        }


    });

	app.get("/catalogues", function(req, res) {
		let room_id = req.query.room_id;
		var pageNo = req.query.page > 1 ? parseInt(req.query.page) : 0;
		var pageLimit = req.query.limit > 1 ? parseInt(req.query.limit) : 0;
		var offset = pageLimit * (pageNo - 1);
		var pageOffset = offset > 1 ? offset : 0;
		var q = Catalogue.find({ room_id: room_id }).limit(pageLimit).skip(pageOffset);
		
		q.exec(function(err, result) {
			if (err) {
				res.json({ error: true, message: err });
			} else {
				res.json({ error: false, data: result });
			}
		});
	});
	
	app.post("/decrypt", function(req, res) {
		let cypherText = req.body.q;
		if(cypherText != '') {
			var key = _crypt.getHashSha256(constants.SECRET_KEY, 32);
			var decypherText = _crypt.decrypt(cypherText, key, constants.IV)
			res.json({ error: false, data: decypherText });
		} else {
			res.json({ error: true, data: 'No input provided.' });
		}
	});
	
	app.get('/rooms', function (req, res) {
        room.count({}, function (err, totalCount) {
            if (err) {
                res.json({ success: false, message: "No data found!!!" });
            } else {
				room.aggregate({ $match: {} }, { $limit: 500000 }, function (err, roomsData) {
                    if (err) {
                        res.json({ error: true, message: "Database Error!!! fetch" });
                    } else {
						res.json({ success: true, message: "Data", data: roomsData });
                    }
                });
            }
        });
    });
	
	 
	app.post("/addReview", function(req, res) {
		authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var user_id = ObjectId(req.body.id);
                var room_id = req.body.room_id ? ObjectId(req.body.room_id) : '';
                var rating = req.body.rating ? parseFloat(req.body.rating) : '';
                var comment = req.body.comment ? req.body.comment : '';
                var where = {room_id: ObjectId(room_id), user_id: ObjectId(user_id)};

                if(room_id != '' && rating != '') {
                    review.find(where, function (errs, reviewData) {

                        if (errs) {
                            res.json({ error: true, message: "Database Error !!"});
                        }else {
                            if (reviewData.length > 0) {
                                res.json({ error: true, message: "Already Reviewed" });

                            }else{
                                if(typeof rating == 'number') {
                                    if(rating => 1 && rating <= 5) {
                                        let data = new review({
                                            user_id : user_id,
                                            room_id : room_id,
                                            rating : rating,
                                            comment : comment,
                                            created_at : Date.now().toString(),
                                            updated_at : Date.now().toString()
                                        })
                                        data.save((err, savedData)=>{
                                            if(err){
                                                res.json({error : true, message : "Error occures", data : err});
                                            }
                                            else{
                                                res.json({error : false, message : "Success", data : savedData});
                                            }
                                        })
                                    } else {
                                        var errorMsg = [];
                                        var m = 'Rating must be a valid number between 1 to 5';
                                        errorMsg.push(m);
                                        res.json({ error: true, message: m, data: errorMsg });
                                    }
                                } else {
                                    var errorMsg = [];
                                    var m = 'Rating must be a valid number between 1 to 5';
                                    errorMsg.push(m);
                                    res.json({ error: true, message: m, data: errorMsg });
                                }
                            }
                        }
                    });
                } else {
                   var errorMsg = [];
                   if(room_id == '') {
                      errorMsg.push('Room id is required');
                  }
                  if(rating == '') {
                      errorMsg.push('Rating is required');
                  }
                  // if(comment == '') {
                  //     errorMsg.push('Comment is required');
                  // }
                  res.json({ error: true, message: "Data is missing", data: errorMsg });
              }
          } else {
            res.json(result);
        }
    });
	});
     
    app.post("/updateReview", function(req, res) {
		authentication.data.authenticateToken(req, function (result) {
            if (result == true) {
                var user_id = ObjectId(req.body.id);
                var room_id = req.body.room_id ? ObjectId(req.body.room_id) : '';
                var rating = req.body.rating ? parseFloat(req.body.rating) : '';
                var comment = req.body.comment ? req.body.comment : '';
                var review_id = req.body.review_id ? req.body.review_id : '';
                if(room_id != '' && rating != '') {
                    review.updateOne({ _id: ObjectId(review_id) }, { $set: {  user_id : user_id,
                        room_id : room_id,
                        rating : rating,
                        comment : comment,
                        created_at : Date.now().toString(),
                        updated_at : Date.now().toString() 
                    }},
                    function (err, reviewUpdateDone) {
                        if(err){
                            res.json({error:true,"message":"error at review update"});
                        }else{
                            res.json({error:false,"message":"review updated sucessfully"});
                        }
                    });
                    
                } else {
                   var errorMsg = [];
                   if(room_id == '') {
                      errorMsg.push('Room id is required');
                  }
                  if(rating == '') {
                      errorMsg.push('Rating is required');
                  }
                  // if(comment == '') {
                  //     errorMsg.push('Comment is required');
                  // }
                  res.json({ error: true, message: "Data is missing", data: errorMsg });
              }
          } else {
            res.json(result);
        }
    });
	});
}


// https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/image_5_light.jpeg

// https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/image_6_dark.jpeg

// https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/image_7_dark.jpeg

// https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/image_8_light.jpeg

// https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/image_12_light.jpeg

// https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/image_13_light.jpeg

// https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/image_16_light.jpg

// https://tecmaths.s3.ap-south-1.amazonaws.com/Template+Images/image_17_light.jpg




