var sendNotification = function(ids, messageDetail) {
  
    var data = {
      // app_id: "155d040f-de3a-4769-ab47-76647cc11dcb",
      app_id: "6cd39ff4-2cd7-4227-a90d-0333a3e900e6",
      content_available: true,
      data: { "message": messageDetail, "type": "message" },
      include_player_ids: ids
      };

      ///console.log(data, "savbdhjsa");
  
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
      // "Authorization": "Basic M2U3MTRjZDEtYTc1ZC00NDk3LWIyOTItNjc4YjFmNDNhMzRi"
      "Authorization": "Basic OTMxMjM3MjYtZGFmYS00Nzg0LTk3ZmQtNTU1Y2QzMzkzMWUz"
    };
    
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers
    };
    
    
    var https = require('https');
    var req = https.request(options, function(res) {  
      res.on('data', function(data) {
        console.log("Response:");
        console.log(JSON.parse(data));
      });
    });
    
    req.on('error', function(e) {
      console.log("ERROR:");
      console.log(e);
    });
    
    req.write(JSON.stringify(data));
    req.end();
  };
  
  module.exports = sendNotification;