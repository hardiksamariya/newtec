var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//to check when if server restart it will reschedule jobs...
var gallerySchema = new Schema({
    user_id : {type: String, required: true, default: ""},
    created_at : {type: String, required: true},
    gallery_images:{type: String, default: ""}
  });
  
  var Gallery = mongoose.model('gallery', gallerySchema);
  
  module.exports = Gallery;