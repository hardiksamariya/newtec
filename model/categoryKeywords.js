var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//to check when if server restart it will reschedule jobs...
var categoryKeywordsSchema = new Schema({
	categoryId:{ type:String ,required:true,default: ""},
    name : {type: String, required: true, default: ""},
    created_at : {type: String, required: true},
    updated_at : {type: String, default: ""}
  });
  
  var categoryKeywordsModel = mongoose.model('categoryKeywords', categoryKeywordsSchema);
  
  module.exports = categoryKeywordsModel;