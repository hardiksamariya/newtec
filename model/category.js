var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//to check when if server restart it will reschedule jobs...
var categorySchema = new Schema({
    name : {type: String, required: true, default: ""},
    created_at : {type: String, required: true},
    image:{type: String, default: ""},
    type:{type: String, default: ""}
  });
  
  var Category = mongoose.model('categories', categorySchema);
  
  module.exports = Category;