var mongoose = require('mongoose');
//var ObjectId = mongoose.Types.ObjectId;
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/tecconnect', {useMongoClient: true});

var UserSchema = new Schema({
  first_name : {type: String, required: true, default: ""},
  last_name : {type: String, default: ""},
  phone : {type: String, default: ""},   //new added
  about : {type: String, default: ""},   //new added
  profile_image_url : {type: String, default: ""},
  device_id : {type: String, default: ""},
  device_type : {type: String, default: ""},
  
  // for push notification on one signal
  notification_token : {type: Object, default: ""},
  access_token : {type: String, default: ""},   //new added

  socket_id : {type: Object, default: ""}, //always update on every login time
  online_status : {type: Boolean, default: true}, //if socket id is available then it set to online else offline
  is_login : {type: Boolean, default: true}, //for login and logout

  is_blocked : {type: Boolean, default: false},   //new added

  created_at : {type: String, required: true},
  last_login : {type: String, required: true},

  rooms : [{type: ObjectId, default: ""}],  //new added
  invitations: [{                                   //new added
      sender_id : {type : Object, default: ""},
      room_id : {type : Object, default: ""}
  }],
  gallery_images : {type: String, default: ""},
  categories : [{type : ObjectId}],

  blocked_room : [{type: Object, default: ""}],
  muted_room : [{type: ObjectId, default: ""}],
  blocked : [{type: ObjectId, default: ""}],  //new added
  blocks : [{type: ObjectId, default: ""}]   //new added
});

var User = mongoose.model('users', UserSchema);

module.exports = User;