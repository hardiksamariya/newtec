var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var catalogueSchema = new Schema({
    text : {type: String, default: ""},
    caption : {type: String, default: ""},
    image : {type: String, default: ""},
    type : {type: String, default: ""},
    room_id : { type: String, required: true, default: "" },
    read_by : [{type: ObjectId, default: ""}],
    created_at : {type: String, required: true},
    updated_at : {type: String, default: ""}
});
  
var Catalogue = mongoose.model('catalogues', catalogueSchema);

module.exports = Catalogue;