var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var reviewSchema = new Schema({
    user_id : { type: Object, required: true, default: "" },
    room_id : { type: Object, required: true, default: "" },
	rating : { type: Number, min: 1, max: 5, require: true, default: "" },
	comment: { type: String, require: true, default: "" },
    created_at : { type: String, required: true, default: "" },
    updated_at : { type: String, default: "" }
});
  
var review = mongoose.model('review', reviewSchema);

module.exports = review;