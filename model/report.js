var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var ReportsSchema = new Schema({
    by_user_id : {type: Object, required: true, default: ""},
    to_user_id : {type: Object, required: true, default: ""},
    room_id : {type: Object, required: true, default: ""},
    reported_for : {type: String, required: false, default: ""},
    message_id : {type: Object, required: false, default: ""},
    message_content : {type: String, required: false, default: ""},
    created_at : {type: String, required: true}
});
var Report = mongoose.model('reports', ReportsSchema);
module.exports = Report;